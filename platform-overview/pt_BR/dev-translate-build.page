<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-translate-build" xml:lang="pt-BR">

  <info>
    <link type="next" xref="dev-translate-tools"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2014, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

  <title>Configurando seu sistema de compilação para tradução</title>

  <links type="series" style="floatend">
    <title>Configurando traduções</title>
  </links>

  <p>Você precisará configurar seu projeto e sistema de compilação para funcionar com traduções.</p>

  <p>Crie um subdiretório <file>po/</file> no diretório de seu projeto e liste os arquivos que possuem mensagens traduzíveis em <file>po/POTFILES.in</file>. Liste arquivos <em>sem</em> mensagens traduzíveis em <file>po/POTFILES.skip</file>.</p>

  <p>Adicione as seguintes linhas ao seu <file>configure.ac</file>:</p>
  <code>
IT_PROG_INTLTOOL([0.50.0])
AC_SUBST([GETTEXT_PACKAGE], [$PACKAGE_TARNAME])
AC_DEFINE_UNQUOTED([GETTEXT_PACKAGE], ["$GETTEXT_PACKAGE"], [Nome de pacote para gettext])</code>

  <p>Adicione as seguintes linhas ao seu <file>Makefile.am</file>:</p>
  <code>
SUBDIRS = po
</code>
  <code>
AM_CPPFLAGS = -DPACKAGE_LOCALEDIR=\""$(datadir)/locale"\"
</code>
  <code>
@INTLTOOL_DESKTOP_RULE@
desktopdir = $(datadir)/applications
desktop_in_files = data/<input>nomedoaplicativo</input>.desktop.in
desktop_DATA = $(desktop_in_files:.desktop.in=.desktop)
</code>

  <p>Execute <cmd>intltoolize</cmd> para copiar a infraestrutura de compilação do intltool para a árvore de compilação antes de executar <cmd>autoreconf</cmd>.</p>

  <p>Agora que seu sistema de compilação e suas mensagens fontes estão prontos para tradução, você precisa dizer ao <app>gettext</app> três coisas:</p>

  <list>
    <item><p>o <em>domínio de tradução</em>, geralmente o mesmo que o nome do aplicativo</p></item>
    <item><p>a localização onde as traduções compiladas serão instaladas</p></item>
    <item><p>a codificação de caracteres das traduções, geralmente UTF-8</p></item>
  </list>

  <example>
  <note>
    <p>Esse exemplo presume que seu aplicativo está escrito em C. Ele é um pouco diferente para outras linguagens de programação.</p>
  </note>

  <p>Adicione a seguinte linha ao arquivo fonte que contém sua função <code>main()</code>:</p>

  <code>#include "config.h"</code>

  <p>Então, adicione as seguintes linhas à sua função <code>main()</code>:</p>

  <code>
bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALEDIR);
bind_textdomain_codeset (PACKAGE_TARNAME, "UTF-8");
textdomain (GETTEXT_PACKAGE);
</code>
  </example>

  <p>Execute <cmd>make <input>nomedoprojeto</input>.pot</cmd> no diretório <file>po</file>. Ele executa <cmd>intltool-extract</cmd> para extrair as mensagens traduzíveis e colocá-las em um arquivo de modelo de po (POT).</p>

</page>
