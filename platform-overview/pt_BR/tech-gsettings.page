<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gsettings" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="tech" group="gsettings"/>
    <revision pkgversion="3.0" date="2013-01-30" status="candidate"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Armazenamento de configuração para preferências de aplicativo</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2014, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

<title>GSettings</title>

  <p>GSettings é a parte do <link xref="tech-glib">GLib</link> que permite que aplicativos salvem suas definições de configurações e preferências do usuário em uma forma padrão.</p>

  <p>Um aplicativo que usa GSettings define um <em>schema</em> (em português, esquema) de chaves de configuração. O schema para cada chave contém o nome da chave, uma descrição legível por humanos de para que a chave serve, um tipo para a chave (string, inteiro, etc.) e um valor padrão.</p>

  <p>GSettings usa o armazenamento do sistema operacional para dados de configuração. Em sistemas GNU, isso é o DConf; no Windows, isso é o Registro, e no Mac OS isso é o mecanismo de lista de propriedade NextStep.</p>

  <p>GSettings permite que você monitore alterações nos valores das chaves, de forma que seu aplicativo pode responder dinamicamente a alterações globais na configuração. Por exemplo, todos aplicativos que exibem relógios podem responder a uma definição global para exibição 12 horas ou 24 horas imediatamente, sem ter que reiniciar.</p>

  <list style="compact">
    <item><p><link href="http://developer.gnome.org/gio/stable/GSettings.html">Manual de referência do GSettings</link></p></item>
</list>


</page>
