<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="license" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="index" group="license"/>
    <revision pkgversion="3.14" date="2014-05-01" status="draft"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Qual licença você deveria usar para seu aplicativo?</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2014, 2016, 2017</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Isaac Ferreira Filho</mal:name>
      <mal:email>isaacmob@riseup.net</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  </info>

  <title>Licenciando seu aplicativo</title>

  <p>Ao escrever um novo aplicativo ou nova biblioteca, você precisará escolher uma licença de forma que outros saibam como eles podem usar ou reusar sua obra.</p>

  <p>Bibliotecas usadas no GNOME são, geralmente, licenciadas sob a <link href="http://www.gnu.org/">GNU</link> <link href="https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html">LGPL2.1</link>.</p>

  <p>A maioria dos aplicativos novos do GNOME são licenciados sob <link href="http://www.gnu.org/licenses/gpl-3.0.html">GPL3+</link>, enquanto alguns mais antigos são licenciados sob <link href="http://www.gnu.org/licenses/gpl-2.0.html">GPL2+</link>.</p>

  <p>Ajuda do usuário é escrita pela GNOME Documentation Team (Equipe de Documentação do GNOME) está licenciada sob <link href="http://creativecommons.org/licenses/by-sa/3.0/">CC-by-SA 3.0 Não Adaptada</link>. A equipe de documentação tenta usar essa licença consistentemente, pois ela permite reusar o texto do Wikipédia e muitas outras fontes de referências.</p>

  <p>Traduções possuem a mesma licença que as mensagens originais. Por exemplo, mensagens dos aplicativos são geralmente GPL2+ ou GPL3+, enquanto a documentação do usuário é geralmente CC-by-SA 3.0.</p>

  <p>GNOME não pode fornecer conselho legal sobre qual licença você deveria escolher, mas você pode se interessar em ler as informações que estão disponíveis no <link href="http://opensource.org/licenses">Open Source Initiative</link>, a <link href="http://www.gnu.org/licenses/license-recommendations.html">FSF</link> e a <link href="https://blogs.gnome.org/bolsh/2014/04/17/choosing-a-license/">publicação no blog do Dane Neary sobre a escolha de uma licença</link>. Você também pode se interessar pelas <link href="http://gstreamer.freedesktop.org/documentation/licensing.html">informações sobre licenciamento do GStreamer</link>, já que o GStreamer usa plug-ins.</p>
</page>
