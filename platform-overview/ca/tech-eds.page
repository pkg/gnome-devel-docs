<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-eds" xml:lang="ca">

  <info>
    <link type="guide" xref="tech" group="eds"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Desktop-wide address book for contacts and calendar</desc>
  </info>

<title>Evolution Data Server</title>

<p>Amb l'Evolution Data Server, el GNOME proporciona una única llibreta d'adreces i calendari perquè l'utilitzin totes les aplicacions per desar-hi i recuperar-ne informació. Si utilitzeu l'Evolution Data Server permetreu que l'usuari no hagi de mantenir llistes de contactes per a cada aplicació o hagi de copiar esdeveniments d'un calendari a un altre.</p>

<p>Cada vegada s'utilitzen més els ordinadors com a mitjà de comunicació amb els amics o companys. Les aplicacions de correu electrònic, de missatgeria instantània, de telefonia i de videoconferència s'utilitzen per comunicar-se amb altres persones. Aquestes aplicacions normalment proporcionen llistes de contactes per facilitar-ne l'ús. Amb l'Evolution Data Server les aplicacions poden emmagatzemar la informació dels contactes en una sola ubicació, facilitant que qualsevol aplicació pugui veure totes les dades rellevants dels contactes de l'usuari.</p>

<p>Les aplicacions també poden utilitzar l'Evolution Data Server per emmagatzemar i recuperar cites al calendari de l'usuari. Per exemple, el rellotge del quadre mostra un calendari simple quan s'hi fa clic, però si l'usuari té alguna cita planificada, aquesta es mostra al costat del calendari. D'aquesta manera és molt senzill veure si l'usuari té cap cita pròximament sense haver d'obrir l'aplicació del calendari. </p>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/libebook/stable/">Evolution API Reference: libebook</link></p></item>
  <item><p><link href="http://developer.gnome.org/libecal/stable/">Evolution API Reference: libecal</link></p></item>
</list>
</page>
