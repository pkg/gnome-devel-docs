<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-launching-startupnotify" xml:lang="sv">

  <info>
    <link type="next" xref="dev-launching-mime"/>
    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Uppstartsavisering</title>

  <links type="series" style="floatend">
    <title>Kör ditt program</title>
  </links>

  <p>Avisera användaren då ditt program har startats färdigt.</p>

  <p>GNOME implementerar <link href="https://standards.freedesktop.org/startup-notification-spec/startup-notification-latest.txt">Startup Notification-protokollet</link> för att ge återkoppling till användaren då programmets uppstart är klar.</p>

  <p>GTK-program stöder automatiskt uppstartsavisering, och aviserar som standard att programmets uppstart är färdig då det första fönstret visas. Ditt program måste deklarera att det stöder uppstartsavisering genom att lägga till <code>StartupNotify=true</code> till sin desktop-fil.</p>

  <p>Mer komplicerade uppstartsscenarier, så som att visa en startbild under uppstart, skulle behöva anpassad hantering med <code href="https://developer.gnome.org/gdk3/stable/gdk3-General.html#gdk-notify-startup-complete">gdk_notify_startup_complete()</code>.</p>

</page>
