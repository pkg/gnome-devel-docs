<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gstreamer" xml:lang="sv">

  <info>
    <link type="guide" xref="tech" group="gstreamer"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Spela upp, mixa och manipulera ljud och video</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>GStreamer</title>

<p>GStreamer är ett kraftfullt multimediabibliotek för att spela upp, skapa samt manipulera ljud, video och andra media. Du kan använda GStreamer för att tillhandahålla uppspelning av ljud och video, spela in inmatning från flera källor, samt redigera multimediainnehåll. GStreamer stöder kodning och avkodning av flera format som standard, och stöd för ytterligare format kan läggas till via insticksmoduler.</p>

<p>GStreamer tillhandahåller en flexibel arkitektur där media bearbetas genom en rörledning av element. Varje element kan tillämpa filter på innehållet, så som att koda eller avkoda, kombinera flera källor, eller omvandla multimediainnehållet. Denna arkitektur tillåter ett godtyckligt arrangemang av element, så du kan åstadkomma nästan vilken effekt som helst med GStreamer. Vidare är GStreamer designat för att ha låg prestandakostnad, så det kan användas i program med höga latenskrav.</p>

<p>Medan GStreamer tillhandahåller ett kraftfullt API för att manipulera multimedia så tillhandahåller det även behändiga rutiner för enkel uppspelning. GStreamer kan automatiskt konstruera en rörledning för att läsa och spela upp filer i alla format som stöds, vilket låter dig lätt använda ljud och video i ditt program.</p>

<p>GStreamer-arkitekturen låter insticksmodulera lägga till kodare, avkodare, och alla sorters innehållsfilter. Tredjepartsutvecklare kan tillhandahålla GStreamer-insticksmoduler vilka kommer att automatiskt vara tillgängliga för andra program som använder GStreamer. Insticksmoduler kan tillhandahålla stöd för andra multimediaformat eller tillhandahålla ytterligare funktionalitet och effekter.</p>

<p>Du bör använda GStreamer närhelst du behöver läsa eller spela upp multimediainnehåll i ditt program, eller om ditt behöver manipulera ljud eller video. Att använda GStreamer gör din programutveckling enkel, och det tillhandahåller dig vältestade element för många av dina behov.</p>

<list style="compact">
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/index.html">GStreamers programutvecklingshandbok</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/">Referenshandbok för GStreamer 1.0 Core</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/documentation/">GStreamers dokumentationssida</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org">GStreamers webbplats</link></p></item>
</list>

</page>
