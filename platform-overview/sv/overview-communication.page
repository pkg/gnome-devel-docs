<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="overview" id="overview-communication" xml:lang="sv">
  <info>
    <revision version="0.1" date="2012-02-19" status="stub"/>
    <link type="guide" xref="index" group="communication"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>

    <title type="link" role="trail">Kommunikation</title>
    <desc>Stöd för snabbmeddelanden, nätverk, sociala medier, e-post och kalender.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Kommunikation och sociala nätverk</title>

<list>
 <item>
  <p><em style="strong">Anslut till tjänster för snabbmeddelanden och sociala nätverk</em></p>
 </item>
 <item>
  <p><em style="strong">Konfigurera flerprotokollsanslutningar med webbtjänster eller andra klienter</em></p>
 </item>
 <item>
  <p><em style="strong">Hantera e-post, nätkontakter och kalendertjänster</em></p>
 </item>
</list>

<p>Få dina användare att bli anslutna och kunna kommunicera med sina vänner och kontakter genom snabbmeddelanden, sociala medier och e-post. GNOME:s omfattande kommunikationsstack ger dig abstrakt högnivååtkomst till komplicerade protokoll för snabbmeddelanden och e-post. För mer specialiserade kommunikationsbehov finns det även åtkomst till det underliggande maskineriet genom API:er på lägre nivå.</p>

<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>Empathy snabbmeddelandeklient</p>
</media>

<section id="what">
 <title>Vad kan du göra?</title>

 <p>För att <em style="strong">ansluta till snabbmeddelandetjänster</em>, använd <em style="strong">Telepathy</em>. Det tillhandahåller ett kraftfullt ramverk för att interagera med användarens snabbmeddelandekontakter, och har stöd för ett stort antal meddelandeprotokoll. Med Telepathy hanteras alla konton och anslutningar av en D-Bus-sessionstjänst som är djupt integrerad med GNOME. Program kan ansluta sig till denna tjänst för att kommunicera med kontakter.</p>

 <p>Skapa spel för flera spelare eller samarbetesredigerare som är integrerade med de skrivbordsomfattande snabbmeddelandetjänsterna. Med <em style="strong" xref="tech-telepathy">Telepathy Tubes</em>-API:t kan du <em style="strong">tunnla ett godtyckligt protokoll</em> över moderna snabbmeddelandeprotokoll som Jabber för att skapa interaktiva program.</p>

 <p>Låt användare se andra personer som de kan chatta med, och hitta skrivare, delade filer samt delade musiksamlingar så fort som de ansluts till ett nätverk. <em style="strong" xref="tech-avahi">Avahi</em>-API:t tillhandahåller <em style="strong">tjänsteupptäckt</em> på ett lokalt nätverk via mDNS/DNS-SD-protokollsviten. Det är kompatibelt med liknande teknologi som hittas i MacOS X och Windows.</p>

 <p>Hantera användares adressböcker, lokala och på nätet, med <em style="strong" xref="tech-eds">Evolution Data Server</em> (EDS). Det tillhandahåller ett sätt att lagra kontoinformation och interagera med…</p>

 <p>Med <em style="strong" xref="tech-folks">Folks</em> kommer du ha åtkomst till ett ensamt API för att hantera sociala nätverk, chatt, e-post och ljud/videokommunikation.</p>

</section>

<!--<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>Change the IM status</p></item>
  <item><p>Fetch a contact from a Gmail address book</p></item>
  <item><p>Scan the network for zeroconf printers</p></item>
  <item><p>Something with Telepathy Tubes</p></item>
 </list>
</section>-->

<section id="realworld">
 <title>Exempel från verkligheten</title>

 <p>Du kan se många faktiska tillämpningar av GNOME-kommunikationsteknologier i öppna källkodsprojekt, så som exemplen nedan.</p>
 <list>
  <item>
   <p><em style="strong">Empathy</em> är ett snabbmeddelandeprogram med stöd för ett stort antal meddelandetjänster. Det använder Telepathy för att hantera anslutningar, närvaro samt kontaktinformation för alla protokoll som det stöder.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Empathy">Webbplats</link> | <link href="https://wiki.gnome.org/Apps/Empathy#Screenshots">Skärmbilder</link> | <link href="https://gitlab.gnome.org/GNOME/empathy/">Källkod för Empathy</link> )</p>
  </item>

  <item>
   <p>Med stöd för Telepathy Tubes kunde samlingen <em style="strong">GNOME Spel</em> lägga till stöd för flerspelarläge genom Jabber-protokollet.</p>
   <p>(<link href="https://wiki.gnome.org/Projects/Games">Webbplats</link> | <link href="https://wiki.gnome.org/Apps/Chess#Screenshots">Skärmbild</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-chess/">GNOME Schack-kod för flera spelare över nätet</link> )</p>
  </item>

  <item>
   <p>Avahi-stöd låter användare av musikspelaren <em style="strong">Rhythmbox</em> se delade musiksamlingar på deras lokala nätverk med DAAP.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Rhythmbox">Webbplats</link> | <link href="https://wiki.gnome.org/Apps/Rhythmbox/Screenshots">Skärmbilder</link> | <link href="https://gitlab.gnome.org/GNOME/rhythmbox/tree/master/plugins/daap">DAAP-kod</link> )</p>
  </item>
 </list>

</section>

</page>
