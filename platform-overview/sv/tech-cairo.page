<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-cairo" xml:lang="sv">

  <info>
    <link type="guide" xref="tech" group="cairo"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Vektorbaserad 2D-uppritning för högkvalitativ grafik</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Nylander</mal:name>
      <mal:email>po@danielnylander.se</mal:email>
      <mal:years>2007</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Cairo</title>

<p>Cairo är ett 2D-grafikbibliotek som har ett sofistikerat API för att rita vektorgrafik, sammansättning av bilder, och rendering av kantutjämnad text. Cairo tillhandahåller stöd för flera utdataenheter, inkluderande X-fönstersystemet, Microsoft Windows, och bildbuffertar i minnet, vilket låter dig skriva plattformsoberoende kod för att rita grafik på olika media.</p>

<p>Cairos ritmodell är liknande de som tillhandahålls av PostScript och PDF. Cairo-API:t tillhandahåller ritåtgärder som penselstreck och att fylla kubiska Bézier-splinekurvor, sammansättning av bilder samt utförande av affina avbildningar. Dessa vektoråtgärder möjliggör en rik, kantutjämnad grafik.</p>

<p>Cairos rika ritmodell möjliggör högkvalitativ rendering till flera medier. Samma API kan användas för att skapa grafik och text på skärmen, rendera bilder, eller skapa skarp utmatning lämplig för utskrift.</p>

<p>Du bör använda Cairo närhelst du behöver rita grafik i ditt program bortom komponenterna som tillhandahålls av GTK. Nästan allt ritande i GTK görs med Cairo. Att använda Cairo för ditt anpassade ritande kommer låta ditt program ha högkvalitativ, kantutjämnad och upplösningsoberoende grafik.</p>

<list style="compact">
  <item><p><link href="http://www.cairographics.org/manual/">Cairo-handbok</link></p></item>
  <item><p><link href="http://www.cairographics.org">Cairos webbplats</link></p></item>
</list>
</page>
