<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-eds" xml:lang="ja">

  <info>
    <link type="guide" xref="tech" group="eds"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Desktop-wide address book for contacts and calendar</desc>
  </info>

<title>Evolution Data Server</title>

<p>Evolution データサーバーにより、GNOME はすべてのアプリケーションが情報を保存・取得することができる単一のアドレス帳とカレンダーを提供します。Evolution データサーバーを使うということはユーザーがそれぞれのアプリケーションの連絡先を分割管理する必要がなくなり、それらのカレンダーに手動でイベントをコピーする必要がなくなることを意味します。</p>

<p>人々が友人や同僚と対話するのにコンピューターを使用する機会はますます増加しています。電子メール、インスタント・メッセンジャー、テレフォニー、そしてビデオ会議はいずれも他の人と対話するのに使用されるアプリケーションです。これらのアプリケーションはユーザーのためにしばしば連絡先一覧を提供します。Evolution データサーバーを使用すればアプリケーションは連絡先の情報を1ヶ所に保存することができ、すべてのアプリケーションがユーザーの連絡先と関連するデータを参照することができます。</p>

<p>Evolution データサーバーを使用すればアプリケーションがユーザーのカレンダーに予定を保存・取得することもできます。たとえば、パネルに表示されている時計をクリックするとシンプルなカレンダーが表示されます。もしユーザーが予定をスケジュールしていた場合は、そのカレンダーに予定が表示されます。これによりユーザーはカレンダーのためのアプリケーションを開くことなく簡単に予約を確認することができます。</p>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/libebook/stable/">Evolution API Reference: libebook</link></p></item>
  <item><p><link href="http://developer.gnome.org/libecal/stable/">Evolution API Reference: libecal</link></p></item>
</list>
</page>
