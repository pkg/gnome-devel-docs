<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gtk" xml:lang="cs">

  <info>
    <link type="guide" xref="tech" group="gtk"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011 – 2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Vývojářský nástroj wedgetů pro grafické rozhraní</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Lucas Lommer</mal:name>
      <mal:email>llommer@svn.gnome.org</mal:email>
      <mal:years>2009.</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Marek Černocký</mal:name>
      <mal:email>marek@manet.cz</mal:email>
      <mal:years>2015.</mal:years>
    </mal:credit>
  </info>

<title>GTK</title>

<p>GTK je základní knihovnou pro tvorbu uživatelského rozhraní v prostředí GNOME. Její název pochází z „GIMP Tool Kit“, která byla původně napsána pro potřeby grafického programu GIMP a později od něj oddělena jako samostatná knihova. Nabízí všechny uživatelské ovládací prvky, tzv. <em>widgety</em>, používané v aplikacích pro grafický režim. Její moderní a objektově orientované API vám umožňuje tvořit atraktivní a sofistikovaná uživatelská rozhraní, aniž se musíte potýkat s nizkoúrovňovými problémy jako vykreslování nebo spolupráce se zařízeními.</p>

<p>K základním prvkům jako tlačítka, zaškrtávací políčka a textová pole přidává GTK také API k Model-View-Controller (MVC) pro stromové zobrazení, víceřádková textová pole a operace s nástrojovými lištami a nabídkami.</p>

<p>Widgety jsou v GTK umisťovány do okna s využitím <em>modelu box-packing</em>. Programátoři pouze udají, jak chtějí „zabalit“ widgety dohromady do kontejnerových boxů namísto určování pozice na absolutních souřadnicích. GTK zajišťuje, že mají okna správné rozměry vzhledem k požadavkům obsahu a automaticky obsluhuje změny velikosti okna. Pro jazyky používající směr zprava-do-leva, jako je arabština nebo hebrejština, obrací GTK automaticky směr uživatelského rozhraní zleva doprava, takže prvky mají od pohledu očekávané pořadí.</p>

<p>GTK umožňuje vyvíjet vlastní widgety pro použití v aplikacích. Podobně jako nativní a skladové widgety, i tyto vlastní widgety mohou podporovat všechny funkce, které GTK má: podpora jazyků psaných zprava-do-leva, přístupnost rozhraní, ovládání z klávesnice a automatická změna velikosti.</p>

<list style="compact">
  <item><p><link href="https://www.gtk.org/">Oficiální webové stránky</link></p></item>
  <item><p><link href="https://developer.gnome.org/gnome-devel-demos/stable/">Průvodce ukázkami</link></p></item>
  <item><p><link href="https://developer.gnome.org/gtk3/stable/">Referenční příručka</link></p></item>
  <item><p><link href="https://gitlab.gnome.org/GNOME/gtk/">Repozitář git</link></p></item>
  <item><p><link href="https://discourse.gnome.org/">Diskuze o GTK a poskytování pomoci</link></p></item>
</list>
</page>
