<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="overview" id="overview-communication" xml:lang="uk">
  <info>
    <revision version="0.1" date="2012-02-19" status="stub"/>
    <link type="guide" xref="index" group="communication"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>

    <title type="link" role="trail">Зв'язок</title>
    <desc>Підтримка миттєвого обміну повідомленнями, роботи у мережі, соціальних мережах, роботи із електронною поштою та календарями.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Спілкування і соціальні мережі</title>

<list>
 <item>
  <p><em style="strong">Встановлення з'єднання зі службами миттєвого обміну повідомленнями та соціальними мережами</em></p>
 </item>
 <item>
  <p><em style="strong">Налаштовування багатопротокольних з'єднань із службами мережі та іншими клієнтами</em></p>
 </item>
 <item>
  <p><em style="strong">Робота з поштою, службами інтернет-контактів та календарів</em></p>
 </item>
</list>

<p>Надайте вашим користувачам змогу зв'язуватися і спілкуватися із їхніми друзями та контактами за допомогою обміну миттєвими повідомленнями, соціальних мереж та електронної пошти. Широкий стос бібліотек для спілкування GNOME надає у ваше розпорядження високорівневий абстрактний доступ до складних протоколів миттєвого обміну повідомленнями та електронної пошти. Для спеціалізованіших потреб у спілкування передбачено також і доступ до основ за допомогою низькорівневих програмних інтерфейсів.</p>

<media type="image" mime="image/png" src="test_comm1.png" width="65%">
 <p>Клієнт обміну миттєвими повідомленнями Empathy</p>
</media>

<section id="what">
 <title>Що робити?</title>

 <p>Для <em style="strong">встановлення з'єднання зі службами миттєвого обміну повідомленнями</em> скористайтеся <em style="strong">Telepathy</em>. Ця програма надає потужні бібліотеки для взаємодії із контактами миттєвого обміну повідомленнями користувача, у ній реалізовано широкий діапазон для протоколів обміну повідомленнями. За допомогою Telepathy усі облікові записи і з'єднання обробляються службою сеансів D-Bus, яку тісно інтегровано із GNOME. Програми можуть прив'язуватися до цієї служби для того, щоб обмінюватися даним з контактами.</p>

 <p>Створюйте ігри із декількома гравцями або редактори для спільного редагування, які інтегруються із загальними службами миттєвого обміну повідомленнями. За допомогою програмного інтерфейсу <em style="strong" xref="tech-telepathy">Telepathy Tubes</em> ви можете <em style="strong">тунелювати довільний протокол</em> крізь сучасні протоколи миттєвого обміну повідомленнями, подібні до Jabber, для створення інтерактивних програм.</p>

 <p>Надайте користувачам змогу переглядати список тих, із ким вони можуть спілкуватися, знаходити принтери, спільні файли та спільні збірки музичних творів, щойно буде встановлено з'єднання з мережею. Програмний інтерфейс <em style="strong" xref="tech-avahi">Avahi</em> забезпечує <em style="strong">виявлення служб</em> у локальній мережі за допомогою комплексу протоколів mDNS/DNS-SD. Він сумісний із подібною технологією, яка використовується у MacOS X та Windows.</p>

 <p>Обробляйте локальні та мережеві адресні книги користувачів та календарі за допомогою <em style="strong" xref="tech-eds">сервера даних Evolution</em> (EDS). Сервер надає спосіб зберігання відомостей щодо облікових записів та спосіб роботи з ними…</p>

 <p>За допомогою <em style="strong" xref="tech-folks">Folks</em> ви матимете доступ до єдиного програмного інтерфейсу для роботи у соціальних мережах, текстового спілкування, електронної пошти та спілкування за допомогою обміну звуковими та відеоданими.</p>

</section>

<!--<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>Change the IM status</p></item>
  <item><p>Fetch a contact from a Gmail address book</p></item>
  <item><p>Scan the network for zeroconf printers</p></item>
  <item><p>Something with Telepathy Tubes</p></item>
 </list>
</section>-->

<section id="realworld">
 <title>Приклади з реального життя</title>

 <p>Технології комунікації GNOME використовуються у багатьох реальних програмах у проєктах із відкритим кодом, подібно до прикладів, які описано нижче.</p>
 <list>
  <item>
   <p><em style="strong">Empathy</em> — програма для обміну миттєвими повідомленнями, у якій передбачено підтримку широкого діапазону служб обміну повідомленнями. У ній використовується Telepathy для обробки з'єднань, даних щодо присутності та контактів для усіх підтримуваних протоколів.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Empathy">Сайт</link> | <link href="https://wiki.gnome.org/Apps/Empathy#Screenshots">Знімки вікон</link> | <link href="https://gitlab.gnome.org/GNOME/empathy/">Початковий код Empathy</link>)</p>
  </item>

  <item>
   <p>Використовуючи підтримку Telepathy Tubes, у збірці <em style="strong">ігор GNOME</em> реалізовано підтримку ігор із декількома гравцями за допомогою протоколу Jabber.</p>
   <p>(<link href="https://wiki.gnome.org/Projects/Games">Сайт</link> | <link href="https://wiki.gnome.org/Apps/Chess#Screenshots">Знімок вікна</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-chess/">Код інтернет-версії гри у шахи із багатьма гравцями для GNOME</link> )</p>
  </item>

  <item>
   <p>Підтримка Avahi надає змогу користувачам музичного програвача <em style="strong">Rhythmbox</em> переглядати спільні музичні збірки у їхній локальній мережі за допомогою DAAP.</p>
   <p>(<link href="https://wiki.gnome.org/Apps/Rhythmbox">Сайт</link> | <link href="https://wiki.gnome.org/Apps/Rhythmbox/Screenshots">Знімки вікон</link> | <link href="https://gitlab.gnome.org/GNOME/rhythmbox/tree/master/plugins/daap">Код DAAP</link> )</p>
  </item>
 </list>

</section>

</page>
