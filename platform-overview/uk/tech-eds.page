<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-eds" xml:lang="uk">

  <info>
    <link type="guide" xref="tech" group="eds"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Загальна адресна книга середовища для записів контактів та календаря</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

<title>Сервер даних Evolution</title>

<p>Сервер даних Evolution забезпечує GNOME єдиною адресною книгою та календарем, яким користуються усі програми для зберігання та отримання даних. Використання сервера даних Evolution усуває для користувачів потребу у супроводі окремих списків контактів у кожній програмі або у копіювання записів подій до календаря вручну.</p>

<p>Люди все ширше використовують комп'ютери для взаємодії із друзями та колегами. Програми, зокрема програми для обміну електронною поштою, миттєвими повідомленнями та програми для проведення телефонних та відеоконференцій використовуються для спілкування із іншими людьми. Часто у цих програмах реалізовано допоміжні списки контактів. Використання сервера даних Evolution надає програмам змогу зберігати дані контактів у єдиному сховищі, забезпечуючи програмам доступ до усіх потрібних даних щодо записів контактів користувачів.</p>

<p>Крім того, програми можуть використовувати сервер даних Evolution для зберігання і отримання даних щодо зустрічей у календарі користувача. Наприклад, годинник на панелі може показувати простий календар, коли користувач клацне на ньому. Якщо у користувача призначено якісь зустрічі, їхні дані буде показано поруч із календарем. Це надає змогу бачити наступні зустрічі без потреби у запуску повноцінної програми-календаря.</p>

<list style="compact">
  <item><p><link href="http://developer.gnome.org/libebook/stable/">Довідник із програмного інтерфейсу Evolution: libebook</link></p></item>
  <item><p><link href="http://developer.gnome.org/libecal/stable/">Довідник із програмного інтерфейсу Evolution: libecal</link></p></item>
</list>
</page>
