<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-tracker" xml:lang="uk">

  <info>
    <link type="guide" xref="tech" group="tracker"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Зберігання і отримання метаданих документів</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Tracker</title>

  <p>Tracker — рушій зберігання RDF (формату даних ресурсів). RDF складається з <em>триплетів</em>, зокрема комбінацій тема:дія:об'єкт. Наприклад, можуть бути стандартні триплети для заголовків книг та авторів, зокрема <code>Othello:has-author:William Shakespeare</code>. Стандартний набір форм триплетів називається <em>онтологією</em>.</p>

  <p>Tracker надає сховище для таких триплетів та рушій обробки запитів у форматі запитів мови SPARQL.</p>

  <p>GNOME використовує Tracker як сховище для метаданих документів. Метадані документа можуть включати його заголовок, авторів, авторські права, дату внесення змін та ключові слова. Усі ці метадані зберігаються як триплети RDF у Tracker. Запити щодо них подаються за допомогою SPARQL програмами, подібними до «Документів» GNOME. Вибір <link href="http://developer.gnome.org/ontology/unstable/">використаної онтології</link> засновано на різноманітних стандартних підонтологіях: <link href="http://dublincore.org/">Dublin Core</link> для метаданих документів і <link href="http://nepomuk.kde.org/">NEPOMUK</link> для анотацій, файлів, записів контактів та інших даних.</p>

  <list style="compact">
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker">Домашня сторінка Tracker</link></p></item>
    <item><p><link href="https://wiki.gnome.org/Projects/Tracker/Documentation">Документація з Tracker</link></p></item>
  </list>

</page>
