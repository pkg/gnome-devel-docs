<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-help-write" xml:lang="uk">

  <info>
    <link type="next" xref="dev-help-build"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author">
      <name>Єкатерина Герасимова (Ekaterina Gerasimova)</name>
      <email its:translate="no">kittykat3756@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Максим Дзюманенко</mal:name>
      <mal:email>dziumanenko@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Юрій Чорноіван</mal:name>
      <mal:email>yurchor@ukr.net</mal:email>
      <mal:years>2020</mal:years>
    </mal:credit>
  </info>

  <title>Написання довідки</title>

  <links type="series" style="floatend">
    <title>Налаштовування довідки</title>
  </links>

  <p>Напишіть декілька сторінок <link href="http://www.projectmallard.org/">Mallard</link> і запишіть їх до каталогу <file>help/C/</file>.</p>

  <p>У більшості проєктів має бути <file>index.page</file> та декілька сторінок вмісту, хоча файла <file>index.page</file> може і не бути, якщо ви готуєте довідку як додаток до іншого проєкту.</p>

  <example>
    <listing>
      <title><file>help/C/index.page</file></title>
<code>
&lt;page xmlns="http://projectmallard.org/1.0/"
      xmlns:its="http://www.w3.org/2005/11/its"
      type="guide"
      id="index"&gt;

  &lt;info&gt;
    &lt;revision pkgversion="3.9" date="2013-06-19" status="stub"/&gt;

    &lt;include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/&gt;
  &lt;/info&gt;

  &lt;title&gt;
    &lt;media type="image" mime="image/png" its:translate="no" src="figures/icon.png" /&gt;
    Application name
  &lt;/title&gt;

  &lt;section id="features" style="2column"&gt;
    &lt;title&gt;Features&lt;/title&gt;
  &lt;/section&gt;

&lt;/page&gt;
</code>
    </listing>

    <listing>
      <title><file>help/C/introduction.page</file></title>
<code>
&lt;page xmlns="http://projectmallard.org/1.0/"
      xmlns:its="http://www.w3.org/2005/11/its"
      type="topic"
      id="introduction"&gt;

  &lt;info&gt;
    &lt;link type="guide" xref="index"/&gt;
&lt;!--
    &lt;link type="guide" xref="index#features"/&gt;
    &lt;link type="seealso" xref="anotherpageid"/&gt;
--&gt;
    &lt;revision pkgversion="3.9" date="2013-06-19" status="stub"/&gt;

    &lt;credit type="author"&gt;
      &lt;name&gt;Your Name&lt;/name&gt;
      &lt;email its:translate="no"&gt;mail@example.com&lt;/email&gt;
      &lt;years&gt;2013&lt;/years&gt;
    &lt;/credit&gt;

    &lt;include href="legal.xml" xmlns="http://www.w3.org/2001/XInclude"/&gt;

    &lt;desc&gt;Welcome to _Application name_&lt;/desc&gt;
  &lt;/info&gt;

  &lt;title&gt;Introduction&lt;/title&gt;

  &lt;p&gt;This application is awesome!&lt;/p&gt;

&lt;/page&gt;
</code>
    </listing>
  </example>

</page>
