<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-io" xml:lang="de">
  <info>
    <link type="guide" xref="index" group="io"/>
    <revision version="0.1" date="2012-02-24" status="review"/>

    <credit type="author copyright">
      <name>Phil Bull</name>
      <email its:translate="no">philbull@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Zugriff auf strukturierte Datenspeicher, Netzwerkfreigaben und Dateien.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Datei- und Datenzugriff</title>

<list>
 <item>
  <p><em style="strong">Dateien und andere Datenströme asynchron schreiben und lesen</em></p>
 </item>
 <item>
  <p><em style="strong">Metadaten zu Dokumenten beziehen und speichern</em></p>
 </item>
</list>

<p>Greifen Sie mit den E/A-Bibliotheken in GNOME nahtlos auf lokale und ferne Dateien zu. Halten Sie Ihre Anwendung im laufenden Betrieb benutzbar, indem Sie die umfangreiche Unterstützung für asynchrone E/A-Operationen nutzen. Ermöglichen Sie den Benutzern, ihre Dateien besser und treffsicherer zu finden, indem Sie Metadaten zur Dokumentbeschreibung verwenden.</p>

<section id="what">
 <title>Was können Sie tun?</title>

  <p>Um <em style="strong">Dateien und andere Datenströme asynchron zu lesen und zu schreiben </em>, verwenden Sie <em style="strong" xref="tech-gio">GIO</em>. Es stellt eine VFS-API (virtuelles Dateisystem) höherer Ebene bereit, ergänzt durch Hilfswerkzeuge wie Symbole und Anwendungsstarter.</p>

  <p>Mit <em style="strong" xref="tech-tracker">Tracker</em> speichern Sie <em style="strong">Dokument-Metadaten</em> sowie <em style="strong">strukturierte Daten data</em>, beispielsweise Kontakte.</p>

</section>

<!-- TODO Add link to code examples.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Reale Beispiele</title>

  <p>Nachfolgend finden Sie einige Beispiele aus Anwendungen der realen Welt, in denen GNOME-E/A-Technologien in Projekten der freien Software verwendet werden.</p>
  <list>
    <item>
      <p>Die Anwendung <em style="strong">Dokumente</em> erleichtert die Dateisuche mit <em style="strong">Tracker</em>.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Documents">Internetseite</link> | <link href="https://gitlab.gnome.org/GNOME/gnome-documents">Quellcode</link> )</p>
  </item>
    <item>
      <p>Die Anwendung <em style="strong">Dateien</em> verwendet <em style="strong">GIO</em>, um sowohl lokale als auch ferne Dateien zu verwalten.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Nautilus">Internetseite</link> | <link href="https://wiki.gnome.org/Apps/Nautilus/Screenshots">Bildschirmfotos</link> | <link href="https://gitlab.gnome.org/GNOME/nautilus">Quellcode</link> )</p>
  </item>
 </list>
</section>
</page>
