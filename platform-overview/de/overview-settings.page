<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-settings" xml:lang="de">
  <info>
    <link type="guide" xref="index" group="settings"/>
    <revision version="0.1" date="2013-08-06" status="review"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Flexibles System zur Benutzerkonfiguration.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Einstellungsverwaltung</title>

<list>
  <item>
    <p><em style="strong">Zugriff auf Anwendungseinstellungen auf hohem Niveau</em></p>
  </item>
  <item>
    <p><em style="strong">Einfaches Verbinden von Einstellungen mit UI-Elementen</em></p>
  </item>
  <item>
    <p><em style="strong">Flexible Anpassungen für Paketersteller und Systemverwalter</em></p>
  </item>
</list>

<p>Speichern Sie Benutzereinstellungen und lassen Sie mit GSettings Ihre Anwendung automatisch auf Einstellungsänderungen reagieren. Setzen Sie als Systemverwalter Standardeinstellungen außer Kraft. Speichern Sie ein breites Spektrum von Daten, wie Ganzzahlen oder String arrays.</p>

<section id="what">
  <title>Was können Sie tun?</title>

  <p>Die <em style="strong" xref="tech-gsettings">GSettings</em>-API von GIO liest und schreibt <em style="strong">Anwendungseinstellungen settings</em>. GSettings nutzt die <em style="strong">Konfigurationsdatenbank der Plattform transparent</em>, so dass plattformspezifische Einstellungswerkzeuge verwendet werden können. Mit einer einzigen Funktion können <em style="strong">Bedienelemente der grafischen Oberfläche aktualisiert werden</em>.</p>

</section>

<!-- TODO: Write code sample.
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples">More…</link></p></item>
 </list>
</section>
-->

<section id="realworld">
  <title>Reale Beispiele</title>

  <p>Fast alle GNOME-Anwendungen nutzen GSettings.</p>
  <list>
    <item>
      <p><em style="strong">Dconf editor</em> ist ein grafisches Werkzeug zur Verwaltung von Einstellungen, die in der Dconf-Datenbank mit GSettings gespeichert werden.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/DconfEditor">Internetseite</link> | <link href="https://gitlab.gnome.org/GNOME/dconf-editor/">Quellcode</link> )</p>
    </item>
  </list>
</section>
</page>
