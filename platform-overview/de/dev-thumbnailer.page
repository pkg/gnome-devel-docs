<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-thumbnailer" xml:lang="de">

  <info>
    <link type="guide" xref="index" group="thumbnailer"/>
    <link type="seealso" xref="dev-launching-desktop"/>

    <revision version="0.1" date="2014-01-28" status="draft"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2014</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Vorschaubilder für Dokument-artige Dateien.</desc>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Vorschaubilder für Dateien</title>

  <p>Wenn Ihre Anwendung mit Dateien umgehen kann, die ausgedruckt oder als Dokument angezeigt werden können, fügen Sie einen Thumbnailer hinzu, damit diese in <app>Dateien</app> mit Vorschaubildern angezeigt werden können.</p>

  <p>Die Bibliothek gnome-desktop definiert die Schnittstelle und die zugehörigen Dateien, die ein Programm zur Vorschaubilderzeugung implementieren muss. Die für den Aufruf des Vorschaubilderzeugers verantwortliche Komponente ist <link href="https://developer.gnome.org/gnome-desktop3/stable/GnomeDesktopThumbnailFactory.html">GnomeDesktopThumbnailFactory</link>.</p>

  <p>Ein häufig verwendetes Hilfsprogramm für Vorschaubilder, das die Auswertung der Befehlszeile vornimmt und die Ausgabedatei erzeugt, ist <link href="https://github.com/hadess/gnome-thumbnailer-skeleton">gnome-thumbnailer-skeleton</link>.</p>

</page>
