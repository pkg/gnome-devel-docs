<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-telepathy" xml:lang="de">

  <info>
    <link type="guide" xref="tech" group="telepathy"/>
    <revision pkgversion="3.0" date="2011-04-05" status="incomplete"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>
    <credit type="copyright editor">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Vereinheitlichter Sofortnachrichten- und Kommunikationsdienst</desc>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

<title>Telepathy</title>

  <p>Telepathy bietet eine mächtige Umgebung für die Interaktion mit den Sofortnachrichten-Kontakten des Benutzers. Mit Telepathy werden alle Konten und Verbindungen über einen <link xref="tech-d-bus">D-Bus</link>-Sitzungsdienst verarbeitet, der tief in der GNOME-Arbeitsumgebung verankert ist. Anwendungen können diesen Dienst nutzen, um mit den Kontakten zu kommunizieren.</p>

<p>Mit der Tubes-API von Telepathy können Sie ein beliebiges Protokoll durch moderne Sofortnachrichten-Protokolle wie beispielsweise Jabber tunneln, um interaktive Anwendungen zu entwerfen. Erstellen Sie für mehrere Teilnehmer geeignete Spiele oder kollaborative Editoren, die sich in die globalen Sofortnachrichtendienste integrieren.</p>

<list style="compact">
  <item><p><link href="http://telepathy.freedesktop.org/doc/book/">Das Entwicklerhandbuch von Telepathy</link></p></item>
  <item><p><link href="http://telepathy.freedesktop.org">Die Webseite von Telepathy</link></p></item>

</list>

</page>
