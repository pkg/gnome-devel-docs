<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="dev-translate-setup" xml:lang="de">

  <info>
    <link type="next" xref="dev-translate-build"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author copyright">
      <name>Michael Hill</name>
      <email its:translate="no">mdhillca@gmail.com</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2009-2012, 2016</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2011, 2012, 2015, 2017, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Aljosha Papsch</mal:name>
      <mal:email>al@rpapsch.de</mal:email>
      <mal:years>2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  </info>

  <title>Zeichenketten für die Übersetzung markieren</title>

  <links type="series" style="floatend">
    <title>Übersetzungen einrichten</title>
  </links>

  <p>Bevor die Zeichenketten Ihrer Anwendung übersetzt werden können, müssen sie aus dem Quellcode extrahiert werden.</p>

  <p>Schließen Sie Meldungen oder <em>string literals</em> in Ihrem Code mit dem Makro '<code>_()</code>' ein.</p>

  <note>
    <p>Für C ist dieses Makro in der Header-Datei <file>glib/gi18n.h</file> definiert, das in der obersten Ebene der Quelltexte Ihrer Anwendung enthalten sein muss.</p>
  </note>

  <p>Die gekennzeichneten Zeichenketten sollten folgendermaßen aussehen:</p>
  <code>_("Press a key to continue")</code>

  <p>Dadurch werden Zeichenketten als übersetzbar markiert und die Laufzeitumgebung ruft <app>gettext</app> auf, um die übersetzten Zeichenketten zu ersetzen.</p>

</page>
