<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-network-manager" xml:lang="el">

  <info>
    <link type="guide" xref="tech" group="network-manager"/>

    <credit type="author copyright">
      <name>Federico Mena Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Διαχείριση συνδέσεων δικτύου και παρακολούθηση κατάστασης με σύνδεση/χωρίς σύνδεση</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Διαχειριστής δικτύου (NetworkManager)</title>

  <p>Ο NetworkManager διαχειρίζεται τις συνδέσεις δικτύου του υπολογιστή. Φροντίζει τη διαπραγμάτευση DHCP για τη λήψη μιας διεύθυνσης IP για τον υπολογιστή, όταν η δικτύωσή του πρωτοενεργοποιείται. Επιτρέπει στους χρήστες να επιλέξουν μεταξύ διαφορετικών ενσύρματων και ασύρματων δικτύων, ρυθμίζει τα εικονικά ιδιωτικά δίκτυα (VPNs) και συνδέει το δίκτυο με μόντεμ.</p>

  <p>Ο NetworkManager παρέχει εκτεταμένη API που επιτρέπει τον έλεγχο εφαρμογών των συνδέσεων δικτύου. Όμως, αυτό είναι κυρίως ενδιαφέρον μόνο για το λογισμικό που υλοποιεί τον ίδιο τον πυρήνα της επιφάνειας εργασίας. Οι κανονικές εφαρμογές μπορούν να χρησιμοποιήσουν τη API NetworkManager για να παρακολουθήσουν την κατάσταση με/χωρίς σύνδεση του υπολογιστή και να εκτελέσουν άλλες υψηλού επιπέδου εργασίες σχετικές με το δίκτυο.</p>

  <p>Ο φλοιός επιφάνειας εργασίας του πυρήνα στο GNOME έχει ένα εμφανές εικονίδιο NetworkManager· εσωτερικά χρησιμοποιεί τη API του NetworkManager για να αλλάξει τις ρυθμίσεις δικτύου με βάση τις επιλογές χρήστη. Εφαρμογές όπως η Evolution, που χρειάζεται να ξέρει για την κατάσταση με/χωρίς σύνδεση του υπολογιστή, χρησιμοποιούν τον NetworkManager επίσης.</p>

  <list style="compact">
    <item><p><link href="https://wiki.gnome.org/Projects/NetworkManager">Αρχική σελίδα NetworkManager</link></p></item>
    <item><p><link href="https://wiki.gnome.org/Projects/NetworkManager/Developers">Αναφορά API NetworkManager</link></p></item>
  </list>


</page>
