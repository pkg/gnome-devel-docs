<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" style="task" id="overview-media" xml:lang="el">
  <info>
    <link type="guide" xref="index" group="media"/>
    <revision version="0.1" date="2013-06-19" status="draft"/>

    <credit type="author copyright">
      <name>David King</name>
      <email its:translate="no">davidk@gnome.org</email>
      <years>2013</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Αναπαραγωγή ήχου και βίντεο πολλών μορφών καθώς και επεξεργασία, ροή από τον ιστό και υποστήριξη δικτυακής κάμερας.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2010-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012-2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Τζένη Πετούμενου</mal:name>
      <mal:email>epetoumenou@gmail.com</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μάριος Ζηντίλης</mal:name>
      <mal:email>m.zindilis@dmajor.org</mal:email>
      <mal:years>2010</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκυδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gnome.org</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

<title>Πολυμέσα</title>

<list>
 <item>
  <p><em style="strong">Αναπαραγωγή και εγγραφή πλήθους μορφών ήχου και βίντεο</em></p>
 </item>
 <item>
  <p><em style="strong">Πρόσβαση διακτυακών καμερών και άλλων συσκευών μέσων συνδεμένων με το σύστημα</em></p>
 </item>
 <item>
  <p><em style="strong">Κοινόχρηστα και μέσα ροής με απομακρυσμένες συσκευές</em></p>
 </item>
</list>

<p>Προσθέστε πολυμέσα στην εφαρμογή σας, έτσι ώστε οι χρήστες να μπορούν εύκολα να εκτελέσουν το περιεχόμενό τους. Καταναλώστε και μοιραστείτε περιεχόμενο με άλλες συσκευές προσαρτημένες σε ένα σύστημα ή απομακρυσμένα μέσα από το δίκτυο. Το υποκείμενο χαμηλού επιπέδου API είναι διαθέσιμο αν χρειαστείτε περισσότερο έλεγχο.</p>

<media type="image" mime="image/png" src="media/totem-screenshot.png" width="65%">
 <p>Βίντεο</p>
</media>

<section id="what">
 <title>Τι μπορείτε να κάνετε;</title>

  <p>Τα <em style="strong">Πολυμέσα</em> στο GNOME δομούνται στο πλαίσιο <em style="strong"><link xref="tech-gstreamer">GStreamer</link></em>. Με το GStreamer, μπορούν να δημιουργηθούν ευέλικτες <em>διοχετεύσεις</em> πολυμέσων, από μια απλή αναπαραγωγή ήχου και βίντεο σε μια σύνθετη μη γραμμική επεξεργασία.</p>

  <p>Το GStreamer χρησιμοποιεί το <em style="strong"><link xref="tech-pulseaudio">PulseAudio</link></em> όταν εξάγει ήχο και συνεπώς μπορεί να στοχεύει πολλούς τύπους υλικού εξόδου. Το PulseAudio επίσης διαχειρίζεται δυναμικά κάθε αλλαγή εξόδου και ελέγχει την έντασης μιας συγκεκριμένης εφαρμογής.</p>

  <p>Για <em style="strong">δικτυακές κάμερες</em>, χρησιμοποιήστε το <em style="strong">Cheese</em>. Παρέχει μια απλή διεπαφή για δικτυακές κάμερες συνδεμένες με το σύστημα και έναν εύκολο τρόπο για να προσθέσετε έναν επιλογέα εικόνας στην εφαρμογή σας.</p>

  <p>Χρησιμοποιήστε το <em style="strong">Rygel</em> για να <em style="strong">μοιραστείτε περιεχόμενο μέσα από το δίκτυο</em> σε συσκευές όπως τηλεοράσεις και κονσόλες παιχνιδιών. Το Rygel χρησιμοποιεί <em style="strong"><link xref="tech-gupnp">GUPnP</link></em> από κάτω, που είναι ένα χαμηλού επιπέδου API για πρόσβαση περιεχομένου με πρωτόκολλα <em style="strong">UPnP</em>.</p>

  <p>Για απλούς <em style="strong">ήχους συμβάντων</em>, όπως ο ήχος διαφράγματος όταν παίρνετε μια φωτογραφία, χρησιμοποιήστε το <em style="strong"><link xref="tech-canberra">libcanberra</link></em>, που υλοποιεί τις προδιαγραφές θέματος ήχου του freedesktop.org.</p>

</section>

<!-- TODO: Link to code examples if they are moved to the platform overview
<section id="samples">
 <title>Code samples</title>
 <list>
  <item><p>A sample we should write</p></item>
  <item><p><link xref="samples#integration">More...</link></p></item>
 </list>
</section>
-->

<section id="realworld">
 <title>Πραγματικά παραδείγματα</title>

  <p>Μπορείτε να δείτε πολλές πραγματικές εφαρμογές των τεχνολογιών πολυμέσων του GNOME σε έργα ανοικτού λογισμικού, όπως τα παραδείγματα που δίνονται παρακάτω.</p>
  <list>
    <item>
      <p>Το <em style="strong">Βίντεο</em> είναι ο αναπαραγωγός πολυμέσων GNOME.</p>
      <p>( <link href="https://wiki.gnome.org/Apps/Videos">Website</link> | <link href="https://gitlab.gnome.org/GNOME/totem/blob/master/data/appdata/ss-videos.png">Screenshot</link> | <link href="https://gitlab.gnome.org/GNOME/totem/">Source code</link> )</p>
    </item>
    <item>
      <p>Το <em style="strong">PiTiVi</em> είναι ένας μη γραμμικός επεξεργαστής βίντεο, που κάνει εκτεταμένη χρήση του GStreamer.</p>
      <p>( <link href="http://www.pitivi.org/">Website</link> | <link href="http://www.pitivi.org/?go=tour">Screenshot</link> | <link href="http://www.pitivi.org/?go=download">Source code</link> )</p>
    </item>
 </list>
</section>
</page>
