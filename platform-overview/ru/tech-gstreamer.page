<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gstreamer" xml:lang="ru">

  <info>
    <link type="guide" xref="tech" group="gstreamer"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Playing, mixing, and manipulating sound and video</desc>
  </info>

<title>GStreamer</title>

<p>GStreamer — мощная мультимедийная библиотека для воспроизведения, создания и манипулирования звуком и видео. С помощью GStreamer можно проигрывать звук и видео, записывать их из различных источников, а также редактирование содержимое. GStreamer по умолчанию поддерживает кодирование и декодирование во множестве форматов, а поддержка дополнительных форматов может быть добавлена с помощью подключаемых модулей.</p>

<p>GStreamer имеет гибкую архитектуру в которой данные обрабатываются как поток через элементы системы. Каждый элемент может наложить фильтры на содержимое, например кодировать или декодировать его, наложить несколько источников или трансформировать содержимое мультимедиа. Подобная архитектура допускает разнообразное сочетание элементов, реализующее практически любой эффект. Более того, GStreamer спроектирован таким образом, чтобы иметь низкие накладные расходы, поэтому может быть использован в приложениях с высокими требованиями к задержкам.</p>

<p>Кроме мощного API для манипулирования медиа-содержимым, GStreamer также предоставляет удобные инструменты для простого воспроизведения. GStreamer может автоматически создавать потоки для чтения и воспроизведения файлов любых поддерживаемых форматов, что позволяет легко использовать звук и видео в приложениях.</p>

<p>Архитектура GStreamer позволяет с помощью подключаемых модулей добавлять кодеки, декодеры и фильтры содержимого любых типов. Сторонние разработчики могут создавать подключаемые модули GStreamer, которые автоматически станут доступными другим приложениям, использующим GStreamer. Подключаемые модули могут обеспечивать поддержку дополнительных форматов или добавлять новую функциональность и эффекты.</p>

<p>В приложении следует использовать GStreamer если необходимо прочитать или воспроизвести мультимедиа содержимое, а также для манипуляций со звуком и видео. Использование GStreamer облегчает разработку приложений и обеспечивает хорошо протестированными элементами для множества целей.</p>

<list style="compact">
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/index.html">The GStreamer Application Development Manual</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/">The GStreamer 1.0 Core Reference Manual</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org/documentation/">The GStreamer documentation page</link></p></item>
  <item><p><link href="http://gstreamer.freedesktop.org">The GStreamer web site</link></p></item>
</list>

</page>
