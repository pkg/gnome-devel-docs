<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="tech-gdk" xml:lang="ko">

  <info>
    <link type="guide" xref="tech" group="gdk"/>
    <revision pkgversion="3.0" date="2011-04-05" status="candidate"/>

    <credit type="author copyright">
      <name>Shaun McCance</name>
      <email its:translate="no">shaunm@gnome.org</email>
      <years>2011–2012</years>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>윈도우 시스템용 저수준 추상</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2016, 2017, 2018.</mal:years>
    </mal:credit>
  </info>

<title>GDK</title>

  <p>GDK is the low-level library used by <link xref="tech-gtk">GTK</link> to
  interact with the windowing system for graphics and input devices. Although
  you will rarely use GDK directly in application code, it contains all the
  necessary functionality to create low-level windows in the screen and to
  interact with the user with various input devices. GDK acts as an abstraction
  over various windowing systems, so that GTK can be portable to all of them:
  the X Window System (X11), Microsoft Windows, Mac OS X Quartz.</p>

<p>GDK enables you to access events from keyboards, mice, and other
input devices.  Implementations of widgets in GTK use this functionality, and
translate the events into higher-level signals that can in turn be used from
application code.  For example, a <code>GtkButton</code> widget will track
<code>GDK_BUTTON_PRESS</code> and <code>GDK_BUTTON_RELEASE</code> events,
which come from the mouse, and translate them as appropriate into a
<code>GtkButton::clicked</code> signal when the user presses and releases
the button in the right location.</p>

<p>GDK는 끌어다 놓기 및 시스템의 클립보드 데이터에 접근할 저수준 루틴을 제공합니다. 개별 컨트롤을 구현할 때 적당한 사용자의 상호작용 행동에 따른 반응을 구현하려면 이 기능에 접근해야합니다.</p>

<p>GDK provides other functionality which is needed to implement
a complete graphical toolkit like GTK.  Since GDK acts as a platform
abstraction, allowing GTK to run under multiple environments, it
provides an API for all of the system functionality needed by GTK.
This includes information about multi-head displays, resolution and
color depth, colormaps, and cursors.</p>

  <p>You should use GDK whenever you need low-level access to the underlying
  windowing system, including low-level access to events, windows, and the
  clipboard. Using GDK for these tasks ensures that your code is portable and
  integrates with the rest of your GTK code. The simple drawing routines in
  GDK should generally not be used; these are a left-over from when GDK simply
  wrapped the windowing system's drawing primitives. Instead, you should use
  the extensive functionality provided by <link xref="tech-cairo">Cairo</link>
  to draw high-quality 2D graphics.</p>

<list style="compact">
  <item><p><link href="https://developer.gnome.org/gdk3/stable/">GDK 참고 설명서</link></p></item>
</list>
</page>
