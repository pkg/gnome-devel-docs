<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="index" xml:lang="de">

  <info>
    <credit type="author copyright">
      <name>Federico Mena-Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Miguel de Icaza</name>
      <email its:translate="no">miguel@gnome.org</email>
    </credit>
    <credit type="author copyright">
      <name>Morten Welinder</name>
      <email its:translate="no">mortenw@gnome.org</email>
    </credit>
    <credit type="author copyright">
      <name>Philip Withnall</name>
      <email its:translate="no">philip.withnall@collabora.co.uk</email>
      <years>2015</years>
    </credit>
    <credit type="publisher">
      <name>GNOME Foundation</name>
      <page xmlns="http://xmlns.com/foaf/0.1/">http://foundation.gnome.org/</page>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>GNOME Programmierungsrichtlinien</desc>

    <title type="link" role="trail">Programmierrichtlinien</title>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2016, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Stefan Melmuk</mal:name>
      <mal:email>stefan.melmuk@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>GNOME Programmierungsrichtlinien</title>

  <p>Dieser Artikel enthält einige Richtlinien und Vorschläge für Programmierer, die im und mit dem GNOME-Stack arbeiten. Ziel ist es, Entwickler mit den Prozessen, Konventionen und Philosophien hinter den GNOME-Anwendungen und den zugehörigen Bibliotheken vertraut zu machen. Die Hoffnung ist, dass Programmierer, die wissen »wie Dinge hier funktionieren« im GNOME-Ökosystem, sich besser mit den GNOME-APIs auseinandersetzen und neue Anwendungen entwickeln können, und damit letztlich lesbaren Code schreiben, der über lange Zeit von einem diversen Team an Programmierern verwaltet werden kann.</p>

  <p>Diese Richtlinie verfolgt zweierlei Zweck:</p>

  <list>
    <item>
      <p>Wir wollen Ihnen praxisgerechte Empfehlungen für die Programmierung für GNOME oder mittels GNOME-Technologien geben. Dies wird Ihnen helfen, konsistenten Code zu schreiben, der von der Gemeinschaft akzeptiert wird. Als Nebeneffekt wird Ihnen die Programmierung so auch mehr Spaß machen.</p>
    </item>

    <item>
      <p>Wir wollen unser über die Jahre gewonnenes Wissen weitervermitteln, wie das GNOME-Projekt nachhaltig geführt wird, auch wenn die Beiträge der Mitwirkenden schwanken.</p>
    </item>
  </list>

  <section id="general-guidelines">
    <info>
      <title type="link" role="trail">Allgemeine Richtlinien</title>
    </info>
    <title>Allgemeine Richtlinien</title>
    <links type="topic" groups="general-guidelines" style="grid"/>
  </section>

  <section id="maintainer-guidelines">
    <info>
      <title type="link" role="trail">Maintainer-Richtlinien</title>
    </info>
    <title>Maintainer-Richtlinien</title>
    <links type="topic" groups="maintainer-guidelines" style="grid"/>
  </section>

  <section id="specific-how-tos">
    <info>
      <title type="link" role="trail">Spezifische How-Tos</title>
    </info>
    <title>Spezifische How-Tos</title>
    <links type="topic" groups="specific-how-tos" style="grid"/>
  </section>

  <section id="references">
    <info>
      <title type="link" role="trail">Referenzen</title>
    </info>
    <title>Referenzen</title>
    <links type="topic" groups="references" style="grid"/>
  </section>

</page>
