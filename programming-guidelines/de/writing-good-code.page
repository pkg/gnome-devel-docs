<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="writing-good-code" xml:lang="de">

  <info>
    <link type="guide" xref="index#general-guidelines"/>
    
    <credit type="author copyright">
      <name>Federico Mena-Quintero</name>
      <email its:translate="no">federico@gnome.org</email>
      <years>2013</years>
    </credit>
    <credit type="author copyright">
      <name>Miguel de Icaza</name>
      <email its:translate="no">miguel@gnome.org</email>
    </credit>
    <credit type="author copyright">
      <name>Morten Welinder</name>
      <email its:translate="no">mortenw@gnome.org</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="cc-by-sa-3-0.xml"/>

    <desc>Guter, lesbarer Code erhält die Wartungsfähigkeit des Projekts</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2016, 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Stefan Melmuk</mal:name>
      <mal:email>stefan.melmuk@gmail.com</mal:email>
      <mal:years>2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Tim Sabsch</mal:name>
      <mal:email>tim@sabsch.com</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Die Bedeutung von gutem Code</title>

  <p>GNOME ist ein ambitioniertes Freies Softwareprojekt und besteht aus vielen Softwarepaketen, die mehr oder weniger unabhängig voneinander sind. Ein beträchtlicher Anteil der Arbeit in GNOME wird von Freiwilligen übernommen. Auch wenn viele Menschen in Voll- oder Teilzeit an GNOME arbeiten, machen Freiwillige dennoch einen Großteil an den Mitwirkenden aus. Programmierer kommen und gehen wieder, und widmen GNOME unterschiedlich viel Zeit. Das »echte Leben« der Menschen ändert sich vielleicht und wirkt sich darauf aus, viele Zeit sie in GNOME stecken können und wollen.</p>

  <p>Softwareentwicklung erfordert viel Zeit und Mühe. Daher können Teilzeit-Mitwirkende nur selten alleine große Projekte starten. Es ist viel einfacher und belohnender, wenn sie zu bestehenden Projekten beitragen, weil dies schnell zu sichtbaren und nützlichen Ergebnissen führt.</p>

  <p>Daraus schlussfolgern wir, dass bestehende Projekte es so einfach wie möglich für Menschen machen müssen, an ihnen mitzuwirken. Ein Teilaspekt davon ist, sicherzustellen, dass die Programme einfach zu lesen, verstehen, ändern und warten sind.</p>

  <p>Unordentlicher Code ist schwer zu lesen und führt dazu, dass Menschen das Interesse an ihm verlieren, wenn sie ihn nicht verstehen können. Es ist weiterhin wichtig, dass Programmierer den Code verstehen können, damit sie bereits nach kurzer Zeit durch Fehlerbehebungen und Verbesserungen mitwirken können. Quellcode ist eine Form von <em>Kommunikation</em> und für Menschen gedacht - nicht für Rechner. So wie Sie vermutlich keinen Roman mit lauter Rechtschreibfehlern, schlechter Grammatik und nachlässig gesetzter Interpunktion lesen möchten, sollten auch Programmierer guten Code schreiben, der leicht von anderen Leuten verstanden und bearbeitet werden kann.</p>

  <p>Im folgenden finden Sie einige wichtige Qualitätsmerkmale von gutem Code:</p>

  <terms>
    <item>
      <title>Übersichtlichkeit</title>
      <p>Übersichtlicher Code ist mit geringem Aufwand leicht zu lesen. Dritte können ihn einfach verstehen. Darunter fällt der Programmierstil selbst (Platzierung von geschweiften Klammern, Einrückung, Variablennamen) und der eigentliche Kontrollfluss im Code.</p>
    </item>

    <item>
      <title>Konsistenz</title>
      <p>Konsistenter Code erlaubt zu verstehen, wie ein Programm funktioniert. Beim Lesen von konsistentem Code erstellt man unbewusst einige Annahmen und Erwartungen, wie der Code funktioniert, sodass man leichter Änderungen einpflegen kann. Code, der an unterschiedlichen Orten gleich <em>aussieht</em>, sollte auch gleich <em>funktionieren</em>.</p>
    </item>

    <item>
      <title>Erweiterbarkeit</title>
      <p>Allgemeiner Code ist einfacher wiederzuverwenden und zu ändern als sehr spezifischer Code mit vielen hartcodierten Annahmen. Wenn jemand einem Programm eine neue Funktion hinzufügen möchte, ist dies natürlich einfacher, wenn der Code von Anfang an erweiterbar ist. Code, der nicht auf diese Weise geschrieben wurde, kann dazu führen, dass Menschen hässliche Hacks implementieren müssen, um Funktionen hinzuzufügen.</p>
    </item>

    <item>
      <title>Korrektheit</title>
      <p>Korrekter Code führt dazu, dass man weniger Zeit mit Fehlersuche verbringt, die man stattdessen in neue Funktionsmerkmale stecken kann. Auch Nutzer schätzen korrekten Code, denn niemand mag Programmabstürze. Code, der auf Korrektheit und Stabilität ausgelegt ist (also Code, der explizit versucht sicherzustellen, dass der Programmstatus erhalten bleibt), verhindert alle möglichen ärgerlichen Fehler.</p>
    </item>
  </terms>

  <section id="book-references">
    <title>Bücher</title>

    <list>
      <item><p><link href="http://www.cc2e.com">Code Complete</link> von Steve McConnell.</p></item>
      <item><p><link href="http://martinfowler.com/books/refactoring.html"> Refactoring: Improving the Design of Existing Code </link> von Martin Fowler.</p></item>
      <item><p><link href="https://de.wikipedia.org/wiki/Entwurfsmuster_(Buch)"> Entwurfsmuster. Elemente wiederverwendbarer objektorientierter Software </link> von Erich Gamma, Richard Helm, Ralph Johnson und John Vlissides.</p></item>
      <item><p><link href="http://astore.amazon.com/gnomestore-20/detail/020163385X"> Object-Oriented Design Heuristics </link> von Arthur Riel.</p></item>
    </list>
  </section>
</page>
