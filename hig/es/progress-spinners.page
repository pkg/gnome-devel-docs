<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="progress-spinners" xml:lang="es">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2021</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alex Puchades</mal:name>
      <mal:email>alex94puchades@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gnome.org</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Benjamín Valero Espinosa</mal:name>
      <mal:email>benjavalero@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2009</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rosa Elena Pérez</mal:name>
      <mal:email>rozydicemiau@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Oscar Rafael Arachi</mal:name>
      <mal:email>arachi910@gmail.com</mal:email>
      <mal:years>2014 - 2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Miguel Ajuech</mal:name>
      <mal:email>miguel.ann.28@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Adrián Pérez Domínguez</mal:name>
      <mal:email>adrian@aztli.org</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Celina Osorio Ochoa</mal:name>
      <mal:email>chelinoska@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Dario Amigon Espinoza</mal:name>
      <mal:email>daramigon@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Yenisei Ramírez Pérez</mal:name>
      <mal:email>yeniseirape@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Roberto Ramíres Mendoza</mal:name>
      <mal:email>aczella@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bernardo Sánchez Romero</mal:name>
      <mal:email>b.sanchez.rom@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Emmanuel Márquez Juárez</mal:name>
      <mal:email>resident3333@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>David Romero Serrano</mal:name>
      <mal:email>lemi136@hotmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Omar Garcia Guzman</mal:name>
      <mal:email>garciag.omar91@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Edilberto Huerta Niño</mal:name>
      <mal:email>edilberto.huerta0290@gmail.com</mal:email>
      <mal:years>2015</mal:years>
    </mal:credit>
  </info>

<title>Indicadores de progreso</title>

<p>Los indicadores de progreso son un elemento común de la interfaz de usuario para indicar el progreso de una tarea. A diferencia de las barras de progreso, sólo indican el progreso que tiene lugar, y no muestran la proporción de la tarea que se ha completado.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/progress-spinner.svg"/>

<section id="when-to-use">
<title>Cuándo usarlas</title>

<p>Generalmente se necesita un indicador de progreso cuando una operación tarda más de tres segundos, y es necesario indicar que la operación se está realizando. De otro modo, el usuario puede dudar de si ha ocurrido un error o no.</p>

<p>Al mismo tiempo, las indicaciones de progreso son una potencial fuente de distracción, especialmente cuando se muestran durante cortos de períodos de tiempo. Si una operación tarda menos de 3 segundos, es preferible evitar el uso de un indicador de progreso incremental, ya que los elementos animados que se muestran durante muy poco tiempo pueden degradar la experiencia global del usuario.</p>

<p>Los indicadores incrementales no muestra gráficamente el grado de progreso de una tarea, y a menudo son más adecuados para operaciones cortas. Si la tarea va a durar habitualmente más de un minuto, una <link xref="progress-bars">barra de progreso</link> puede ser una mejor elección.</p>

<p>La forma de los indicadores de progreso incrementales también afecta su idoneidad para diferentes situaciones. Dado que son efectivos en tamaños pequeños, los indicadores se pueden incorporar fácilmente en elementos pequeños de la interfaz de usuario, como listas o barras de cabecera. Del mismo modo, su forma significa que pueden ser eficaces si se integran dentro de contenedores cuadrados o rectangulares. Por otro lado, si el espacio vertical es limitado, una barra de progreso puede ser una mejor elección.</p>

</section>

<section id="general-guidelines">
<title>Guías Generales</title>

<list>
<item><p>Si una operación puede variar el tiempo que tarda, use una cuenta atrás para mostrar un indicador de progreso sólo cuando hayan pasado los tres segundos. Este indicador no es necesario para tiempos por debajo de este valor.</p></item>
<item><p>Coloque los indicadores de progreso junto a o dentro de otros elementos de la interfaz del usuario relacionados. Si un botón desencadena una operación larga, el indicador se puede colocar junto al botón, por ejemplo. Al cargar contenido, el indicador se debe colocar dentro del área en la que aparecerá en contenido.</p></item>
<item><p>Generalmente, sólo se debe mostrar un indicador de progreso incremental a la vez. Evite mostrar muchos indicadores a la vez, ya que esto puede ser muy cargante visualmente.</p></item>
<item><p>Se puede mostrar una etiqueta junto a un botón incremental si es útil para aclarar la tarea al a que se refiere el botón.</p></item>
<item><p>Si es muestra un botón incremental durante mucho tiempo, una etiqueta puede indicar tanto la identidad de la tarea como el proceso que realiza.Esto puede ser en forma de porcentaje, de indicación del tiempo restante o del progreso de las subtareas (ej. módulos descargados o páginas exportadas)</p></item>
</list>

</section>

<section id="api-reference">
<title>Referencia de la API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSpinner.html">GtkSpinner</link></p></item>
</list>
</section>

</page>
