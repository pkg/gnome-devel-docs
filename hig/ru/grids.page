<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="grids" xml:lang="ru">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Grids of thumbnails or icons</desc>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Сетки</title>

<media type="image" mime="image/svg" src="figures/patterns/grid.svg"/>

<p>Сетка это один из основных способов, позволящих представить набор данных в GNOME 3. Сетки часто применяются вместе с различными шаблонами проектирования, включая <link xref="search">поиск</link> и <link xref="selection-mode">режим выделения</link>.</p>

<section id="when-to-use">
<title>Когда использовать</title>

<p>Поскольку в сетках в качестве элементов используются изображения, сетки лучше всего подходят под визуальное содержимое, например документы или фотоснимки. Если содержимое не имеет визуального представления, вместо сеток можно применять <link xref="lists">списки</link>.</p>

<p>Чтобы предоставить несколько различных режимов просмотра содержимого, сетки можно одновременно использовать со списками. Это может быть пригодиться, когда элементы содержимого имеют дополнительные метаданные (например, дата создания, автор и т. п.).</p>

</section>

<section id="general-guidelines">
<title>Общие рекомендации</title>

<list>
<item><p>Каждый элемент содержимого по возможности должен иметь уникальную миниатюру.</p></item>
<item><p>Располагайте элементы в сетке в таком порядке, который был бы наиболее полезен для пользователей вашего приложения. Расположение элементов по частоте их использование часто является наиболее удачным вариантом упорядочивания.</p></item>
<item><p>Выбор элемента в сетке обычно переводит режим просмотра на просмотр этого элемента.</p></item>
<item><p>Рассмотрите возможность объединения в сетке поиска, режима выделения и коллекций.</p></item>
</list>

</section>

<section id="api-reference">
<title>API reference</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkFlowBox.html">GtkFlowBox</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkIconView.html">GtkIconView</link></p></item>
</list>
</section>

</page>
