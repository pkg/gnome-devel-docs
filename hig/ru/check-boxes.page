<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="check-boxes" xml:lang="ru">

  <info>
    <credit type="author">
      <name>Алан Дэй (Allan Day)</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Калум Бенсон (Calum Benson)</name>
    </credit>
    <credit>
      <name>Адам Элман (Adam Elman)</name>
    </credit>
    <credit>
      <name>Сэт Никел (Seth Nickell)</name>
    </credit>
    <credit>
      <name>Колин Робертсон (Colin Robertson)</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/check-boxes.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Флажки</title>

<p>Флажки используются для отображения или изменения параметра. Флажок имеет два состояния, включён и выключен, которые можно определить по наличию галочки.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/check-boxes.svg"/>

<section id="guidelines">
<title>Рекомендации</title>

<list>
<item><p>Нажатие на флажок не должно влиять на значения в других управляющих элементах. Однако флажок можно управлять чувствительностью или видимостью управляющих элементов.</p></item>
<item><p>Если переключение флажка влияет на видимость других управляющих элементов, флажок должен быть расположен выше или левее управляющих элементов, на которые он влияет. Это показывает, что управляющие элементы зависят от состояния флажка.</p></item>
<item><p>При проектировании интерфейса на английском языке придерживайтесь <link xref="writing-style#capitalization">правла использования прописных букв в предложениях</link>, например <gui>Use custom font</gui>.</p></item>
<item><p>Текстовая метка флажка должна чётко указывать на то, что происходит при изменении состояния флажка. Например, <gui>Показывать значки в меню</gui>. Если вы затрудняетесь в описании состояния, попробуйте вместо флажка использовать пару радио-кнопок.</p></item>
<item><p>Избегайте применения отрицаний в текстовых метках, поскольку отрицания сложнее для восприятия. Например, <gui>Включить предупредительный сигнал</gui> лучше, чем <gui>Выключить предупредительный сигнал</gui>.</p></item>
<item><p>Применяйте <link xref="keyboard-input#access-keys">ускорители</link> во всех текстовых метках флажков, чтобы пользователь мог изменять состояние флажков с клавиатуры.</p></item>
<item><p>Если флажок отображает параметр нескольких выделенных объектов, который установлен для некоторых объектов, а для некоторых не установлен, показывайте флажок в смешанном состоянии. Когда флажок находится в смешанных состоянии:</p>
<list>
<item><p>Однакратное нажатие на флажок должно устанавливать флажок, применяя этот параметр для всех выделенных объектов.</p></item>
<item><p>Второе нажатие на флажок снимает флажок, убирая этот параметр со всех выделенных объектов.</p></item>
<item><p>Третье нажатие на флажок должно возвращать флажок в смешанное состояние, восстанавливая этот исходное значение параметра для всех выделенных объектов.</p></item>
</list></item>
<item><p>Именуйте группы флажков с помощью заголовков, которые должны располагаться над группой или слева от неё.</p></item>
<item><p>Не помещайте больше восьми флажков под один и тот же заголовок. Если вам нужно больше восьми флажков, используйте пустые строки или заголовки, чтобы разбить группу на более мелкие. Или попробуйте использовать список из флажков. Также рассмотрите альтернативные варианты упрощения интерфейса.</p></item>
<item><p>Старайтесь располагать группы флажков вертикально, а не горизонтально, это упрощает их восприятие. Используйте только горизонтальные или прямоугольные выравнивание, если они заметно улучшают расположение элементов.</p></item>
</list>
</section>

<section id="api-reference">
<title>API reference</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkCheckButton.html">GtkCheckButton</link></p></item>
</list>
</section>

</page>
