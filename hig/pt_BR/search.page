<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="search" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Localize e filtre conteúdo digitando</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Pesquisa</title>

<media type="image" mime="image/svg" src="figures/patterns/search.svg"/>

<p>A pesquisa permite que itens de conteúdo sejam encontrados filtrando o conteúdo exibido na tela. É diferente de localizar, que envolve mover ou destacar o conteúdo que está sendo pesquisado, em vez de filtrar.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>Forneça pesquisa sempre que uma grande coleção de conteúdo é apresentada e esses itens de conteúdo têm um componente textual. Isso pode ser uma coleção de itens de conteúdo reais, como documentos, contatos ou vídeos, ou uma lista de opções.</p>

<p>A pesquisa é uma ótima maneira de tornar mais fácil para os usuários encontrarem o que estão procurando, e sua disponibilidade consistente significa que os usuários podem confiar e esperar que ela esteja presente.</p>

<p>No entanto, embora a pesquisa possa ser altamente eficaz e alguns usuários a usem, outros não. Portanto, tente suplementar outros meios para encontrar itens de conteúdo com pesquisa, em vez de confiar apenas nela.</p>

</section>

<section id="search-bar">
<title>A barra de pesquisa</title>

<p>O padrão para pesquisa no GNOME 3 utiliza uma barra de pesquisa especial que desliza abaixo da barra de cabeçalho. Nas janelas primárias, a barra de pesquisa fica normalmente oculta até ser ativada pelo usuário. Existem três maneiras comuns de ativar a pesquisa nesse contexto:</p>

<list>
<item><p>Digitar quando um campo de entrada de texto não está focalizado deve ativar a pesquisa, e o texto inserido deve ser adicionado ao campo de pesquisa. Isso é chamado de “digite para pesquisar”.</p></item>
<item><p>O atalho de teclado para pesquisar (<keyseq><key>Ctrl</key><key>F</key></keyseq>).</p></item>
<item><p>Um botão de pesquisa na barra de cabeçalho deve permitir que a barra de pesquisa seja exibida (o botão de pesquisa deve alternar).</p></item>
</list>

<p>Se a pesquisa for um método primário para encontrar conteúdo em seu aplicativo, você poderá tornar a barra de pesquisa permanentemente visível ou visível quando o aplicativo for iniciado pela primeira vez.</p>

</section>

<section id="search-results">
<title>Resultados de pesquisa</title>

<list>
<item><p>A pesquisa deve ser “ao vivo” sempre que possível – a exibição de conteúdo deve ser atualizada para exibir os resultados da pesquisa à medida que são inseridos.</p></item>
<item><p>Para ser eficaz, é importante que os resultados da pesquisa sejam retornados rapidamente.</p></item>
<item><p>Se um termo de pesquisa não retornar nenhum resultado, assegure-se de que o feedback seja fornecido na visualização de conteúdo. Geralmente, um simples rótulo “No results” é suficiente.</p></item>
</list>

</section>

<section id="additional-guidance">
<title>Orientação adicional</title>

<list>
<item><p>Seja tolerante com erros em termos de pesquisa. Correspondência de erros ortográficos ou de terminologia incorreta é uma maneira de fazer isso. Apresentar sugestões para correspondências semelhantes ou conteúdo relacionado é outra.</p></item>
<item><p>Permitir uma ampla gama de termos de pesquisa correspondentes. Isso ajuda as pessoas que não têm certeza do termo exato que precisam, mas que conhecem as características associadas ao item que desejam encontrar. Uma lista de cidades pode retornar correspondências por país ou região, por exemplo.</p></item>
<item><p>Os resultados devem ser ordenados de forma a garantir que os itens mais relevantes sejam exibidos primeiro.</p></item>
</list>

</section>

<section id="api-reference">
<title>Referência de API</title>
<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html">GtkSearchEntry</link></p></item>
</list>
</section>

</page>
