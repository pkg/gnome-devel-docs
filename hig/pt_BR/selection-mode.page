<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="selection-mode" xml:lang="pt-BR">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Padrão para seleção de itens de conteúdo</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Georges Neto</mal:name>
      <mal:email>georges.stavracas@gmail.com</mal:email>
      <mal:years>2014,</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2017-2021</mal:years>
    </mal:credit>
  </info>

<title>Modo de seleção</title>

<media type="image" mime="image/svg" src="figures/patterns/selection-mode.svg"/>

<p>O modo de seleção é um padrão de design para permitir que ações sejam executadas em itens de conteúdo. É normalmente usado em conjunto com listas e grades.</p>

<p>Quando o modo de seleção está ativo, as caixas de seleção permitem que os itens sejam selecionados e uma barra de ação é mostrada na parte inferior da exibição. Contém as várias ações que podem ser executadas nos itens de conteúdo selecionados.</p>

<section id="when-to-use">
<title>Quando usar</title>

<p>O modo de seleção é apropriado quando:</p>

<list>
<item><p>É comum realizar ações simultaneamente em vários itens de conteúdo.</p></item>
<item><p>Várias ações estão disponíveis para serem executadas nos itens de conteúdo.</p></item>
<item><p>É útil que os usuários possam realizar ações em itens de conteúdo sem abri-los.</p></item>
</list>

<p>Se for mais comum que os usuários executem ações em itens de conteúdo único, convém considerar outro padrão de design, como um menu de contexto. Da mesma forma, se houver apenas uma ação que possa ser executada nos itens de conteúdo, uma variante do modo de seleção pode ser usada (um botão sobreposto poderia permitir que a ação fosse executada diretamente nos itens, em vez de exigir que eles fossem selecionados primeiro, por exemplo).</p>

</section>

<section id="activating-selection-mode">
<title>Ativando o modo de seleção</title>

<p>A principal maneira de ativar o modo de seleção é através do botão do modo de seleção, que está localizado na barra de cabeçalho e que é identificado por um ícone de marca de seleção. O modo de seleção também pode ser ativado pela seleção de elástico, ou pressionando Ctrl ou Shift e clicando/pressionando itens de conteúdo.</p>

</section>

<section id="action-bar">
<title>A barra de ação</title>

<list>
<item><p>Os controles na barra de ação devem ser insensíveis quando nenhum item for selecionado. Às vezes, as ações só podem ser aplicadas a vários itens de conteúdo; nesse caso, os controles relevantes só devem ser sensíveis quando vários itens forem selecionados.</p></item>
<item><p>Controles na barra de ferramentas de ação podem ter ícones ou rótulos de texto.</p></item>
<item><p>Grupos de ações podem ser distinguidos colocando-os em qualquer extremidade da barra de ferramentas. Ações destrutivas, como excluir, devem ser colocadas longe de outros controles.</p></item>
</list>

</section>
</page>
