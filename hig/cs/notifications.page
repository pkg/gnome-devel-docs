<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="notifications" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Oznámení události v rámci celého systému</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Oznámení</title>

<p>Oznámení umožňují informovat uživatele o událostech, i když vaši aplikaci zrovna nepoužívá. Rovněž poskytují uživateli schopnost na tyto události rychle reagovat pomocí činností v oznámení.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Oznámení používejte k informování uživatele o události, která by jej mohla zajímat, i když vaši aplikaci zrovna nepoužívá. Mezi to patří například nová zpráva v komunikační aplikaci, dokončení dlouho běžící úlohy, připomenutí z kalendáře a podobné.</p>

<p>Oznámení by neměla být používána jako náhrada za zpětnou vazbu, kterou poskytují okna vaší aplikace, která by měla být schopná informovat uživatele o události bez potřeby oznámení.</p>

</section>

<section id="notification-elements">
<title>Prvky oznámení</title>

<p>Oznámení v GNOME 3 mají řadu standardních komponent:</p>

<table>
<thead>
<tr>
<td><p>Prvek</p></td><td><p>Popis</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Ikona aplikace</p></td><td><p>Ukazuje, která aplikace oznámení poslala.</p></td>
</tr>
<tr>
<td><p>Název</p></td><td><p>Název oznámení.</p></td>
</tr>
<tr>
<td><p>Tělo</p></td><td><p>Volitelný blok textu, který poskytuje další podrobnosti k oznámení. Může mít více odstavců. Například: úryvek ze začátku e-mailu.</p></td>
</tr>
<tr>
<td><p>Výchozí činnost</p></td><td><p>Jedná se o činnost, která se spustí, když je oznámení aktivováno.</p></td>
</tr>
<tr>
<td><p>Činnosti</p></td><td><p>Každé oznámení může zahrnovat až tři tlačítka.</p></td>
</tr>
</tbody>
</table>

<section id="titles">
<title>Názvy</title>

<p>Název by měl poskytovat krátké a stručné shrnutí popisu události, která oznámení vyvolala. Tělo oznámení nemusí být vždy viditelné, takže je důležité zajistit, že oznámení bude uživatelem pochopeno i ze samotného názvu.</p>

</section>

<section id="default-actions">
<title>Výchozí činnosti</title>

<p>Výchozí činnost by měla vždy zajistit odstranění oznámení a přenést do popředí okno patřící aplikaci, která oznámení vyslala. Když oznámení patří k nějaké konkrétní části uživatelského rozhraní vaší aplikace, měla by výchozí činnost zobrazit tuto část. Například výchozí činnost pro oznámení o novém e-mailu by měla zobrazit příslušnou e-mailovou zprávu.</p>

</section>

<section id="actions">
<title>Činnosti v oznámení</title>

<p>Užitečné funkce můžete poskytnout vložením tlačítek přímo do oznámení. Uživateli to umožní rychle a jednoduše na oznámení reagovat.</p>

<list>
<item><p>Nabídnuté činnosti by se měly vztahovat k obsahu oznámení a neměly by poskytovat obecné činnosti z vaší aplikace. To zajistí, že každé oznámení má jasný cíl a účel.</p></item>
<item><p>Činnosti v oznámení použijte jen když je funkce, kterou poskytují, obecně žádaná.</p></item>
<item><p>Činnosti by neměly nahradit ovládací prvky z uživatelského rozhraní aplikace. Stejnou akci musí být možné provést i z okna vaší aplikace.</p></item>
<item><p>Činnosti není nutné v oznámení použít vždy a řada oznámení je vůbec nepotřebuje.</p></item>
<item><p>Činnosti v oznámení by neměly duplikovat výchozí činnost. Například oznámení o novém e-mailu nepotřebuje mít tlačítko <gui>Open</gui> (<gui>Otevřít</gui>), protože to by měla udělat výchozí činnost.</p></item>
</list>

</section>
</section>

<section id="general-guidance">
<title>Obecné pokyny</title>

<list>
<item><p>Je důležité uživatele oznámeními zbytečně nevyrušovat. Velmi snadno se to totiž zvrhne v obtěžování a frustraci a uživatel pak bude mít tendenci si vaši aplikaci zprotivit. Proto při zavádění oznámení buďte vždy kritičtí a položte si otázku, jestli uživatel opravdu tak moc potřebuje být informován o události, kterou chcete sdělit.</p></item>
<item><p>Aplikace, které operují se spoustou událostí, například s e-maily nebo se zprávami ze sociálních sítí, riskují, že budou uživatele rušit velkým množstvím oznámení. Takovéto aplikace by měli zavést omezení, jak často budou oznámení zasílat. Místo aby zobrazovali oznámení pro každou jednotlivou novou zprávu, je lepší nápad posílat oznámení jako souhrn pro několik nových zpráv.</p></item>
<item><p>Oznámení v GNOME 3 přetrvávají i po té, co jsou poprvé zobrazena. Je proto důležité odstraňovat zprávy oznámení, které pro uživatele nemají nadále žádný význam.</p>
<list>
<item><p>Okno vaší aplikace by mělo poskytnou zpětnou vazbu u všech událostí, které byly oznámeny. Když je okno aplikace zaměřeno, je následkem, že se zprávou v oznámeních by mělo být zacházeno, jako by byla přečtena a měla by být odstraněna.</p></item>
<item><p>Zajistěte, aby vaše aplikace odstranila oznámení, která již neplatí. Například oznámení s varování o počasí, které bylo odvoláno, by mělo být odstraněno.</p></item>
</list></item>
</list>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>
<list>
<item><p><link href="https://developer.gnome.org/gio/stable/GNotification.html">GNotification</link></p></item>
</list>
</section>

</page>
