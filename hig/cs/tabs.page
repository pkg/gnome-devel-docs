<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="tabs" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/tabs.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Karty</title>

<p>Karty poskytují způsob, jak rozdělit okno do řady zobrazení. K dispozici jsou ve dvou formách: pevné a dynamické.</p>

<p>Pevné karty poskytují neměnnou sadu předdefinovaných zobrazení a jsou používány hlavně v dialogových oknech. Dynamické karty umožňují oknu obsáhnout proměnný výběr položek obsahu, jako jsou stránky, dokumenty nebo obrázky. Používají se především v <link xref="primary-windows">hlavních oknech</link>, jako součást editorů nebo prohlížecích aplikací.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/tabs.svg"/>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Pevné karty použijte, když <link xref="dialogs">dialogové okno</link> obsahuje příliš mnoho ovládacích prvků (nebo příliš mnoho informací), aby je mohlo podat pohodlnějším způsobem.</p>

<p>Dynamické karty jsou použitelné hlavně pro různé prohlížeče a editory, kde uživatel může chtít používat několik umístění nebo položek obsahu souběžně.</p>

</section>

<section id="fixed">
<title>Pevné karty</title>

<list>
<item><p>Nepoužívejte příliš mnoho karet. V případě, že nemůžete vidět všechny karty, aniž by se použil posuvník nebo se rozdělily do více řádků, používáte jich nejspíše příliš mnoho a měli byste místo nich použít ovládání pomocí seznamu.</p></item>
<item><p>V popiscích karet používejte pravidla pro <link xref="writing-style#capitalization">velká písmena v nadpisech</link> a používejte spíše podstatná jména než slovesa, například <gui>Font</gui> nebo <gui>Alignment</gui> (<gui>Písmo</gui>, <gui>Zarovnání</gui>). Pokuste se zachovat popisky krátké.</p></item>
<item><p>Nenavrhujte karty tak, aby změna v ovládacích prvcích na jedné kartě ovlivnila ovládací prvky na jiné kartě. Uživatelé si takovýchto závislostí těžko všimnou.</p></item>
<item><p>Pokud ovládací prvek ovlivňuje všechny karty, umístěte jej mimo karty.</p></item>
<item><p>U pevných karet ponechte šířku oušek proporční podle šířky popisku. Nesnažte se je nastavit všechny na stejnou šířku, protože to ztěžuje jejich přelétnutí očima a omezí to počet karet, které můžete zobrazit naráz bez potřeby posuvníku.</p></item>
</list>

</section>

<section id="dynamic">
<title>Dynamické karty</title>

<list>
<item><p>Ouška karet mají často omezenou šířku, takže zajistěte, aby popisky byly krátké a stručné, a aby nejpodstatnější část popisku byla na jeho začátku. Zajistěte, aby popisky zůstaly použitelné, i když jsou zkrácené výpustkem.</p></item>
<item><p>Když se obsah karty změní, nebo vyžaduje pozornost, může být zobrazena vizuální rada.</p></item>
<item><p>U každé karty poskytněte kontextovou nabídku. Ta by měla zahrnovat jen činnosti pro práci s kartou jako takovou, jako je <gui>Move Left</gui>, <gui>Move Right</gui>, <gui>Move to New Window</gui> a <gui>Close</gui> (<gui>Přesunout doleva</gui>, <gui>Přesunout doprava</gui>, <gui>Přesunout do nového okna</gui>, <gui>Zavřít</gui>).</p></item>
<item><p>Pokud jsou karty podstatnou součástí aplikace, můžete na hlavičkovou nebo nástrojovou lištu umístit tlačítko pro novou kartu. Nezobrazujte takové tlačítko v aplikacích, kde se karty nepoužívají pořád. Pak postačuje klávesová zkratka a/nebo položka v nabídce.</p></item>
</list>

<section id="keyboard-shortcuts">
<title>Standardní klávesové zkratky</title>

<p>Když používáte dynamické karty, zajistěte, aby byly podporovány standardní klávesové zkratky.</p>

<table>
<tr>
<td><p><keyseq><key>Ctrl</key><key>T</key></keyseq></p></td>
<td><p>Vytvořit novou kartu</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
<td><p>Zavřít aktuální kartu</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Page Up</key></keyseq></p></td>
<td><p>Přepnout na následující kartu</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Page Down</key></keyseq></p></td>
<td><p>Přepnout na předchozí kartu</p></td>
</tr>
</table>

</section>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkNotebook.html">GtkNotebook</link></p></item>
</list>

</section>
</page>
