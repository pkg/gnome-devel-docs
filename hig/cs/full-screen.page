<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="fullscreen" xml:lang="cs">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Režim, ve kterém okno zaplňuje celou obrazovku.</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Celá obrazovka</title>

<media type="image" mime="image/svg" src="figures/patterns/fullscreen.svg"/>

<p>Celá obrazovka je režim, ve kterém okno zaplní celou dostupnou obrazovku. Když je tento režim aktivní, ovládací prvky se obvykle skryjí, aby byl poskytnut ničím nerušený pohled na data.</p>

<section id="when-to-use">
<title>Kdy použít</title>

<p>Celou obrazovku je vhodné nabízet, pokud budou uživatelé pravděpodobně chtít mít nerušený pohled na obsah, zejména v případech, kdy se na obsah budou nejspíše dívat po dlouhou dobu. Klasickým případem vhodnosti celé obrazovky je sledování videí, kdy by ovládací prvky rušily a výsledkem by byla menší plocha obrazovky pro obsah. Celou obrazovku lze ale dobře využít i pro jiný typ obsahu, včetně dokumentů a obrázků.</p>

<p>Celá obrazovka se typicky používá při prohlížení. Existují ale i situace, kdy se může hodit pro účely úprav. Běžně je také používána pro hry (jak se používá pro hry není předmětem této stránky).</p>

</section>

<section id="guidelines">
<title>Zásady</title>

<list>
<item><p>Pokud má aplikace různá zobrazení, je nutné povolit celou obrazovku jen pro ta zobrazení, kde to má smysl – především u těch, která zobrazují obsah.</p></item>
<item><p>Celá obrazovka může být spuštěna automaticky, když je otevřen obsah, nebo může být nabízena jako režim, který aktivuje uživatel.</p></item>
<item><p>Pokud je celá obrazovka nabízena jako režim, který aktivuje uživatel, zvažte jak významný je ovládací prvek, kterým se na celou obrazovku vstupuje, a jak často bude používán. Typicky bývá tlačítko celé obrazovky umístěné na <link xref="header-bars">hlavičkové liště</link> nebo v <link xref="header-bar-menus">nabídce hlavičkové lišty</link>.</p></item>
<item><p>Když je celá obrazovka aktivní:</p>
  <list>
    <item><p>Všechny ovládací prvky by měly být skryté, včetně hlavičkové lišty okna, <link xref="action-bars">akčních lišt</link> nebo <link xref="action-bars">překrývajících ovládacích prvků</link>.</p></item>
    <item><p>Ovládací prvky, včetně hlavičkové lišty, by se měly znovu objevit, když uživatel pohne ukazatelem myši nebo poklepe na dotykový displej.</p></item>
    <item><p>Zavírací tlačítko by mělo být nahrazeno tlačítkem pro obnovení zobrazení, které režim celé obrazovky vypne.</p></item>
    <item><p>Ovládací prvky, které se v režimu celé obrazovky k ničemu nehodí, by měly být skryty nebo nahrazeny. To se týká i ovládacích prvků navigace, které by zároveň opustili celou obrazovku, jako třeba tlačítko zpět.</p></item>
    <item><p>Snažte se, aby ovládací prvky a uživatelské rozhraní nebránili obsahu. Například dialogová okna, která obsah překrývají, působí rušivě a zhoršují celkový pocit z režimu celé obrazovky.</p></item>
  </list>
</item>
<item><p>Dobrou ukázkou, jak by se měla chovat aplikace s režimem celé obrazovky, je aplikace Videa.</p></item>
</list>

</section>

</page>
