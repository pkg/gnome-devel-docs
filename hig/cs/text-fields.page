<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="text-fields" xml:lang="cs">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  </info>

<title>Textová pole</title>

<p>Textové pole je prvek uživatelského rozhraní sloužící k zadávání a úpravám textu. Jedná se o základní prvek s různými variantami použití, včetně hledání, nastavení a předvoleb. Textové pole může být předvyplněné textem a může obsahovat doplňující tlačítka a ikony.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/text-fields.svg"/>

<section id="general-guidelines">
<title>Obecné zásady</title>

<list>
<item><p>Velikost textového pole volte podle očekávaného rozsahu obsahu, který v něm bude. To poskytne užitečný vizuální podnět ohledně očekávaného rozsahu vstupu a předchází to přeplnění.</p></item>
<item><p>V <link xref="dialogs#instant-and-explicit-apply">dialogových oknech s okamžitou platností</link> provádějte ověření platnosti obsahu vstupního pole až ztratí zaměření nebo když je zavřeno okno, a ne po každém zmáčknutí klávesy. Výjimka: pokud pole přijímá pevný počet znaků, jako třeba šestnáctkové číslo s hodnotou barvy, ověřte platnost a použijte zadanou hodnotu hned, jak je zadán patřičný počet znaků.</p></item>
<item><p>Když implementujete vstupní pole, které přijímá jen určité znaky, jako třeba číslice, přehraje systémové varovné pípnutí pokaždé, když uživatel zkusí napsat neplatný znak.</p></item>
<item><p>Normálně by zmáčknutí klávesy <key>Tab</key> měla v jednořádkovém vstupním poli přesunout zaměření na následující ovládací prvek a ve víceřádkovém vstupním poli vložit znak tabulátor. Zaměření na následující ovládací prvek ve víceřádkovém vstupním poli přesune zmáčknutí <keyseq><key>Ctrl</key> <key>Tab</key></keyseq>.</p></item>
<item><p>Pokud potřebujete poskytnou klávesovou zkratku, která vloží znak tabulátor do jednořádkového vstupního pole, použijte <keyseq><key>Ctrl</key> <key>Tab</key></keyseq>. V praxi ale asi nenajdete moc situací, kdy by to mělo nějaký užitek.</p></item>
</list>

</section>

<section id="embedding-info-and-controls">
<title>Vestavěné informace a ovládací prvky</title>

<p>V textovém poli může být vložená řada doplňujících informací a ovládacích prvků.</p>

<p>Ikona nebo tlačítko s ikonou mohou být vloženy uvnitř textového pole, aby poskytly stavovou informaci nebo doplňující ovládání.</p>

<list>
<item><p>Ikonu na začátku vstupního pole můžete využít, abyste dali najevo účel pole – nahrazuje potřebu označit pole popiskem. Klasickou ukázkou je vstupní pole pro hledání, kdy je na levé straně vstupního pole umístěna ikona lupy.</p></item>
<item><p>Pokud se u vkládaného textu rozlišuje velikost písmen, můžete v poli zobrazit varovnou ikonu v situaci, když je zapnutý <key>Caps Lock</key>. Ta bývá typicky zobrazena na pravé straně pole.</p></item>
<item><p>Pokud je u textového pole běžné, že je potřeba jej vymazat, můžete umístit do pole na pravou stranu tlačítko s mazací ikonou.</p></item>
<item><p>Když do textového pole umisťujete ikonu (ať už jako indikátor nebo tlačítko, použijte její symbolickou variantu z motivu symbolických ikon GNOME.</p></item>
</list>

<p>Pokud může mít pro uživatele při používání textového pole přínos nějaká doplňující informace, předvyplňte do pole nápovědný text. Při rozhodování o zobrazení doplňující informace platí jako vždy, že byste to měli udělat, jen když je to nutné.</p>

</section>

<section id="api-reference">
<title>Referenční příručka API</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkEntry.html">GtkEntry</link></p></item>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkSearchEntry.html">GtkSearchEntry</link></p></item>
</list>

</section>
</page>
