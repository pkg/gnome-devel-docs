<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="overlaid-controls" xml:lang="de">

  <info>
    <link type="guide" xref="patterns#secondary"/>
    <desc>Schwebende Steuerungen, oft für Bilder und Videos verwendet</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Überlagerte Bedienelemente</title>

<media type="image" mime="image/svg" src="figures/patterns/overlaid-controls.svg"/>

<p>Transient controls that float over content are a common pattern for applications that show images or video.</p>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Since overlaid controls hide when they are not in use, they help to provide an uncluttered viewing experience. They are appropriate when it is desirable to present a clean and distraction-free view on a content item — this is particularly (although not exclusively) appropriate for images and video.</p>

<p>Overlaid controls may be inappropriate if they obscure relevant parts of the content below. Image editing controls may interfere with the ability to see their effects, for example. In these cases, controls should not be overlaid.</p>

</section>

<section id="guidelines">
<title>Richtlinien</title>

<list>
<item><p>Follow established conventions for this type of control, such as left/right browse buttons in image viewers, and player controls at the bottom for video.</p></item>
<item><p>Controls should be displayed when the pointer is moved over the content, or when it is tapped (on touch devices).</p></item>
<item><p>Overlaid controls can be attached to the edge of the content/window, or can be free-floating. <link xref="action-bars">Action bars</link> can be treated as overlaid controls.</p></item>
<item><p>Use the standard “OSD” theme style for overlaid controls.</p></item>
</list>

</section>

</page>
