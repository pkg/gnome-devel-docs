<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="selection-mode" xml:lang="de">

  <info>
    <link type="guide" xref="patterns#primary"/>
    <desc>Muster für die Auswahl von Inhaltsobjekten</desc>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Christian Kirbach</mal:name>
      <mal:email>christian.kirbach@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

<title>Markierungsmodus</title>

<media type="image" mime="image/svg" src="figures/patterns/selection-mode.svg"/>

<p>Der Markierungsmodus ist ein Designmuster, das die Ausführung von Aktionen mit Inhaltsobjekten ermöglicht. Er wird typischerweise in Verbindung mit Listen und Gittern verwendet.</p>

<p>Wenn der Markierungsmodus aktiv ist, ermöglichen Ankreuzfelder die Auswahl von Objekten und eine Aktionsleiste wird unterhalb der Ansicht angezeigt. Diese enthält die verschiedenen Aktionen, die auf die ausgewählten Inhaltsobjekte angewendet werden können.</p>

<section id="when-to-use">
<title>Anwendungsfälle</title>

<p>Der Markierungsmodus sollten in folgenden Fällen eingesetzt werden:</p>

<list>
<item><p>Aktionen können gleichzeitig auf mehrere Inhaltsobjekte angewendet werden.</p></item>
<item><p>Es sind mehrere Aktionen verfügbar, die auf Inhaltsobjekte angewendet werden können.</p></item>
<item><p>Es ist hilfreich für Benutzer, Aktionen auf Inhaltsobjekte anwenden zu können, ohne diese öffnen zu müssen.</p></item>
</list>

<p>If it is more typical for users to perform actions on single content items, you might want to consider another design pattern, such as a context menu. Likewise, if there is only one action that can be performed on content items, a variant of selection mode can be used (an overlaid button could allow the action to be performed on items directly, rather than requiring them to be selected first, for example).</p>

</section>

<section id="activating-selection-mode">
<title>Aktivieren des Markierungsmodus</title>

<p>The primary way of activating selection mode is through the selection mode button, which is located in the header bar and which is identified by a check mark icon. Selection mode can also be activated by rubber band selection, or by holding Ctrl or Shift and clicking/pressing content items.</p>

</section>

<section id="action-bar">
<title>Die Aktionsleiste</title>

<list>
<item><p>Bedienelemente in der Aktionsleiste sollten ausgegraut sein, wenn keine Objekte ausgewählt sind. Manchmal können Aktionen nur auf mehrere Inhaltsobjekte angewendet werden. In diesem Fall sollten die relevanten Bedienelemente nur anklickbar sein, wenn mehrere Objekte ausgewählt sind.</p></item>
<item><p>Bedienelemente in der Aktionswerkzeugleiste können Symbole oder Textbeschriftungen haben.</p></item>
<item><p>Aktionsgruppen sind besser unterscheidbar, wenn Sie jeweils an einem Ende der Werkzeugleiste platzieren. Destruktive Aktionen, wie Löschen, sollten in einem wahrnehmbaren Abstand zu anderen Bedienelementen platziert werden.</p></item>
</list>

</section>
</page>
