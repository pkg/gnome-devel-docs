<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:itst="http://itstool.org/extensions/" type="topic" id="keyboard-input" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>
    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
    <desc>Tangentbordsnavigering, snabbtangenter och kortkommandon.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Tangentbordsinmatning</title>

<p>Tangentbord är ett vanligt sätt att interagera med användargränssnitt. De tillhandahåller ett bekvämt och effektivt sätt att använda program i en uppsjö av situationer och kan vara snabbare och mer effektiva än andra inmatningsenheter. Tangentbord är också viktiga för människor med synnedsättning eller de med rörelsehinder.</p>

<p>Du bör försäkra att all funktionalitet som ditt program tillhandahåller kan kommas åt via ett tangentbord. Att försöka att använda ditt program med endast ett tangentbord är ett utmärkt sätt att prova detta.</p>

<p>Tangentbordsinteraktion har tre aspekter i GNOME och GTK: navigering, snabbtangenter och kortkommandon. <link xref="search">Sökning</link> är ytterligare en annan aspekt.</p>

<section id="keyboard-navigation">
<title>Tangentbordsnavigering</title>

<p>Säkerställ att det är möjligt att flytta runt och interagera med varje del i ditt användargränssnitt via tangentbordet genom att följa dessa riktlinjer.</p>

<list>
<item><p>Följ GNOME:s standardtangenter för navigering. <key>Tabb</key> är standardtangenten för att flytta runt i ett gränssnitt med GTK och GNOME.</p></item>
<item><p>Använd en logisk ordning för tangentbordsnavigering. När man navigerar runt i ett fönster med <key>Tabb</key> bör tangentbordsfokus flytta mellan komponenterna enligt en förutsägbar ordning. I västerländska lokaler är detta normalt från vänster till höger samt uppifrån och ner.</p></item>
<item><p>Förutom navigering med <key>Tabb</key>, ansträng dig för att tillåta förflyttning med piltangenterna, både inom användargränssnittselement (så som listor, ikonrutnät eller sidopaneler) och mellan dem.</p></item>
</list>

<note><p>Om aktivering av en komponent aktiverar andra komponenter, ge inte automatiskt fokus till den första beroende komponenter när den aktiveras, utan lämna istället fokus där det är.</p></note>

<section id="navigation-keys">
<title>Standardnavigeringstangenter</title>

<table>
<thead>
<tr>
<td><p>Genväg</p></td>
<td><p>Funktion</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><key>Tabb</key> och <keyseq><key>Skift</key><key>Tabb</key></keyseq></p></td>
<td><p>Flyttar tangentbordsfokus till nästa/föregående komponent</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Tabb</key></keyseq> och <keyseq><key>Skift</key><key>Ctrl</key><key>Tabb</key></keyseq></p></td>
<td><p>Flyttar tangentbordsfokus ut från den omslutande komponenten till nästa/föregående komponent i de situationer där Tabb ensamt har en annan funktion</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Tabb</key></keyseq> och <keyseq><key>Skift</key><key>Ctrl</key><key>Tabb</key></keyseq></p></td>
<td><p>Flyttar tangentbordsfokus till nästa/föregående grupp av komponenter</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>F1</key></keyseq></p></td>
<td><p>Visar en inforuta för det fönster eller den komponent som har fokus för närvarande</p></td>
</tr>
<tr>
<td><p><keyseq><key>Skift</key><key>F1</key></keyseq></p></td>
<td><p>Visa sammanhangskänslig hjälp för det fönster eller den komponent som för närvarande har fokus</p></td>
</tr>
<tr>
<td><p><key>F6</key> och <keyseq><key>Skift</key><key>F6</key></keyseq></p></td>
<td><p>Ge fokus till nästa/föregående panel i ett fönster med GtkPane</p></td>
</tr>
<tr>
<td><p><key>F8</key></p></td>
<td><p>Ge fokus till separatorraden i ett panelfönster</p></td>
</tr>
<tr>
<td><p><key>F10</key></p></td>
<td><p>Ge fokus till menyraden eller öppna en rubrikradsmeny</p></td>
</tr>
<tr>
<td><p><key>Blanksteg</key></p></td>
<td><p>Växla tillståndet för en kryssruta, radioknapp eller växlingsknapp som har fokus</p></td>
</tr>
<tr>
<td><p><key>Retur</key></p></td>
<td><p>Aktivera knappen, menyobjektet o.s.v. som är i fokus</p></td>
</tr>
<tr>
<td><p><key>Retur</key> och <key>End</key></p></td>
<td><p>Välj/flytta till det första objektet i en markerad komponent</p></td>
</tr>
<tr>
<td><p><key>PageUp</key>, <keyseq><key>Ctrl</key><key>PageUp</key></keyseq>, <key>PageDown</key> och <keyseq><key>Ctrl</key><key>PageDown</key></keyseq></p></td>
<td><p>Rulla den markerade vyn en sida upp/vänster/ner/höger</p></td>
</tr>
<tr>
<td><p><key>Escape</key></p></td>
<td><p>Stäng eller avsluta den aktuella kontexten om den är tillfällig. Detta bör användas konsekvent för menyer, kontextfönster eller <link xref="dialogs#default-action-and-escape">dialoger</link>, eller något annat tillfälligt läge eller användargränssnittselement.</p></td>
</tr>
</tbody>
</table>

</section>
</section>

<section id="access-keys">
<title>Snabbtangenter</title>

<p>Snabbtangenter låter användare manövrera etiketterade komponenter genom att använda <key>Alt</key>. De indikeras via en understruket tecken inom varje komponentetikett (detta visas när <key>Alt</key> hålls ned).</p>

<list>
<item><p>När möjligt, bör alla etiketterade komponenter ha en snabbtangent.</p></item>
<item><p>Välj snabbtangenter som är enkla att komma ihåg. Normalt innebär detta att första bokstaven i etiketten används. Om etiketten har mer än ett ord kan den första bokstaven i de övriga orden också användas. Om en annan bokstav ger en bättre association (till exempel ”x” i ”Extra stor”), överväg att använda det tecknet istället.</p></item>
<item><p>Undvik att tilldela snabbtangenter till ”tunna” tecken (så som gemena i eller l), eller tecken med underhäng (så som gemena g eller y), om det inte är oundvikligt. Understrykningen är ibland inte så tydlig för dessa tecken.</p></item>
<item><p>Om valet av snabbtangent är svårt, tilldela snabbtangenter till de mest använda komponenterna först. Om det första tecknet inte är tillgängligt välj en konsonant från etiketten som är lätta att komma ihåg, till exempel ”r” i ”Ersätt”. Tilldela endast vokaler när inga konsonanter är tillgängliga.</p></item>
<item><p>Var uppmärksam på att snabbtangent måste översättas tillsammans med strängarna som de tas från, så även om det inte finns några konflikter på ditt modersmål kan de inträffa i översättningar.</p></item>
</list>

</section>

<section id="shortcut-keys">
<title>Kortkommandon</title>

<p>Kortkommandon tillhandahåller bekväm tillgång till vanliga åtgärder. De kan antingen vara enstaka knappar eller kombinationer av flera tangenter (typiskt en modifierare i kombination med en vanlig tangent)</p>

<list>
<item><p>Använd inte systemövergripande kortkommandon i ditt program. Se nedan för detaljer om dessa.</p></item>
<item><p>Använd GNOME:s standardkortkommandon (se nedan) om ditt program har stöd för de funktionerna. Detta säkerställer att GNOME-program beter sig konsekvent och hjälper användaren att upptäcka kortkommandona.</p></item>
<item><p>Tilldela kortkommandon för de mest använda åtgärderna i ditt program. Försök dock inte att tilldela en tangentbordsgenväg för allting.</p></item>
<item><p>Försök att använda <key>Ctrl</key> i kombination med en bokstav för dina egna kortkommandon. <keyseq><key>Skift</key><key>Ctrl</key></keyseq> och en bokstav är det rekommenderade mönstret för kortkommandon som omvänder eller bygger vidare på en annan funktion. Till exempel <keyseq><key>Ctrl</key><key>Z</key></keyseq> och <keyseq><key>Shift</key><key>Ctrl</key><key>Z</key></keyseq> för <gui>Ångra</gui> och <gui>Gör om</gui>.</p></item>
<item><p>Nya kortkommandon bör fungera som förkortningar då dessa är lättare att lära in och komma ihåg. Till exempel kan <keyseq><key>Ctrl</key><key>R</key></keyseq> vara ett lämpligt kortkommando för ett menyobjekt kallat <gui>Redigera sida</gui>.</p></item>
<item><p>Kortkommandon som enkelt kan användas med en hand är att föredra för vanliga åtgärder.</p></item>
<item><p>Använd inte <key>Alt</key> för kortkommandon, då detta kan stå i konflikt med snabbtangenter.</p></item>
</list>

</section>

<section id="system-shortcuts">
<title>Systemreserverade kortkommandon</title>

<p>Följande kortkommandon för systemet bör inte åsidosättas av program.</p>

<p>GNOME 3 har exklusiv användning av <key>Super</key>-knappen, ofta känd som Windows-knappen, för kortkommandon för systemet. <key>Super</key> bör därför inte användas av några program.</p>

<table>
<thead>
<tr>
<td><p>Funktion</p></td>
<td><p>Genväg</p></td>
<td><p>Förlegat kortkommado</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p>Översiktsvyn Aktiviteter</p></td>
<td><p><key>Super</key></p></td>
<td><p>Ingen</p></td>
<td><p>Öppnar och stänger översiktsvyn Aktiviteter</p></td>
</tr>
<tr>
<td><p>Programvy</p></td>
<td><p><keyseq><key>Super</key><key>A</key></keyseq></p></td>
<td><p>Ingen</p></td>
<td><p>Öppnar och stänger programvyn i översiktsvyn Aktiviteter</p></td>
</tr>
<tr>
<td><p>Meddelandefält</p></td>
<td><p><keyseq><key>Super</key><key>M</key></keyseq></p></td>
<td><p>Ingen</p></td>
<td><p>Växlar synligheten för meddelandefältet.</p></td>
</tr>
<tr>
<td><p>Lås</p></td>
<td><p><keyseq><key>Super</key><key>L</key></keyseq></p></td>
<td><p>Ingen</p></td>
<td><p>Låser systemet genom att blanka skärmen och kräva ett lösenord för att låsa upp, om ett sådant har ställts in.</p></td>
</tr>
<tr>
<td><p>Växla program</p></td>
<td><p><keyseq><key>Super</key><key>Tabb</key></keyseq> och <keyseq><key>Skift</key><key>Super</key><key>Tabb</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>Tabb</key></keyseq> och <keyseq><key>Skift</key><key>Alt</key><key>Tabb</key></keyseq></p></td>
<td><p>Växlar fokus till nästa/föregående program</p></td>
</tr>
<tr>
<td><p>Växla fönster</p></td>
<td><p><keyseq><key>Super</key><key>`</key></keyseq> och <keyseq><key>Skift</key><key>Super</key><key>`</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F6</key></keyseq> och <keyseq><key>Skift</key><key>Alt</key><key>F6</key></keyseq></p></td>
<td><p>Växla fokus till nästa eller föregående sekundära fönster associerat med programmet</p></td>
</tr>
<tr>
<td><p><gui>Maximera</gui></p></td>
<td><p><keyseq><key>Super</key><key>↑</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F10</key></keyseq></p></td>
<td><p>Maximera fönstret i fokus</p></td>
</tr>
<tr>
<td><p><gui>Återställ</gui></p></td>
<td><p><keyseq><key>Super</key><key>↓</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F5</key></keyseq></p></td>
<td><p>Återställer det fokuserade fönstret till dess föregående tillstånd</p></td>
</tr>
<tr>
<td><p><gui>Dölj</gui></p></td>
<td><p><keyseq><key>Super</key><key>H</key></keyseq></p></td>
<td><p><keyseq><key>Alt</key><key>F9</key></keyseq></p></td>
<td><p>Dölj det fokuserade fönstret</p></td>
</tr>

<tr>
<td><p>Växla systemyta</p></td>
<td><p>Ingen</p></td>
<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Tabb</key></keyseq> och <keyseq><key>Skift</key><key>Ctrl</key><key>Alt</key><key>Tabb</key></keyseq></p></td>
<td><p>Växlar fokus till primärytorna för systemet: fönster, systemrad, meddelandefält</p></td>
</tr>
<tr>
<td><p>Stäng av</p></td>
<td><p>Ingen</p></td>
<td><p><keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq></p></td>
<td><p>Frågar användaren om att stänga av systemet. Detta kortkommando är vanligtvis inaktiverat som standard.</p></td>
</tr>
<tr>
<td><p>Fönstermeny</p></td>
<td><p><keyseq><key>Alt</key><key>Blanksteg</key></keyseq></p></td>
<td><p>Ingen</p></td>
<td><p>Öppnar fönstermenyn för det aktuella fönstret</p></td>
</tr>
<tr>
<td><p><gui>Stäng</gui></p></td>
<td><p>Ingen</p></td>
<td><p><keyseq><key>Alt</key><key>F4</key></keyseq></p></td>
<td><p>Stänger det fokuserade fönstret</p></td>
</tr>
<tr>
<td><p><gui>Flytta</gui></p></td>
<td><p>Ingen</p></td>
<td><p><keyseq><key>Alt</key><key>F7</key></keyseq></p></td>
<td><p>Flytta det fokuserade fönstret</p></td>
</tr>
<tr>
<td><p><gui>Ändra storlek</gui></p></td>
<td><p>Ingen</p></td>
<td><p><keyseq><key>Alt</key><key>F8</key></keyseq></p></td>
<td><p>Ändra storlek på det fokuserade fönstret</p></td>
</tr>
</tbody>
</table>

<p>Dessutom bör kortkommandona för inmatning av Unicode-tecken undvikas. Detta inkluderar från <keyseq><key>Skift</key><key>Ctrl</key><key>A</key></keyseq> till <keyseq><key>Skift</key><key>Ctrl</key><key>F</key></keyseq>, eller från <keyseq><key>Skift</key><key>Ctrl</key><key>0</key></keyseq> till <keyseq><key>Skift</key><key>Ctrl</key><key>9</key></keyseq>.</p>

</section>

<section id="application-shortcuts">
<title>Standardkortkommandon för program</title>

<p>Detta avsnitt beskriver vanliga kortkommandon för program. Med undantag för programkortkommandon behöver dessa kortkommandon endast följas när motsvarande åtgärd finns i ditt program. Standardkortkommandon kan tilldelas till andra åtgärder om standardåtgärden inte finns.</p>

<p>Detta avsnitt tillhandahåller också riktlinjer om standardmenyobjekt i ett <link xref="menu-bars">menyrad</link>, om en sådan används.</p>

<section id="application">
<title>Program</title>

<p>Standardkortkommandon för program och menyobjekt. Dessa kortkommandon för program bör inte tilldelas till andra åtgärder även när motsvarande åtgärd inte finns i ditt program.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Hjälp</gui></p></td>
<td><p><key>F1</key></p></td>
<td><p>Öppnar standardhjälpbläddraren på innehållssidan för programmet.</p></td>
</tr>
<tr>
<td><p><gui>Om</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Öppnar Om-dialogen för programmet. Använd GNOME 3:s standarddialog för detta.</p></td>
</tr>
<tr>
<td><p><gui>Avsluta</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Q</key></keyseq></p></td>
<td><p>Stänger programmet, inklusive alla programfönster.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="file">
<title>Arkiv</title>

<p>Standardkortkommandon och menyobjekt för filer.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Nytt</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>N</key></keyseq></p></td>
<td><p>Skapar ett nytt innehållsobjekt, oftast (men inte alltid) i ett nytt primärt fönster eller flik. Om ditt program kan skapa ett antal olika typer av dokument kan du göra <gui>Nytt</gui>-objektet till en undermeny som innehåller ett menyalternativ för varje typ. Etikettera dessa objekt som <gui>Nytt</gui> dokumenttyp, gör det första objektet i undermenyn till den vanligaste dokumenttypen och ge detta kortkommandot <keyseq><key>Ctrl</key><key>N</key></keyseq>.</p></td>
</tr>
<tr>
<td><p><gui>Öppna…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>O</key></keyseq></p></td>
<td><p>Öppnar ett existerade innehållsobjekt, ofta genom att visa användaren standarddialogen <gui>Öppna fil</gui>. Om den valda filen redan är öppen i programmet, visa det fönstret istället för att öppna ett nytt.</p></td>
</tr>
<tr>
<td><p><gui>Öppna senaste</gui></p></td>
<td><p>Ingen</p></td>
<td><p>En undermeny som innehåller en lista över inte fler än sex nyligen använda filer, ordnade efter när de senast använts.</p></td>
</tr>
<tr>
<td><p><gui>Spara</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>S</key></keyseq></p></td>
<td><p>Sparar det aktuella innehållsobjektet. Om dokumentet redan har ett filnamn associerat med sig, spara dokumentet omedelbart utan vidare interaktion från användare. Om det finns ytterligare alternativ involverade i att spara en fil, fråga efter dessa första gången dokumentet sparas, men använd samma värden påföljande gånger tills användaren ändrar dessa. Om dokumentet inte har något aktuellt filnamn eller är skrivskyddat bör detta alternativ fungera på samma sätt som <gui>Spara som</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Spara som…</gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>S</key></keyseq></p></td>
<td><p>Sparar innehållsobjektet under ett nytt filnamn. Visa användaren standarddialogen <gui>Spara som</gui> och spara filen med det valda filnamnet.</p></td>
</tr>
<tr>
<td><p><gui>Spara en kopia…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Frågar användaren om ett filnamn under vilket en kopia av dokumentet sedan sparas. Förändra varken vyn eller filnamnet för originaldokumentet. Alla påföljande ändringar görs fortfarande i originaldokumentet tills användaren anger något annat, genom att till exempel använda kommandot <gui>Spara som</gui>.</p>
<p>I likhet med dialogen <gui>Spara som</gui> kan dialogen <gui>Spara en kopia</gui> presentera olika sätt att spara datan. Om till exempel en bild kan sparas i ett standardformat eller som en PNG.</p></td>
</tr>
<tr>
<td><p><gui>Sidinställningar</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Låter användare styra utskriftsrelaterade inställningar. Visa användaren en dialog som låter användaren ställa in sådana inställningar så som stående eller liggande format, marginaler, o.s.v.</p></td>
</tr>
<tr>
<td><p><gui>Förhandsgranska</gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>P</key></keyseq></p></td>
<td><p>Visar användaren hur det utskrivna dokumentet kommer att se ut. Visa ett nytt fönster som innehåller en korrekt representation av dokumentets utseende så som det skulle skrivas ut.</p></td>
</tr>
<tr>
<td><p><gui>Skriv ut…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>P</key></keyseq></p></td>
<td><p>Skriver ut det aktuella dokumentet. Visa användaren en dialog som tillåter inställning av alternativ så som sidintervall att skriva ut, skrivare som ska användas, o.s.v. Dialogen måste innehålla en knapp med etiketten <gui>Skriv ut</gui> som inleder utskriften och stänger dialogen.</p></td>
</tr>
<tr>
<td><p><gui>Skicka till…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>M</key></keyseq></p></td>
<td><p>Erbjuder användaren ett sätt att bifoga eller skicka det aktuella dokumentet som ett e-postmeddelande eller en bilaga, beroende på dess format. Du kan tillhandahålla mer är ett <gui>Skicka</gui>-objekt beroende på vilka alternativ som är tillgängliga. Om det finns fler än två sådana objekt, flytta dem till en undermeny. Om till exempel endast <gui>Skicka via e-post</gui> och <gui>Skicka via Bluetooth</gui> finns tillgängliga, låt dem vara i toppmenyn. Om det finns ett tredje alternativ så som <gui>Skicka via FTP</gui>, placera alla alternativen i en undermeny med namnet Skicka.</p></td>
</tr>
<tr>
<td><p><gui>Egenskaper…</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Retur</key></keyseq></p></td>
<td><p>Öppnar dokumentets fönster med <gui>Egenskaper</gui>. Detta kan innehålla redigerbar information, så som dokumentets upphovsman, eller skrivskyddad information så som antalet ord i dokumentet, eller en kombination av båda. Kortkommandot <keyseq><key>Alt</key><key>Retur</key></keyseq> bör inte tillhandahållas där <key>Retur</key> vanligen använd för att infoga en ny rad.</p></td>
</tr>
<tr>
<td><p><gui>Stäng</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
<td><p>Stänger den aktuella fliken eller fönstret. Om fönstret använder flikar och det endast finns en öppen bör kortkommandot stänga fönstret.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="edit">
<title itst:context="menu">Redigera</title>

<p>Standardtangentbordsgenvägar för redigering samt menyobjekt.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Ångra <em>åtgärd</em></gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Z</key></keyseq></p></td>
<td><p>Återställer effekten för den föregående åtgärden.</p></td>
</tr>
<tr>
<td><p><gui>Gör om <em>åtgärd</em></gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>Z</key></keyseq></p></td>
<td><p>Utför nästa åtgärd i ångringshistoriken, efter att användaren har flyttat tillbaka med kommandot <gui>Ångra</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Klipp ut</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>X</key></keyseq></p></td>
<td><p>Tar bort det valda innehållet och placerar det i urklipp. Visuellt tas innehållet bort från dokumentet på samma sätt som <gui>Ta bort</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Kopiera</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>C</key></keyseq></p></td>
<td><p>Kopierar det valda innehållet till urklipp.</p></td>
</tr>
<tr>
<td><p><gui>Klistra in</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>V</key></keyseq></p></td>
<td><p>Infogar innehållet från urklipp i innehållsobjektet. När text redigeras, om det inte finns någon aktuell markering, använd markören som infogningspunkt. Om det finns en aktuell markering, ersätt denna med innehållet från urklipp.</p></td>
</tr>
<tr>
<td><p><gui>Klistra in special…</gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>V</key></keyseq></p></td>
<td><p>Infogar en onormal representation av urklipps innehåll. Öppna en dialog som visar en lista över de tillgängliga format som användaren kan välja mellan. Om till exempel urklipp innehåller en PNG-fil kopierad från filhanteraren, kan bilden bäddas in i dokumentet, eller så kan en länk infogas så att ändringar i bilden på disk alltid återspeglas i dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Duplicera</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>U</key></keyseq></p></td>
<td><p>Skapar en kopiera av det markerade objektet.</p></td>
</tr>
<tr>
<td><p><gui>Ta bort</gui></p></td>
<td><p><key>Delete</key></p></td>
<td><p>Tar bort det markerade innehållet utan att placera det i urklipp.</p></td>
</tr>
<tr>
<td><p><gui>Markera allt</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>A</key></keyseq></p></td>
<td><p>Markerar allt innehåll i det aktuella dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Avmarkera allt</gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>A</key></keyseq></p></td>
<td><p>Avmarkerar allt innehåll i det aktuella dokumentet. Tillhandahåll detta objekt endast i situationer när ingen annan metod av att ångra markering är möjlig eller uppenbar för användaren. Till exempel i komplexa grafikprogram där markering och avmarkering vanligtvis inte är enkla att utföra med markörtangenterna. Notera: Tillhandahåll inte <gui>Avmarkera allt</gui> i textinmatningsfält, då <keyseq><key>Skift</key><key>Ctrl</key><key>hex</key></keyseq>-siffra används för att mata in Unicode-tecken så kommer dess kortkommando inte att fungera.</p></td>
</tr>
<tr>
<td><p><gui>Sök…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>F</key></keyseq></p></td>
<td><p>Visar ett användargränssnitt för att låta användaren söka efter specifikt innehåll i innehållsobjektet eller sidan.</p></td>
</tr>
<tr>
<td><p><gui>Sök nästa</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>G</key></keyseq></p></td>
<td><p>Markerar nästa förekomst av den senaste term som sökts efter i det aktuella dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Sök föregående</gui></p></td>
<td><p><keyseq><key>Skift</key><key>Ctrl</key><key>G</key></keyseq></p></td>
<td><p>Markerar föregående förekomst av den senaste term som sökts efter i det aktuella dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Ersätt…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>H</key></keyseq></p></td>
<td><p>Visar ett användargränssnitt som låter användaren leta efter specifikt innehåll och ersätta varje förekomst.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="view">
<title>Visa</title>

<p>Tangentbordsgenvägar och menyobjekt för visning.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Ikoner</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Visar innehåll som ett rutnät av ikoner. Detta är ett radioknappsmenyobjekt.</p></td>
</tr>
<tr>
<td><p><gui>Lista</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Visar innehåller som en lista. Detta är ett radioknappsmenyobjekt.</p></td>
</tr>
<tr>
<td><p><gui>Sortera efter…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Anger kriteriet efter vilket innehållet bör sorteras. Kan öppna en inställningsdialog, kontextfönster eller undermeny.</p></td>
</tr>
<tr>
<td><p><gui>Filtrera…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Låter innehållet filtreras, genom att öppna ett kontextfönster, en rullgardinsmeny eller en dialog.</p></td>
</tr>
<tr>
<td><p><gui>Zooma in</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>+</key></keyseq></p></td>
<td><p>Zoomar in, vilket får innehållet att se större ut.</p></td>
</tr>
<tr>
<td><p><gui>Zooma ut</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>-</key></keyseq></p></td>
<td><p>Zoomar ut, vilket får innehållet att se mindre ut.</p></td>
</tr>
<tr>
<td><p><gui>Normal storlek</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>0</key></keyseq></p></td>
<td><p>Återställer zoomnivån till dess standardvärde.</p></td>
</tr>
<tr>
<td><p><gui>Bästa passning</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Anpassar dokumentet till att fylla ut fönstret.</p></td>
</tr>
<tr>
<td><p><gui>Läs om</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>R</key></keyseq></p></td>
<td><p>Ritar om den aktuella vyn av dokumentet, och kontrollerar om datakällan ändrats först. Kontrollerar till exempel om webbservern uppdaterat sidan innan den ritas om.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="format">
<title>Formatering</title>

<p>Standardtangentbordsgenvägar och menyobjekt för formatering.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Stil…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in stilattribut för den markerade texten eller objekten antingen individuellt eller enligt en namngiven fördefinierad stil.</p></td>
</tr>
<tr>
<td><p><gui>Typsnitt…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in typsnittsegenskaper för den markerade texten eller objekten.</p></td>
</tr>
<tr>
<td><p><gui>Stycke…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in egenskaperna för det markerade stycket.</p></td>
</tr>
<tr>
<td><p><gui>Fet</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>B</key></keyseq></p></td>
<td><p>Växlar mellan med och utan fetstil för den aktuella textmarkeringen. Om någon del av markeringen för närvarande är i fetstil och någon del inte är det bör detta kommando göra all markerad text till fetstil.</p></td>
</tr>
<tr>
<td><p><gui>Kursiv</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>I</key></keyseq></p></td>
<td><p>Växlar mellan med och utan kursiv stil för den aktuella textmarkeringen. Om någon del av markeringen för närvarande är kursiv och någon del inte är det bör detta kommando göra all markerad text till kursiv.</p></td>
</tr>
<tr>
<td><p><gui>Understrykning</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>U</key></keyseq></p></td>
<td><p>Växlar mellan med och utan understrykning för den aktuella textmarkeringen. Om någon del av markeringen för närvarande är understruken och någon del inte är det bör detta kommando göra all markerad text understruken.</p></td>
</tr>
<tr>
<td><p><gui>Celler…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in egenskaperna för de markerade tabellcellerna.</p></td>
</tr>
<tr>
<td><p><gui>Lista…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in egenskaperna för den markerade listan, eller gör om de markerade styckena till en lista om de inte redan är formaterade som en sådan.</p></td>
</tr>
<tr>
<td><p><gui>Lager…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in egenskaperna för alla eller markerade lager av ett flerlagersdokument.</p></td>
</tr>
<tr>
<td><p><gui>Sida…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Ställer in egenskaperna för alla eller markerade sidor av dokumentet.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="bookmarks">
<title>Bokmärken</title>

<p>Standardtangentbordsgenvägar och menyobjekt för bokmärken.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Lägg till bokmärke</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>D</key></keyseq></p></td>
<td><p>Lägger till ett bokmärke till den aktuella platsen. Visa inte en dialog som ber användaren om en titel eller en plats för bokmärket, välj istället vettiga standardvärden (så som dokumentets titel, eller filnamn som bokmärkets namn) och låt användaren ändra dem senare via funktionen <gui>Redigera bokmärken</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Redigera bokmärken</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>B</key></keyseq></p></td>
<td><p>Låter användaren redigera sina bokmärken.</p></td>
</tr>
<tr>
<td><p>Bokmärkeslista</p></td>
<td><p>Ingen</p></td>
<td><p>Visa användarens bokmärken.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="go">
<title>Gå</title>

<p>Standardtangentbordsgenvägar för navigering samt <gui>Gå</gui>-menyobjekt.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Bakåt</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Vänster</key></keyseq></p></td>
<td><p>Navigerar till den föregående platsen.</p></td>
</tr>
<tr>
<td><p><gui>Framåt</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Höger</key></keyseq></p></td>
<td><p>Navigerar till nästa plats i navigeringshistoriken.</p></td>
</tr>
<tr>
<td><p><gui>Upp</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Upp</key></keyseq></p></td>
<td><p>Navigerar till överordnat innehållsobjekt, dokument, sida eller avsnitt.</p></td>
</tr>
<tr>
<td><p><gui>Hem</gui></p></td>
<td><p><keyseq><key>Alt</key><key>Home</key></keyseq></p></td>
<td><p>Navigerar till en startsida definierad av användaren eller programmet.</p></td>
</tr>
<tr>
<td><p><gui>Plats…</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>L</key></keyseq></p></td>
<td><p>Låter användaren ange en URI att navigera till.</p></td>
</tr>
<tr>
<td><p><gui>Föregående sida</gui></p></td>
<td><p><key>PageUp</key></p></td>
<td><p>Navigerar till den föregående sidan i dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Nästa sida</gui></p></td>
<td><p><key>PageDown</key></p></td>
<td><p>Navigerar till nästa sida i dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Gå till sida…</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Låter användaren ange ett sidnummer som navigeras till. Textbaserade program kan också inkludera ett menyobjekt <gui>Gå till rad…</gui> som låter användaren hoppa till ett angivet radnummer.</p></td>
</tr>
<tr>
<td><p><gui>Första sidan</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>Home</key></keyseq></p></td>
<td><p>Navigerar till första sidan i dokumentet.</p></td>
</tr>
<tr>
<td><p><gui>Sista sidan</gui></p></td>
<td><p><keyseq><key>Ctrl</key><key>End</key></keyseq></p></td>
<td><p>Navigerar till sista sidan i dokumentet.</p></td>
</tr>
</tbody>
</table>

</section>

<section id="windows">
<title>Fönster</title>

<p>Standardmenyobjekt för <gui>Fönster</gui>.</p>

<table>
<thead>
<tr>
<td><p>Etikett</p></td>
<td><p>Genväg</p></td>
<td><p>Beskrivning</p></td>
</tr>
</thead>
<tbody>
<tr>
<td><p><gui>Spara alla</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Sparar alla öppna dokument. Om några dokument inte har ett aktuellt filnamn, fråga efter ett filnamn för vart och ett med standarddialogen <gui>Spara</gui>.</p></td>
</tr>
<tr>
<td><p><gui>Stäng alla</gui></p></td>
<td><p>Ingen</p></td>
<td><p>Stänger alla öppna dokument. Om det finns osparade ändringar i några dokument, visa en bekräftelsedialog för vart och ett.</p></td>
</tr>
<tr>
<td><p>Lista över fönster</p></td>
<td><p>Ingen</p></td>
<td><p>Varje menyobjekt höjer motsvarande fönster till toppen i fönsterstapeln.</p></td>
</tr>
</tbody>
</table>

</section>
</section>

</page>
