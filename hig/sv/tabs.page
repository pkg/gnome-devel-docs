<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:uix="http://projectmallard.org/experimental/ui/" type="topic" id="tabs" xml:lang="sv">

  <info>
    <credit type="author">
      <name>Allan Day</name>
      <email>aday@gnome.org</email>
    </credit>
    <credit>
      <name>Calum Benson</name>
    </credit>
    <credit>
      <name>Adam Elman</name>
    </credit>
    <credit>
      <name>Seth Nickell</name>
    </credit>
    <credit>
      <name>Colin Robertson</name>
    </credit>

    <link type="guide" xref="ui-elements"/>
    <uix:thumb mime="image/svg" src="figures/ui-elements/tabs.svg"/>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2015, 2018</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2017, 2018</mal:years>
    </mal:credit>
  </info>

<title>Flikar</title>

<p>Flikar tillhandahåller ett sätt att dela upp ett fönster i en serie vyer. De finns i två huvudsakliga former: fasta och dynamiska.</p>

<p>Fasta flikar tillhandahåller en oföränderlig uppsättning fördefinierade vyer, och används främst i dialogfönster. Dynamiska flikar låter ett fönster innehålla ändringsbara urval av innehållsobjekt, så som sidor, dokument eller bilder. De används främst i <link xref="primary-windows">primära fönster</link>, som en del av redigerar- eller bläddrarprogram.</p>

<media type="image" mime="image/svg" src="figures/ui-elements/tabs.svg"/>

<section id="when-to-use">
<title>När ska de användas</title>

<p>Använd fasta flikar när ett <link xref="dialogs">dialogfönster</link> innehåller för många komponenter (eller för mycket information) för att bekvämt kunna presenteras på en gång.</p>

<p>Dynamiska flikar är främst användbara för bläddrar- eller redigerarprogram, där en användare kan vilja använda flera platser eller innehållsobjekt samtidigt.</p>

</section>

<section id="fixed">
<title>Fasta flikar</title>

<list>
<item><p>Använd inte för många flikar. Om du inte kan se alla flikar utan att rulla genom dem eller att dela upp dem i flera rader, så använder du troligen för många och borde använda en listkomponent istället.</p></item>
<item><p>Etikettera flikar med <link xref="writing-style#capitalization">rubrikversalisering</link> (Detta gäller för engelska. För svenska är meningsversalisering att föredra), och använd substantiv snarare än verb, exempelvis <gui>Typsnitt</gui> eller <gui>Justering</gui>. Försök ge fliketiketter ungefär samma längd.</p></item>
<item><p>Ge inte flikar en design så att ändringar i komponenter på en sida påverkar komponenterna på andra sidor. Användare kommer troligen inte upptäcka sådana beroenden.</p></item>
<item><p>Placera komponenter som påverkar alla flikar utanför flikarna.</p></item>
<item><p>Gör för fasta flikar bredden av varje flik proportionell till bredden på dess etikett. Ställ inte bara in alla flikar till samma bredd, då detta gör dem svårare att läsa av visuellt, och begränsar antalet flikar du kan se utan att rulla dem.</p></item>
</list>

</section>

<section id="dynamic">
<title>Dynamiska flikar</title>

<list>
<item><p>Flikar har ofta begränsad bredd, så säkerställ att fliketiketter är korta och koncisa, samt att etikettens mest användbara del visas först. Detta säkerställer att etiketten förblir användbar även då den förkortas med en ellips.</p></item>
<item><p>Om innehållet i en flik ändras eller kräver uppmärksamhet så kan en visuell påminnelse om detta visas.</p></item>
<item><p>Tillhandahåll en snabbvalsmeny för varje flik. Denna meny bör endast inkludera åtgärder för att manipulera fliken själv, så som <gui>Flytta åt vänster</gui>, <gui>Flytta åt höger</gui>, <gui>Flytta till nytt fönster</gui> och <gui>Stäng</gui>.</p></item>
<item><p>Om flikar är en viktig del av programmet kan en ”Ny flik”-knapp placeras i rubrikraden eller verktygsfältet. Visa inte en ”Ny flik”-knapp i program där flikar inte alltid kommer att användas — tangentbordsgenvägar och/eller menyobjekt räcker till i dessa fall.</p></item>
</list>

<section id="keyboard-shortcuts">
<title>Standardtangentbordsgenvägar</title>

<p>När dynamiska flikar användas, försäkra att standardtangentbordsgenvägar stöds.</p>

<table>
<tr>
<td><p><keyseq><key>Ctrl</key><key>T</key></keyseq></p></td>
<td><p>Skapa en ny flik</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>W</key></keyseq></p></td>
<td><p>Stäng den aktuella fliken</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Page Up</key></keyseq></p></td>
<td><p>Växla till nästa flik</p></td>
</tr>
<tr>
<td><p><keyseq><key>Ctrl</key><key>Page Down</key></keyseq></p></td>
<td><p>Växla till föregående flik</p></td>
</tr>
</table>

</section>

</section>

<section id="api-reference">
<title>API-referens</title>

<list>
<item><p><link href="https://developer.gnome.org/gtk3/stable/GtkNotebook.html">GtkNotebook</link></p></item>
</list>

</section>
</page>
