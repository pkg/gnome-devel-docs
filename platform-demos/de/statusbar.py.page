<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="statusbar.py" xml:lang="de">
  <info>
    <title type="text">Statusbar (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="spinner.py"/>    
    <revision version="0.2" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Report messages of minor importance to the user</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Statusbar</title>
  <media type="image" mime="image/png" src="media/statusbar.png"/>
  <p>This statusbar tells you if you click the button or if you press any key (and which key).</p>

  <links type="section"/>

  <section id="code">
  <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="StatusBar Example", application=app)
        self.set_default_size(200, 100)

        # a label
        label = Gtk.Label(label="Press any key or ")

        # a button
        button = Gtk.Button(label="click me.")
        # connected to a callback
        button.connect("clicked", self.button_clicked_cb)

        # the statusbar
        self.statusbar = Gtk.Statusbar()
        # its context_id - not shown in the UI but needed to uniquely identify
        # the source of a message
        self.context_id = self.statusbar.get_context_id("example")
        # we push a message onto the statusbar's stack
        self.statusbar.push(
            self.context_id, "Waiting for you to do something...")

        # a grid to attach the widgets
        grid = Gtk.Grid()
        grid.set_column_spacing(5)
        grid.set_column_homogeneous(True)
        grid.set_row_homogeneous(True)
        grid.attach(label, 0, 0, 1, 1)
        grid.attach_next_to(button, label, Gtk.PositionType.RIGHT, 1, 1)
        grid.attach(self.statusbar, 0, 1, 2, 1)

        # add the grid to the window
        self.add(grid)

    # callback function for the button clicked
    # if the button is clicked the event is signaled to the statusbar
    # onto which we push a new status
    def button_clicked_cb(self, button):
        self.statusbar.push(self.context_id, "You clicked the button.")

    # event handler
    def do_key_press_event(self, event):
    # any signal from the keyboard is signaled to the statusbar
    # onto which we push a new status with the symbolic name
    # of the key pressed
        self.statusbar.push(self.context_id, Gdk.keyval_name(event.keyval) +
                            " key was pressed.")
        # stop the signal emission
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  <note><p>
    <code>Gdk.keyval_name(event.keyval)</code> converts the key value <code>event.keyval</code> into a symbolic name. The names and corresponding key values can be found <link href="https://gitlab.gnome.org/GNOME/gtk/blob/master/gdk/gdkkeysyms.h">here</link>, but for instance <code>GDK_KEY_BackSpace</code> becomes the string <code>"BackSpace"</code>.
  </p></note>
  </section>

  <section id="methods">
  <title>Nützliche Methoden für ein Statusbar-Widget</title>
    <p>In line 17 the signal <code>"clicked"</code> is connected to the callback function <code>button_clicked_cb()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
  <list>
    <item><p><code>pop(context_id)</code> removes the first message in the statusbar stack with the given <code>context_id</code>.</p></item>
    <item><p><code>remove_all(context_id)</code> removes all the messages in the statusbar stack with the given <code>context_id</code>.</p></item>
    <item><p><code>remove(context_id, message_id)</code> removes the message with the given <code>message_id</code> in the statusbar stack with the given <code>context_id</code>. The <code>message_id</code> is returned by <code>push(context_id, "the message")</code> when pushing the message on the statusbar.</p></item>
  </list>
  </section>

  <section id="references">
  <title>API-Referenzen</title>
  <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkStatusbar.html">GtkStatusbar</link></p></item>
    <item><p><link href="https://developer.gnome.org/gdk3/stable/gdk3-Keyboard-Handling.html">Gdk - Key Values</link></p></item>
  </list>
  </section>
</page>
