<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="textview.vala" xml:lang="de">
  <info>
  <title type="text">TextView (Vala)</title>
    <link type="guide" xref="beginner.vala#multiline"/>
    <revision version="0.1" date="2012-06-07" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Widget which displays a GtkTextBuffer</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>TextView-Widget</title>
     <note style="sidebar"><p>If we press "enter", we have a new line.</p>
     <p>If we press "enter" more times then there are lines in the default sized window, then a vertical scrollbar appears.</p>
     <p>If we write a long sentence, the text will wrap breaking lines between words.</p>
     <p>If we have a loooooooooooooooooooooooooooooooooooong word, a horizontal scrollbar will appear.</p></note>

    <media type="image" mime="image/png" src="media/textview.png"/>
    <p>This is an example of Gtk.TextView</p>

<code mime="text/x-csharp" style="numbered">/* This is the application. */
public class MyApplication : Gtk.Application {
	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {
		/* Create the window of this application. */
		new MyWindow (this).show_all ();
	}
}

/* This is the window. */
class MyWindow: Gtk.ApplicationWindow {
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "TextView Example");
		this.set_default_size (220, 200);

		var buffer = new Gtk.TextBuffer (null); //stores text to be displayed
		var textview = new Gtk.TextView.with_buffer (buffer); //displays TextBuffer
		textview.set_wrap_mode (Gtk.WrapMode.WORD); //sets line wrapping

		var scrolled_window = new Gtk.ScrolledWindow (null, null);
		scrolled_window.set_policy (Gtk.PolicyType.AUTOMATIC,
		                            Gtk.PolicyType.AUTOMATIC);

		scrolled_window.add (textview);
		scrolled_window.set_border_width (5);

		this.add (scrolled_window);
	}
}
/* main creates and runs the application. */
public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.TextBuffer.html">Gtk.TextBuffer</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.TextView.html">Gtk.TextView</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ScrolledWindow.html">Gtk.ScrolledWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.WrapMode.html">Gtk.WrapMode</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.PolicyType.html">Gtk.PolicyType</link></p></item>
</list>
</page>
