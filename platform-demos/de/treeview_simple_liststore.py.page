<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="treeview_simple_liststore.py" xml:lang="de">
  <info>
    <title type="text">Simple TreeView with ListStore (Python)</title>
    <link type="guide" xref="beginner.py#treeview"/>
    <link type="seealso" xref="model-view-controller.py"/>
    <link type="next" xref="treeview_treestore.py"/>
    <revision version="0.2" date="2012-06-30" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A TreeView displaying a ListStore (simpler example)</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Simple TreeView with ListStore</title>
  <media type="image" mime="image/png" src="media/treeview_simple_liststore.png"/>
  <p>This TreeView displays a simple ListStore with the selection "changed" signal connected.</p>

  <links type="section"/>

  <section id="code">
    <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Pango
import sys

columns = ["First Name",
           "Last Name",
           "Phone Number"]

phonebook = [["Jurg", "Billeter", "555-0123"],
             ["Johannes", "Schmid", "555-1234"],
             ["Julita", "Inca", "555-2345"],
             ["Javier", "Jardon", "555-3456"],
             ["Jason", "Clinton", "555-4567"],
             ["Random J.", "Hacker", "555-5678"]]


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="My Phone Book", application=app)
        self.set_default_size(250, 100)
        self.set_border_width(10)

        # the data in the model (three strings for each row, one for each
        # column)
        listmodel = Gtk.ListStore(str, str, str)
        # append the values in the model
        for i in range(len(phonebook)):
            listmodel.append(phonebook[i])

        # a treeview to see the data stored in the model
        view = Gtk.TreeView(model=listmodel)
        # for each column
        for i, column in enumerate(columns):
            # cellrenderer to render the text
            cell = Gtk.CellRendererText()
            # the text in the first column should be in boldface
            if i == 0:
                cell.props.weight_set = True
                cell.props.weight = Pango.Weight.BOLD
            # the column is created
            col = Gtk.TreeViewColumn(column, cell, text=i)
            # and it is appended to the treeview
            view.append_column(col)

        # when a row is selected, it emits a signal
        view.get_selection().connect("changed", self.on_changed)

        # the label we use to show the selection
        self.label = Gtk.Label()
        self.label.set_text("")

        # a grid to attach the widgets
        grid = Gtk.Grid()
        grid.attach(view, 0, 0, 1, 1)
        grid.attach(self.label, 0, 1, 1, 1)

        # attach the grid to the window
        self.add(grid)

    def on_changed(self, selection):
        # get the model and the iterator that points at the data in the model
        (model, iter) = selection.get_selected()
        # set the label to a new value depending on the selection
        self.label.set_text("\n %s %s %s" %
                            (model[iter][0],  model[iter][1], model[iter][2]))
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Nützliche Methoden für ein TreeView-Widget</title>
    <p>The TreeView widget is designed around a <em>Model/View/Controller</em> design: the <em>Model</em> stores the data; the <em>View</em> gets change notifications and displays the content of the model; the <em>Controller</em>, finally, changes the state of the model and notifies the view of these changes. For more information, and for a list of useful methods for TreeModel, see <link xref="model-view-controller.py"/>.</p>
    <p>In line 44 the <code>"changed"</code> signal is connected to the callback function <code>on_changed()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
  </section>

  <section id="references">
    <title>API-Referenzen</title>
    <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeView.html">GtkTreeView</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeModel.html">GtkTreeModel</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkListStore.html">GtkListStore</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererText.html">GtkCellRendererText</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkTreeViewColumn.html">GtkTreeViewColumn</link></p></item>
      <item><p><link href="https://gitlab.gnome.org/GNOME/pygobject/blob/master/gi/overrides/Gtk.py">pygobject - Python bindings for GObject Introspection</link></p></item>
      <item><p><link href="http://developer.gnome.org/pango/stable/pango-Fonts.html">Schriften</link></p></item>
    </list>
  </section>
</page>
