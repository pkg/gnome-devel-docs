<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="button.c" xml:lang="de">
  <info>
    <title type="text">Knopf (C)</title>
    <link type="guide" xref="c#buttons"/>
    <link type="seealso" xref="dialog.c"/>
    <link type="seealso" xref="grid.c"/>
    <revision version="0.2" date="2012-06-22" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ein Knopf-Widget, das ein Signal ausgibt, wenn darauf geklickt wird.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Knopf</title>

  <media type="image" mime="image/png" src="media/button.png"/>
  <p>A button widget connected to a callback function that reverses its label when clicked.</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



/*This is the callback function. It is a handler function which 
reacts to the signal. In this case, it will cause the button label's 
string to reverse.*/
static void
button_clicked (GtkButton *button,
                gpointer   user_data)
{
  const char *old_label;
  char *new_label;

  old_label = gtk_button_get_label (button);
  new_label = g_utf8_strreverse (old_label, -1);

  gtk_button_set_label (button, new_label);
  g_free (new_label);
}



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *button;

  /*Create a window with a title and a default size*/
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "GNOME Button");
  gtk_window_set_default_size (GTK_WINDOW (window), 250, 50);

  /*Create a button with a label, and add it to the window*/
  button = gtk_button_new_with_label ("Click Me");
  gtk_container_add (GTK_CONTAINER (window), button);

  /*Connecting the clicked signal to the callback function*/
  g_signal_connect (GTK_BUTTON (button),
                    "clicked", 
                    G_CALLBACK (button_clicked), 
                    G_OBJECT (window));

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>In diesem Beispiel haben wir Folgendes verwendet:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkButton.html">GtkButton</link></p></item>
  <item><p><link href="http://developer.gnome.org/glib/stable/glib-Unicode-Manipulation.html#g-utf8-strreverse">Unicode-Manipulation</link></p></item>
</list>
</page>
