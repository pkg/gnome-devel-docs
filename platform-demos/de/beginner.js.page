<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="beginner.js" xml:lang="de">
  <info>
    <link type="guide" xref="js#code-samples"/>
    <revision version="0.2" date="2012-06-10" status="draft"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasettii@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Ein Leitfaden für Anfänger zum Schreiben von GNOME-Anwendungen in JavaScript, inklusive Codebeispielen und praktischen Übungen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Tutorial für Anfänger mit Codebeispielen</title>
<synopsis>
  <p>JavaScript is one of the most popular programming languages on the web. It's not just for the web, though. If you have even a basic understanding of JavaScript, you can write full-fledged applications for GNOME. <link href="https://wiki.gnome.org/Apps/Documents">GNOME Documents</link> is written in JavaScript, and so is <link href="https://live.gnome.org/GnomeShell/Tour">GNOME Shell</link>, the most basic part of GNOME.</p>
  <note style="tip"><p>GNOME Shell is what you see when you click on "Activities" in the top-left corner of your screen. It also controls the clock and the rest of the top panel. Besides showing how you to write GNOME applications, these tutorials will also show you how to use JavaScript to write GNOME Shell extensions, which give it new features or change the way it does things.</p></note>
</synopsis>

<section id="getting-started">
<title>Erste Schritte</title>
 <p>These tutorials are designed for people who already know how to write in JavaScript, and who have GNOME installed on their computers already, but who are new to developing GNOME applications. If you don't already know JavaScript, or if you need help getting GNOME set up, take a look at these resources first:</p>
<steps>
  <item><p><link href="http://eloquentjavascript.net/contents.html">Eloquent JavaScript</link> is a free, Creative Commons-licensed book, which explains the basics of JavaScript programming. Since you won't be writing JavaScript for the web, you only need to read up to chapter 10 or so.</p></item>
  <item><p><link href="http://www.gnome.org/getting-gnome/">Download GNOME</link> as part of a distribution, like Fedora, openSUSE, or Ubuntu. Each distribution has its own instructions for how to get GNOME.</p></item>
  <item><p><link xref="set-up-gedit.js">Richten Sie gedit</link> zum Schreiben von Anwendungen ein. Gedit, der Texteditor von GNOME, wird oft einfach »Texteditor« genannt.</p></item>
</steps>
</section>


<section id="tutorials">
<title>Einführungen</title>
</section>

<section id="samples">
<title>Codebeispiele</title>
  <p>These samples show how to use widgets in your GNOME applications. Each one demonstrates a complete application which showcases the featured widget. At the end of each sample, you will find links to more detailed reference material.</p>
  <p>So führen Sie die Codebeispiele aus:</p>
  <steps>
    <item><p>Copy and paste the code into <var>filename</var>.js</p></item>
    <item><p>Geben Sie Folgendes im Terminal ein:</p>
          <screen>gjs <var>Dateiname</var>.js</screen></item>
  </steps>

  <section id="windows" style="2column"><title>Fenster</title>
  </section>
  <section id="display-widgets" style="2column"><title>Widgets anzeigen</title>
  </section>
  <section id="buttons" style="2column"><title>Knöpfe und Auslöser</title>
  </section>
  <section id="entry" style="2column"><title>Numerische und Textdateneingabe</title>
  </section>
  <section id="multiline" style="2column"><title>Ein mehrzeiliger Texteditor</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Menü-, Kombinationsfeld- und Werkzeugleisten-Widgets</title>
  </section>
  <section id="treeview" style="2column"><title>TreeView-Widget</title>
  </section>
  <section id="selectors"><title>Auswähler</title>
    <section id="file-selectors"><title>Dateiwähler</title>
    </section>
    <section id="font-selectors"><title>Schriftwähler</title>
    </section>
    <section id="color-selectors"><title>Farbwähler</title>
    </section>
  </section>
  <section id="layout" style="2column"><title>Layout-Container</title>
  </section>
  <section id="ornaments" style="2column"><title>Ornamente</title>
  </section>
  <section id="scrolling" style="2column"><title>Bildlauf</title>
  </section>
  <section id="misc" style="2column"><title>Verschiedenes</title>
  </section>
</section>

<section id="exercises">
<title>Übungen</title>
</section>
</page>
