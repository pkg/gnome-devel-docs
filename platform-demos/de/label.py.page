<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="label.py" xml:lang="de">
  <info>
    <title type="text">Label (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="seealso" xref="properties.py"/>
    <link type="seealso" xref="strings.py"/>
    <link type="next" xref="properties.py"/>
    <revision version="0.2" date="2012-06-18" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Sebastian Pölsterl</name>
      <email its:translate="no">sebp@k-d-w.org</email>
      <years>2012</years>
    </credit>

    <desc>Ein Widget, das eine kleine bis mittlere Menge an Text anzeigt</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Mario Blättermann</mal:name>
      <mal:email>mario.blaettermann@gmail.com</mal:email>
      <mal:years>2011, 2013, 2016, 2018, 2021</mal:years>
    </mal:credit>
  </info>

  <title>Label</title>
  <media type="image" mime="image/png" src="media/label.png"/>
  <p>A simple label</p>

  <links type="section"/>

  <section id="code">
  <title>Code, der zum Generieren dieses Beispiels verwendet wurde</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # constructor for a Gtk.ApplicationWindow

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, 100)

        # create a label
        label = Gtk.Label()
        # set the text of the label
        label.set_text("Hello GNOME!")
        # add the label to the window
        self.add(label)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

    <p>Another way to obtain what we have in the example is to create the label as an instance of another class and add it to the instance of <code>MyWindow</code> in the <code>do_activate(self)</code> method:</p>
    <note>
      <p>The highlighted lines indicate code that is different from the previous snippet.</p>
    </note>
      <code mime="text/x-python">
# a class to define a window
class MyWindow(Gtk.ApplicationWindow):
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, 100)

# a class to define a label
<e:hi>
class MyLabel(Gtk.Label):
    def __init__(self):
        Gtk.Label.__init__(self)
        self.set_text("Hello GNOME!")
</e:hi>

class MyApplication(Gtk.Application):
    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        # create an instance of MyWindow
        win = MyWindow(self)

        # create an instance of MyLabel
<e:hi>
        label = MyLabel()
</e:hi>
        # and add it to the window
<e:hi>
        win.add(label)
</e:hi>
        # show the window and everything on it
        win.show_all()</code>

  </section>

  <section id="methods">
  <title>Nützliche Methoden für ein Label-Widget</title>
  
  <note style="tip">
    <p>An explanation of how to work with strings in GTK+ can be found in <link xref="strings.py"/>.</p>
  </note>

  <list>
    <item><p><code>set_line_wrap(True)</code> breaks lines if the text of the label exceeds the size of the widget.</p></item>
    <item><p><code>set_justify(Gtk.Justification.LEFT)</code> (or <code>Gtk.Justification.RIGHT, Gtk.Justification.CENTER, Gtk.Justification.FILL</code>) sets the alignment of the lines in the text of the label relative to each other. The method has no effect on a single-line label.</p></item>
    <item><p>For decorated text we can use <code>set_markup("text")</code>, where <code>"text"</code> is a text in the <link href="http://developer.gnome.org/pango/stable/PangoMarkupFormat.html">Pango Markup Language</link>. An example:</p>
      <code mime="text/x-python">
label.set_markup("Text can be &lt;small&gt;small&lt;/small&gt;, &lt;big&gt;big&lt;/big&gt;, "
                 "&lt;b&gt;bold&lt;/b&gt;, &lt;i&gt;italic&lt;/i&gt; and even point to somewhere "
                 "on the &lt;a href=\"http://www.gtk.org\" "
                 "title=\"Click to find out more\"&gt;internet&lt;/a&gt;.")</code>
    </item>
  </list>
  </section>

  <section id="references">
  <title>API-Referenzen</title>
  <p>In diesem Beispiel haben wir Folgendes verwendet:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
  </list>
  </section>
</page>
