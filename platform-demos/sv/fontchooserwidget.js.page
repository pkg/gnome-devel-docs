<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="fontchooserwidget.js" xml:lang="sv">
  <info>
    <title type="text">FontChooserWidget (Javascript)</title>
    <link type="guide" xref="beginner.js#font-selectors"/>
    <revision version="0.2" date="2013-06-25" status="review"/>

    <credit type="author copyright">
      <name>Meg Ford</name>
      <email its:translate="no">megford@gnome.org</email>
      <years>2013</years>
    </credit>

    <desc>En komponent för att välja ett typsnitt</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>FontChooserWidget</title>

  <media type="image" mime="image/png" src="media/fontchooserwidget.png"/>
  <p>En FontChooserWidget med en återanropsfunktion.</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>
    <code mime="application/javascript" style="numbered">//!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;

class FontChooserWidgetExample {

    // Skapa programmet i sig
    constructor() {
        this.application = new Gtk.Application({ application_id: 'org.example.fontchooserwidget' });

        // Anslut ”activate”- och ”startup”-signaler till återanropsfunktionerna
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Återanropsfunktion för ”activate”-signal visar fönster när den aktiveras
    _onActivate() {
        this.window.present();
    }

    // Återanropsfunktion för ”startup”-signal bygger användargränssnittet
    _onStartup() {
        this._buildUI();
    }

    // Bygg programmets användargränssnitt
    _buildUI() {
        // Skapa programfönstret
        this.window = new Gtk.ApplicationWindow  ({ application: this.application,
                                                    window_position: Gtk.WindowPosition.CENTER,
                                                    title: "FontChooserWidget",
                                                    default_width: 200,
                                                    default_height: 200,
                                                    border_width: 10 });

        this.fontChooser = new Gtk.FontChooserWidget();
        // ett standardtypsnitt
        this.fontChooser.set_font("Sans");
        // en text för att förhandsvisa typsnittet
        this.fontChooser.set_preview_text("Detta är ett exempel på förhandsvisningstext!");

        // anslut signal från typsnittsväljaren till återanropsfunktionen
        this.fontChooser.connect("notify::font", this._fontCb.bind(this));

        // lägg till typsnittsväljaren till fönstret
        this.window.add(this.fontChooser);
        this.window.show_all();
   }

     // återanropsfunktion:
     _fontCb() {
        // skriv i terminalen
        print("Du valde typsnittet " + this.fontChooser.get_font());
    }
};

// Kör programmet
let app = new FontChooserWidgetExample();
app.application.run (ARGV);
</code>
  </section>

  <section id="references">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkFontChooserWidget.html">GtkFontChooserWidget</link></p></item>
    </list>
  </section>
</page>
