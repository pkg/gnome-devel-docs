<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="aboutdialog.vala" xml:lang="sv">
  <info>
  <title type="text">AboutDialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <link type="seealso" xref="button.vala"/>
    <link type="seealso" xref="linkbutton.vala"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email its:translate="no">desrt@desrt.ca</email>
      <years>2012</years>
    </credit>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>


    <desc>Visa information om ett program</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>AboutDialog</title>
  <media type="image" mime="image/png" src="media/aboutdialog_GMenu.png"/>
  <p>Ett AboutDialog-exempel som använder Gtk.ApplicationWindow och Menu</p>
  <note><p><em style="bold">Du måste köra Gtk3.4 eller senare för att detta ska fungera</em></p></note>

<code mime="text/x-csharp" style="numbered">/* Ett fönster i programmet */
public class Window : Gtk.ApplicationWindow {

	/* Konstruktorn */
	public Window (Application app) {
		Object (application: app, title: "AboutDialog-exempel");

		var about_action = new SimpleAction ("about", null);

		about_action.activate.connect (this.about_cb);
		this.add_action (about_action);
		this.show_all ();
	}

	/* Detta är återanropsfunktionen ansluten till signalen ”activate”
	 * för en SimpleAction med namnet about_action.
	 */
	void about_cb (SimpleAction simple, Variant? parameter) {
		string[] authors = { "GNOME:s dokumentationsgrupp", null };
		string[] documenters = { "GNOME:s dokumentationsgrupp", null };

		Gtk.show_about_dialog (this,
                               "program-name", ("GtkApplication-exempel"),
                               "copyright", ("Copyright \xc2\xa9 2012 GNOME:s dokumentationsgrupp"),
                               "authors", authors,
                               "documenters", documenters,
                               "website", "http://developer.gnome.org",
                               "website-label", ("GNOME:s utvecklarwebbplats"),
                               null);
	}
}

/* Detta är programmet */
public class Application : Gtk.Application {

	/* Här åsidosätter vi ”activate”-signalen för GLib.Application */
	protected override void activate () {
		new Window (this);
	}

	/* Här åsidosätter vi ”startup”-signalen för GLib.Application */
	protected override void startup () {

		base.startup ();

		var menu = new Menu ();
		menu.append ("Om", "win.about");
		menu.append ("Avsluta", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		//quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	/* Konstruktorn */
	public Application () {
		Object (application_id: "org.example.application");
	}
}

/* main-funktionen skapar Application och kör den */
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.set_default_size.html">set_default_size</link></p></item>
</list>
</page>
