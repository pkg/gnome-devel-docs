<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="comboboxtext.js" xml:lang="sv">
  <info>
  <title type="text">ComboBoxText (Javascript)</title>
    <link type="guide" xref="beginner.js#menu-combo-toolbar"/>
    <link type="seealso" xref="GtkApplicationWindow.js"/>
    <link type="seealso" xref="messagedialog.js"/>
    <revision version="0.1" date="2012-07-06" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>En rullgardinsmeny med bara text</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ComboBoxText</title>
  <media type="image" mime="image/png" src="media/combobox.png"/>
  <p>A ComboBox is a drop-down menu. The difference between a <link xref="combobox.js">ComboBox</link> and a ComboBoxText is that a ComboBoxText just has basic text options, while a full ComboBox uses a ListStore or TreeStore (which are basically spreadsheets) to show things like branching options, or pictures to go alongside each choice.</p>
  <note><p>Om du inte behöver de extra funktionerna hos en fullständig ComboBox, eller inte är bekväm med att jobba med ListStore och TreeStore, så kan du finna det mycket enklare att använda en ComboBoxText närhelst möjligt.</p></note>
    <links type="section"/>

  <section id="imports">
    <title>Bibliotek att importera</title>
    <code mime="application/javascript">
#!/usr/bin/gjs

const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
</code>
    <p>Detta är biblioteken som vi behöver importera för att detta program ska köras. Kom ihåg att raden som säger till GNOME att vi använder Gjs alltid behöver vara först.</p>
  </section>

  <section id="applicationwindow">
    <title>Skapa programfönstret</title>
    <code mime="application/javascript">
const ComboBoxTextExample = new Lang.Class ({
    Name: 'ComboBoxText-exempel',

    // Skapa programmet i sig
    _init: function () {
        this.application = new Gtk.Application ({
            application_id: 'org.example.jscomboboxtext'});

        // Anslut ”activate”- och ”startup”-signaler till återanropsfunktionerna
        this.application.connect('activate', Lang.bind(this, this._onActivate));
        this.application.connect('startup', Lang.bind(this, this._onStartup));
    },

    // Återanropsfunktion för ”activate”-signal visar fönster när den aktiveras
    _onActivate: function () {
        this._window.present ();
    },

    // Återanropsfunktion för ”startup”-signal bygger användargränssnittet
    _onStartup: function () {
        this._buildUI ();
    },
</code>
    <p>All kod för detta exempel hamnar i klassen MessageDialogExample. Koden ovan skapar en <link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link> att ha våra komponenter och fönstret i.</p>
    <code mime="application/javascript">
    // Bygg programmets användargränssnitt
    _buildUI: function () {

        // Skapa programfönstret
        this._window = new Gtk.ApplicationWindow  ({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            title: "Välkommen till GNOME",
            default_width: 200,
            border_width: 10 });
</code>
    <p>_buildUI-funktionen är var vi stoppar all kod för att skapa programmets användargränssnitt. Det första steget är att skapa ett nytt <link xref="GtkApplicationWindow.js">Gtk.ApplicationWindow</link> att stoppa alla våra komponenter i.</p>
  </section>

  <section id="comboboxtext">
    <title>Skapa vår ComboBoxText</title>
    <code mime="application/javascript">
        // Skapa kombinationsrutan
        this._comboBoxText = new Gtk.ComboBoxText();

        // Fyll kombinationsrutan
        let distros = ["Välj distribution", "Fedora", "Mint", "Suse"];
        for (let i = 0; i &lt; distros.length; i++)
            this._comboBoxText.append_text (distros[i]);
        this._comboBoxText.set_active (0);

        // Anslut kombinationsrutans ”changed”-signal till vår återanropsfunktion
        this._comboBoxText.connect ('changed', Lang.bind (this, this._onComboChanged));
</code>
    <p>After we create the ComboBoxText, we use its <file>append_text</file> method to add text strings to it. Like the entries in an array, they each have a number for an ID, starting with 0. To make things simpler, you can actually create an array for your ComboBoxText entries, then use a for loop to append them in order, like we did here.</p>
    <p>After we populate the ComboBoxText, we set its first entry to be active, so that we'll see the "Select distribution" line before we click on it. Then we connect its <file>changed</file> signal to the _onComboChanged function, so that it's called whenever you make a new selection from the drop-down menu.</p>
    <note><p>If you'd like to add an entry to a ComboBoxText, you can use the <file>insert_text</file> method. And if you'd rather use a text string as an ID for each entry than rely on numbers alone, you can use the <file>append</file> and <file>insert</file> methods. See the links at the bottom of this tutorial for the details of how to use them.</p></note>

    <code mime="application/javascript">
        // Lägg till kombinationsrutan till fönstret
        this._window.add (this._comboBoxText);

        // Visa fönstret och alla barnkomponenter
        this._window.show_all();
    },
</code>
    <p>Finally, we add the ComboBoxText to the window, and tell the window to show itself and the widget inside it.</p>
  </section>

  <section id="function">
    <title>Funktion som hanterar ditt val</title>
    <code mime="application/javascript">
    _onComboChanged: function () {

        // Svaren som vi kommer använda för vår meddelandedialog
        let responses = ["",
            "Fedora är en gemenskapsdistribution sponsrad av Red Hat.",
            "Mint är en populär distribution baserad på Ubuntu.",
            "SUSE är ett namn som delas av två olika distributioner."];
</code>
    <p>We're going to create a pop-up <link xref="messagedialog.js">MessageDialog</link>, which shows you a message based on which distro you select. First, we create the array of responses to use. Since the first string in our ComboBoxText is just the "Select distribution" message, we make the first string in our array blank.</p>

    <code mime="application/javascript"><![CDATA[
        // Which combobox item is active?
        let activeItem = this._comboBoxText.get_active();

        // No messagedialog if you chose "Select distribution"
        if (activeItem != 0) {
            this._popUp = new Gtk.MessageDialog ({
                transient_for: this._window,
                modal: true,
                buttons: Gtk.ButtonsType.OK,
                message_type: Gtk.MessageType.INFO,
                text: responses[activeItem]});

            // Connect the OK button to a handler function
            this._popUp.connect ('response', Lang.bind (this, this._onDialogResponse));

            // Show the messagedialog
            this._popUp.show();
        }

    },
]]></code>
    <p>Before showing a MessageDialog, we first test to make sure you didn't choose the "Select distribution" message. After that, we set its text to be the entry in the array that corresponds to the active entry in our ComboBoxText. We do that using the <file>get_active</file> method, which returns the number ID of your selection.</p>
    <note><p>Other methods you can use include <file>get_active_id,</file> which returns the text ID assigned by <file>append,</file> and <file>get_active_text,</file> which returns the full text of the string you selected.</p></note>
    <p>After we create the MessageDialog, we connect its response signal to the _onDialogResponse function, then tell it to show itself.</p>

    <code mime="application/javascript">
    _onDialogResponse: function () {

        this._popUp.destroy ();

    }

});
</code>
    <p>Since the only button the MessageDialog has is an OK button, we don't need to test its response_id to see which button was clicked. All we do here is destroy the popup.</p>

    <code mime="application/javascript">
// Kör programmet
let app = new ComboBoxTextExample ();
app.application.run (ARGV);
</code>
    <p>Slutligen skapar vi en ny instans av den slutförda ComboBoxTextExample-klassen, och startar programmet.</p>
  </section>

  <section id="complete">
    <title>Fullständigt kodexempel</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';
const Gtk = imports.gi.Gtk;

class ComboBoxTextExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.jscomboboxtext'});

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents windows when active
    _onActivate() {
        this._window.present ();
    }

    // Callback function for 'startup' signal builds the UI
    _onStartup() {
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow  ({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            title: "Welcome to GNOME",
            default_width: 200,
            border_width: 10 });

        // Create the combobox
        this._comboBoxText = new Gtk.ComboBoxText();

        // Populate the combobox
        let distros = ["Select distribution", "Fedora", "Mint", "Suse"];
        for (let i = 0; i &lt; distros.length; i++)
            this._comboBoxText.append_text (distros[i]);
        this._comboBoxText.set_active (0);

        // Connect the combobox's 'changed' signal to our callback function
        this._comboBoxText.connect ('changed', this._onComboChanged.bind(this));

        // Add the combobox to the window
        this._window.add (this._comboBoxText);

        // Show the window and all child widgets
        this._window.show_all();
    }

    _onComboChanged() {

        // The responses we'll use for our messagedialog
        let responses = ["",
            "Fedora is a community distro sponsored by Red Hat.",
            "Mint is a popular distro based on Ubuntu.",
            "SUSE is a name shared by two separate distros."];

        // Which combobox item is active?
        let activeItem = this._comboBoxText.get_active();

        // No messagedialog if you chose "Select distribution"
        if (activeItem != 0) {
            this._popUp = new Gtk.MessageDialog ({
                transient_for: this._window,
                modal: true,
                buttons: Gtk.ButtonsType.OK,
                message_type: Gtk.MessageType.INFO,
                text: responses[activeItem]});

            // Connect the OK button to a handler function
            this._popUp.connect ('response', this._onDialogResponse.bind(this));

            // Show the messagedialog
            this._popUp.show();
        }

    }

    _onDialogResponse() {

        this._popUp.destroy ();

    }

};

// Run the application
let app = new ComboBoxTextExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="in-depth">
    <title>Djupgående dokumentation</title>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.ComboBoxText.html">Gtk.ComboBoxText</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.MessageDialog.html">Gtk.MessageDialog</link></p></item>
</list>
  </section>
</page>
