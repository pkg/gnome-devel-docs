<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="photo-wall.c" xml:lang="sv">
  <info>
    <title type="text">Fotovägg (C)</title>
    <link type="guide" xref="c#examples"/>

    <desc>En bildvisare med Clutter</desc>

    <revision pkgversion="0.1" version="0.1" date="2011-03-22" status="review"/>
    <credit type="author">
      <name>Chris Kühl</name>
      <email its:translate="no">chrisk@openismus.com</email>
    </credit>
    <credit type="author">
      <name>Johannes Schmid</name>
      <email its:translate="no">jhs@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Fotovägg</title>

<synopsis>
  <p>För detta exempel kommer vi bygga en enkel bildvisare med Clutter. Du kommer lära dig:</p>
  <list>
    <item><p>How to size and position <code>ClutterActor</code>s </p></item>
    <item><p>Hur du placerar en bild i en <code>ClutterActor</code></p></item>
    <item><p>Hur du gör enkla övergångar med Clutters animeringsramverk</p></item>
    <item><p>How to make <code>ClutterActor</code>s respond to mouse events</p></item>
    <item><p>Hur du får filnamn från en katalog</p></item>
  </list>
</synopsis>

<section id="intro">
  <title>Introduktion</title>
  <p>Clutter är ett bibliotek för att skapa dynamiska användargränssnitt med OpenGL för hårdvaruacceleration. Detta exempel demonstrerar en liten, men central, del av Clutter-biblioteket för att skapa ett enkelt men vackert bildvisningsprogram.</p>
  <p>
    To help us reach our goal we will be utilising a few other common pieces of GLib as well. Most importantly, we'll use one <code>GPtrArray</code>, a dynamic array of pointers, to hold the file path names. We will also use <code>GDir</code>, a utility for working with directories, to access our image directory and gather file paths.
  </p>
</section>

<section id="anjuta">
  <title>Skapa ett projekt i Anjuta</title>
  <p>Innan du börjar koda kommer du behöva konfigurera ett nytt projekt i Anjuta. Detta kommer skapa alla filer som du behöver för att bygga och köra koden senare. Det är också användbart för att hålla allting samlat.</p>
  <steps>
    <item>
    <p>Starta Anjuta och klicka på <guiseq><gui>Arkiv</gui><gui>Ny</gui><gui>Projekt</gui></guiseq> för att öppna projektguiden.</p>
    </item>
    <item>
    <p>Välj <gui>GTK+ (enkel)</gui> från fliken <gui>C</gui>, klicka på <gui>Fortsätt</gui>, och fyll i dina detaljer på de nästkommande sidorna. Använd <file>photo-wall</file> som projektnamn och katalog.</p>
   	</item>
    <item>
    <p>Säkerställ att <gui>Använd GtkBuilder för användargränssnitt</gui> är inaktiverat då vi kommer skapa användargränssnittet manuellt i denna handledning. Se handledningen <link xref="guitar-tuner.c">Gitarrstämmare</link> för användning av gränssnittsbyggaren.</p>
    </item>
    <item>
    <p>Aktivera <gui>Konfigurera externa paket</gui>. På nästa sida, välj <em>clutter-1.0</em> från listan för att inkludera Clutter-biblioteket i ditt projekt.</p>
    </item>
    <item>
    <p>Klicka på <gui>Verkställ</gui> så kommer projektet skapas åt dig. Öppna <file>src/main.c</file> från flikarna <gui>Projekt</gui> eller <gui>Filer</gui>. Du bör se kod som börjar med raderna:</p>
    <code mime="text/x-csrc">
#include &lt;config.h&gt;
#include &lt;gtk/gtk.h&gt;</code>
    </item>
  </steps>
</section>

<section id="look">
  <title>En titt på fotoväggen</title>
  <p>Vår bildvisare visar en vägg av bilder för användaren.</p>
  <media type="image" mime="image/png" src="media/photo-wall.png"/>
  <p>När en bild klickas på animeras den så att den fyller visningsområdet. När bilden som har fokus klickas på så returneras den till sin ursprungliga position med en animering med samma längd, 500 millisekunder.</p>
  <media type="image" mime="image/png" src="media/photo-wall-focused.png"/>
</section>

<section id="setup">
  <title>Initial konfiguration</title>
  <p>
    The following code segment contains many of the defines and variables we will be using in the following sections. Use this as a reference for later sections. Copy this code to the beginning of <file>src/main.c</file>:
  </p>
<code mime="text/x-csrc" style="numbered">
#include &lt;gdk-pixbuf/gdk-pixbuf.h&gt;
#include &lt;clutter/clutter.h&gt;

#define STAGE_WIDTH  800
#define STAGE_HEIGHT 600

#define THUMBNAIL_SIZE 200
#define ROW_COUNT (STAGE_HEIGHT / THUMBNAIL_SIZE)
#define COL_COUNT (STAGE_WIDTH  / THUMBNAIL_SIZE)
#define THUMBNAIL_COUNT (ROW_COUNT * COL_COUNT)

#define ANIMATION_DURATION_MS 500

#define IMAGE_DIR_PATH "./berlin_images/"

static GPtrArray *img_paths;

static ClutterPoint unfocused_pos;

</code>
</section>

<section id="code">
  <title>Ett hopp in i koden</title>
  <p>We will start by taking a look at the <code>main()</code> function as a whole. Then we'll discuss the other code sections in detail.
  Change the <file>src/main.c</file> to contain this <code>main()</code> function. You can delete the
  <code>create_window()</code> function as we don't need it in this example.</p>
  <code mime="text/x-csrc" style="numbered"><![CDATA[
int
main(int argc, char *argv[])
{
    ClutterColor stage_color = { 16, 16, 16, 255 };
    ClutterActor *stage = NULL;

    if (clutter_init (&argc, &argv) != CLUTTER_INIT_SUCCESS)
        return 1;

    stage = clutter_stage_new();
    clutter_actor_set_size(stage, STAGE_WIDTH, STAGE_HEIGHT);
    clutter_actor_set_background_color(stage, &stage_color);
    clutter_stage_set_title(CLUTTER_STAGE (stage), "Photo Wall");
    g_signal_connect(stage, "destroy", G_CALLBACK(clutter_main_quit), NULL);

    load_image_path_names();

    guint row = 0;
    guint col = 0;
    for(row=0; row < ROW_COUNT; ++row)
    {
        for(col=0; col < COL_COUNT; ++col)
        {
            const char *img_path = g_ptr_array_index(img_paths, (row * COL_COUNT) + col);
            GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file_at_size(img_path, STAGE_HEIGHT, STAGE_HEIGHT, NULL);
            ClutterContent *image = clutter_image_new ();
            ClutterActor *actor = clutter_actor_new ();

            if (pixbuf != NULL)
            {
                clutter_image_set_data(CLUTTER_IMAGE(image),
                                       gdk_pixbuf_get_pixels(pixbuf),
                                       gdk_pixbuf_get_has_alpha(pixbuf)
                                           ? COGL_PIXEL_FORMAT_RGBA_8888
                                           : COGL_PIXEL_FORMAT_RGB_888,
                                       gdk_pixbuf_get_width(pixbuf),
                                       gdk_pixbuf_get_height(pixbuf),
                                       gdk_pixbuf_get_rowstride(pixbuf),
                                       NULL);
            }

            clutter_actor_set_content(actor, image);
            g_object_unref(image);
            g_object_unref(pixbuf);

            initialize_actor(actor, row, col);
            clutter_actor_add_child(stage, actor);
        }
    }

    /* Show the stage. */
    clutter_actor_show(stage);

    /* Start the clutter main loop. */
    clutter_main();

    g_ptr_array_unref(img_paths);

    return 0;
}]]></code>
  <list>
    <item><p>Line 4: <code>ClutterColor</code> is defined by setting the red, green, blue and transparency (alpha) values. The values range from 0-255. For transparency a value of 255 is opaque.</p></item>
    <item><p>Line 7: Du måste initiera Clutter. Om du glömmer att göra detta kommer du stöta på väldigt märkliga fel. Detta är en varning.</p></item>
    <item><p>Lines 10‒14: Here we create a new <code>ClutterStage</code> . We then set the size using the defines from the previous section and the address of the <code>ClutterColor</code> we just defined.</p>
      <note><p>A <code>ClutterStage</code> is the top-level <code>ClutterActor</code> onto which other <code>ClutterActor</code>s are placed.</p></note>
</item>
    <item><p>Line 16: Here we call our function for getting the image file paths. We'll look at this in a bit.</p></item>
    <item><p>Lines 18‒49: This is where we set up the <code>ClutterActor</code>s, load the images and place them into their spot in the image wall. We will look at this in detail in the next section.</p></item>
    <item><p>Line 52: Show the stage and <em>all its children</em>, meaning our images.</p></item>
    <item><p>Line 55: Start the Clutter main loop.</p></item>
  </list>
</section>

<section id="actors">
  <title>Konfigurera våra bildaktörer</title>
 <note><p>I Clutter är en aktör det mest grundläggande visuella elementet. I princip allt du ser är en aktör.</p></note>
<p>
In this section, we are going to take a closer look at the loop used for setting up the <code>ClutterActor</code>s that will display our images.
</p>
  <code mime="text/x-csrc" style="numbered">
guint row = 0;
guint col = 0;
for(row=0; row &lt; ROW_COUNT; ++row)
{
    for(col=0; col &lt; COL_COUNT; ++col)
    {
        const char *img_path = g_ptr_array_index(img_paths, (row * COL_COUNT) + col);
        GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file_at_size(img_path, STAGE_HEIGHT, STAGE_HEIGHT, NULL);
        ClutterContent *image = clutter_image_new ();
        ClutterActor *actor = clutter_actor_new ();

        if (pixbuf != NULL)
        {
            clutter_image_set_data(CLUTTER_IMAGE(image),
                                   gdk_pixbuf_get_pixels(pixbuf),
                                   gdk_pixbuf_get_has_alpha(pixbuf)
                                       ? COGL_PIXEL_FORMAT_RGBA_8888
                                       : COGL_PIXEL_FORMAT_RGB_888,
                                   gdk_pixbuf_get_width(pixbuf),
                                   gdk_pixbuf_get_height(pixbuf),
                                   gdk_pixbuf_get_rowstride(pixbuf),
                                   NULL);
        }

        clutter_actor_set_content(actor, image);
        g_object_unref(image);
        g_object_unref(pixbuf);

        initialize_actor(actor, row, col);
        clutter_actor_add_child(stage, actor);
    }
}

</code>
<list>
  <item><p>Line 7: Here we want to get the path at the <var>n</var>th location in the <code>GPtrArray</code> that is holding our image path names. The <var>n</var>th position is calculated based on <code>row</code> and <code>col</code>.</p>
  </item>
  <item><p>Line 8‒23: This is where we actually create the <code>ClutterActor</code> and place the image into the actor. The first argument is the path which we access through our <code>GSList</code> node. The second argument is for error reporting but we are ignoring that to keep things short.</p>
  </item>
  <item><p>Line 47: This adds the <code>ClutterActor</code> to the stage, which is a container. It also assumes ownership of the <code>ClutterActor</code> which is something you'll want to look into as you get deeper into GNOME development. See the <link href="http://library.gnome.org/devel/gobject/stable/gobject-memory.html"><code>GObject</code> documentation</link> for the gory details.</p>
  </item>
</list>
</section>

<section id="load">
  <title>Läsa in bilderna</title>
  <p>Låt oss ta en kort rast från Clutter för att se hur vi kan få filnamnen från vår bildkatalog.</p>
  <code mime="text/x-csrc" style="numbered">
static void
load_image_path_names()
{
    /* Säkerställ att vi kan komma åt katalogen. */
    GError *error = NULL;
    GDir *dir = g_dir_open(IMAGE_DIR_PATH, 0, &amp;error);
    if(error)
    {
        g_warning("g_dir_open() failed with error: %s\n", error-&gt;message);
        g_clear_error(&amp;error);
        return;
    }

    img_paths = g_ptr_array_new_with_free_func (g_free);

    const gchar *filename = g_dir_read_name(dir);
    while(filename)
    {
        if(g_str_has_suffix(filename, ".jpg") || g_str_has_suffix(filename, ".png"))
        {
            gchar *path = g_build_filename(IMAGE_DIR_PATH, filename, NULL);
            g_ptr_array_add (img_paths, path);
        }
        filename = g_dir_read_name(dir);
    }
}</code>
  <list>
    <item><p>Rad 5 och 12: Detta öppnar vår katalog eller, om ett fel uppstod, returnerar efter att ha skrivit ut ett felmeddelande.</p></item>
    <item><p>Lines 16‒25: The first line gets another file name from the <code>GDir</code> we opened earlier. If there was an image file (which we check by looking at its extension, ".png" or ".jpg") in the directory we proceed to prepend the image directory path to the filename and prepend that to the list we set up earlier. Lastly we attempt to get the next path name and reenter the loop if another file was found.</p></item>
  </list>
</section>

<section id="actors2">
  <title>Konfigurera aktörerna</title>
  <p>
     We now take a look at the sizing and  positioning of <code>ClutterActor</code>s and also readying the <code>ClutterActor</code> for user interaction.
  </p>
  <code mime="text/x-csrc" style="numbered"><![CDATA[
/* This function handles setting up and placing the rectangles. */
static void
initialize_actor(ClutterActor *actor, guint row, guint col)
{
    clutter_actor_set_size(actor, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
    clutter_actor_set_position(actor, col * THUMBNAIL_SIZE, row * THUMBNAIL_SIZE);
    clutter_actor_set_reactive(actor, TRUE);

    g_signal_connect(actor,
                     "button-press-event",
                     G_CALLBACK(actor_clicked_cb),
                     NULL);
}]]></code>
  <list>
    <item>
      <p>Line 7: Setting an actor reactive means that it reacts to events, such as <code>button-press-event</code> in our case. For Photo Wall, all <code>ClutterActor</code>s in the wall should initially be reactive.</p>
    </item>
    <item>
      <p>Rad 9‒12: Nu ansluter vi <code>button-press-event</code> till återanropet <code>actor_clicked_cb</code> som vi kommer ta en titt på härnäst.</p>
    </item>
  </list>
  <p>Vid denna punkt har vi en vägg av bilder som är redo att tittas på.</p>
</section>

<section id="click">
  <title>Reagera på klicken</title>
  <p>

  </p>
  <code mime="text/x-csrc" style="numbered"><![CDATA[
static gboolean
actor_clicked_cb(ClutterActor *actor,
                 ClutterEvent *event,
                 gpointer      user_data)
{
    /* Flag to keep track of our state. */
    static gboolean is_focused = FALSE;
    ClutterActorIter iter;
    ClutterActor *child;

    /* Reset the focus state on all the images */
    clutter_actor_iter_init (&iter, clutter_actor_get_parent(actor));
    while (clutter_actor_iter_next(&iter, &child))
      clutter_actor_set_reactive(child, is_focused);

    clutter_actor_save_easing_state(actor);
    clutter_actor_set_easing_duration(actor, ANIMATION_DURATION_MS);

    if(is_focused)
    {
        /* Restore the old location and size. */
        clutter_actor_set_position(actor, unfocused_pos.x, unfocused_pos.y);
        clutter_actor_set_size(actor, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
    }
    else
    {
        /* Save the current location before animating. */
        clutter_actor_get_position(actor, &unfocused_pos.x, &unfocused_pos.y);
        /* Only the currently focused image should receive events. */
        clutter_actor_set_reactive(actor, TRUE);

        /* Put the focused image on top. */
        clutter_actor_set_child_above_sibling(clutter_actor_get_parent(actor), actor, NULL);

        clutter_actor_set_position(actor, (STAGE_WIDTH - STAGE_HEIGHT) / 2.0, 0);
        clutter_actor_set_size(actor, STAGE_HEIGHT, STAGE_HEIGHT);
    }

    clutter_actor_restore_easing_state(actor);

    /* Toggle our flag. */
    is_focused = !is_focused;

    return TRUE;
}]]></code>
  <list>
    <item><p>Lines 1‒4: We have to make sure our callback function matches the signature required for the <code>button_clicked_event</code> signal. For our example, we will only use the first argument, the <code>ClutterActor</code> that is actually clicked.</p>
<note>
  <p>A few words on the arguments we are not using in this example. The <code>ClutterEvent</code> is different depending on what event is being handled. For example, a key event produces a <code>ClutterKeyEvent</code> from which you can get the key being pressed among other information. For mouse click events you get a <code>ClutterButtonEvent</code> from which you can get the <code>x</code> and <code>y</code> values. See the Clutter documentation for other <code>ClutterEvent</code> types.</p>
  <p>
    The <code>user_data</code> is what one uses to pass data into the function. A pointer to any data type can be passed in. If you need multiple data to be passed into the callback, you can place the data into a struct and pass its address in.
  </p>
</note></item>
    <item><p>Line 7: We set up a static flag to track which state we are in: wall mode or focus mode. We start out in wall mode so no image has focus. Thus, we set the flag to <code>FALSE</code> initially.</p></item>
    <item><p>Line 12‒14: These set the image actors to receive events if they are focused.</p></item>
    <item><p>Line 16‒17: Here we set the animation duration and save the current state.</p></item>
    <item><p>Lines 21‒23: Reaching this code means that one image currently has focus and we want to return to wall mode. Setting a position on a <code>ClutterActor</code> begins an animation with the duration that we set in line 17.</p>
    </item>
    <item><p>Line 24: Reaching this line of code means we are currently in the wall state and are about to give a <code>ClutterActor</code> focus. Here we save the starting position so that we can return to it later.</p></item>
    <item><p>Line 25: Setting the <code>ClutterActor</code>'s <code>reactive</code> property to <code>TRUE</code> makes this <code>ClutterActor</code> react to events. In this focused state the only <code>ClutterActor</code> that we want to receive events will be the <code>ClutterActor</code> being viewed. Clicking on the <code>ClutterActor</code> will return it to its starting position. </p></item>
    <item><p>Lines 27‒36: This is where we save the current position of the image, set it to receive events and then make it appear above the other images and start animating it to fill the stage.</p></item>
    <item><p>Line 39: Here we restore the easing state to what was set before we changed it in line 16.</p></item>
    <item><p>Line 42: Here we toggle the <code>is_focused</code> flag to the current state.</p></item>
<item><p>As mentioned previously, the <code>ClutterActor</code>s with higher <code>depth</code> values receive events but can allow <code>ClutterActor</code>s below them to also receive events. Returning <code>TRUE</code> will stop events from being passed down, while <code>FALSE</code> will pass events down.</p>
 <note>
   <p>Remember, however, that to receive events the <code>ClutterActor</code>s must be set <code>reactive</code>.</p>
 </note>
</item>
 </list>
</section>

<section id="run">
  <title>Bygg och kör programmet</title>
  <p>All of the code should now be ready to go.
  All you need now is some pictures to load. By default, the pictures are loaded from a <file>berlin_images</file> directory. If you want, you can change the <code>#define IMAGE_DIR_PATH</code> line near the top to refer to your photo directory, or create a <file>berlin_images</file> directory by clicking <guiseq><gui>Project</gui><gui>New Directory...</gui></guiseq> and creating a <file>berlin_images</file> directory as a subdirectory of the <file>photo-wall</file> directory. Make sure to put at least twelve images in the directory! <!--You can download the example images <link href="photo-wall/berlin_images/">here</link> (<link href="photo-wall/CREDITS">credits</link>).--></p>
  <p>When you have done that, click <guiseq><gui>Build</gui><gui>Build Project</gui></guiseq> to build everything again, then <guiseq><gui>Run</gui><gui>Execute</gui></guiseq> to start the application.</p>
  <p>If you haven't already done so, choose the <file>Debug/src/photo-wall</file> application in the dialog that appears. Finally, hit <gui>Run</gui> and enjoy!</p>
</section>

<section id="impl">
 <title>Referensimplementation</title>
 <p>Om du stöter på problem med handledningen kan du jämföra din kod med denna <link href="photo-wall/photo-wall.c">referenskod</link>.</p>
</section>

</page>
