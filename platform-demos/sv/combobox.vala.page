<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="combobox.vala" xml:lang="sv">
  <info>
  <title type="text">ComboBox (Vala)</title>
    <link type="guide" xref="beginner.vala#menu-combo-toolbar"/>
    <revision version="0.1" date="2012-05-07" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En komponent som används för att välja från en lista objekt</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ComboBox</title>
  <media type="image" mime="image/png" src="media/combobox.png"/>
  <p>Denna ComboBox skriver till terminalen då du ändrar ditt val.</p>

<code mime="text/x-csharp" style="numbered">/* A window in the application */
class MyWindow : Gtk.ApplicationWindow {

	/* An instance array of linux distributions belonging to this window. */
	string[] distros = {"Select distribution", "Fedora", "Mint", "Suse"};

	/* This enum makes the code more readable when we refer to
	 * the column as Column.DISTRO, instead of just 0.
	 */
	enum Column {
		DISTRO
	}

	/* Constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Welcome to GNOME");

		this.set_default_size (200, -1);
		this.border_width = 10;

		Gtk.ListStore liststore = new Gtk.ListStore (1, typeof (string));

		for (int i = 0; i &lt; distros.length; i++){
			Gtk.TreeIter iter;
			liststore.append (out iter);
			liststore.set (iter, Column.DISTRO, distros[i]);
		}

		Gtk.ComboBox combobox = new Gtk.ComboBox.with_model (liststore);
		Gtk.CellRendererText cell = new Gtk.CellRendererText ();
		combobox.pack_start (cell, false);

		combobox.set_attributes (cell, "text", Column.DISTRO);

		/* Set the first item in the list to be selected (active). */
		combobox.set_active (0);

		/* Connect the 'changed' signal of the combobox
		 * to the signal handler (aka. callback function).
		 */
		combobox.changed.connect (this.item_changed);

		/* Add the combobox to this window */
		this.add (combobox);
		combobox.show ();
	}

	/* Signal handler for the 'changed' signal of the combobox. */
	void item_changed (Gtk.ComboBox combo) {
		if (combo.get_active () !=0) {
			print ("You chose " + distros [combo.get_active ()] +"\n");
		}
	}
}

/* This is the application */
class MyApplication : Gtk.Application {

	/* Constructor */
	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}

	/* Override the activate signal of GLib.Application,
	 * which is inherited by Gtk.Application.
	 */
	protected override void activate () {

		/* Create the window of this application
		 * and show it.
		 */
		new MyWindow (this).show ();
	}
}

/* main creates and runs the application */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>I detta exempel använde vi följande:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ListStore.html">Gtk.ListStore</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ComboBox.html">Gtk.ComboBox</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CellRendererText.html">Gtk.CellRendererText</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CellLayout.set_attributes.html">set_attributes</link></p></item>
</list>
</page>
