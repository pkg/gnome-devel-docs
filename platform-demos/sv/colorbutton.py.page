<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="colorbutton.py" xml:lang="sv">
  <info>
    <title type="text">ColorButton (Python)</title>
    <link type="guide" xref="beginner.py#color-selectors"/>
    <link type="next" xref="fontchooserwidget.py"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En knapp för att starta en färgvalsdialog</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>ColorButton</title>
  <media type="image" mime="image/png" src="media/colorbutton.png"/>
  <p>Denna ColorButton startar en färgvalsdialog och skriver i terminalen RGB-värdena på färgen som väljs.</p>

  <links type="sections"/>
  
  <section id="code">
  <title>Kod som använts för att generera detta exempel</title>
  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="ColorButton", application=app)
        self.set_default_size(150, 50)
        self.set_border_width(10)

        # a colorbutton (which opens a dialogue window in
        # which we choose a color)
        self.button = Gtk.ColorButton()
        # with a default color (blue, in this instance)
        color = Gdk.RGBA()
        color.red = 0.0
        color.green = 0.0
        color.blue = 1.0
        color.alpha = 0.5
        self.button.set_rgba(color)

        # choosing a color in the dialogue window emits a signal
        self.button.connect("color-set", self.on_color_chosen)

        # a label
        label = Gtk.Label()
        label.set_text("Click to choose a color")

        # a grid to attach button and label
        grid = Gtk.Grid()
        grid.attach(self.button, 0, 0, 2, 1)
        grid.attach(label, 0, 1, 2, 1)
        self.add(grid)

    # if a new color is chosen, we print it as rgb(r,g,b) in the terminal
    def on_color_chosen(self, user_data):
        print("You chose the color: " + self.button.get_rgba().to_string())


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Användbara metoder för en ColorButton-komponent</title>
  <p><code>set_color(color)</code>, där <code>color</code> definieras som i exemplet, ställer in färgen för vår ColorButton, vilken som standard är svart. <code>get_color()</code> returnerar färgen.</p>
    <p>På rad 23 ansluts signalen <code>"color-set"</code> till återanropsfunktionen <code>on_color_chosen()</code> med <code><var>komponent</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>
  </section>
  
  <section id="references">
  <title>API-referenser</title>
  <p>I detta exempel använde vi följande:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorButton.html">GtkColorButton</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorChooser.html">GtkColorChooser</link></p></item>
    <item><p><link href="http://developer.gnome.org/gdk3/stable/gdk3-RGBA-Colors.html">RGBA-färger</link></p></item>
  </list>
  </section>
</page>
