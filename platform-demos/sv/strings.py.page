<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="strings.py" xml:lang="sv">

<info>
  <title type="text">Strängar (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="label.py"/>
  <revision version="0.1" date="2012-06-16" status="draft"/>

  <desc>En förklaring hur du hanterar strängar i Python och GTK+.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>

    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

<title>Strängar</title>

<links type="section"/>

<note style="warning"><p>GNOME rekommenderar starkt att använda Python 3 för att skriva program!</p></note>

<section id="python-2">
<title>Strängar i Python 2</title>

<p>Python 2 kommer med två olika sorters objekt som kan användas för att representera strängar, <code>str</code> och <code>unicode</code>. Instanser av <code>unicode</code> används för att uttrycka Unicode-strängar, medan instanser av typen <code>str</code> är byterepresentationer (den kodade strängen). Under huven representerar Python Unicode-strängar antingen som 16- eller 32-bitars heltal, beroende på hur Python-tolken kompilerades.</p>

<code>
&gt;&gt;&gt; unicode_string = u"Fu\u00dfb\u00e4lle"
&gt;&gt;&gt; print unicode_string
Fußbälle
</code>

<p>Unicode-strängar kan konverteras till 8-bitars strängar med <code>unicode.encode()</code>. Pythons 8-bitars strängar har en <code>str.decode()</code>-metod som tolkar strängen med given kodning (det vill säga, det är inversen av <code>unicode.encode()</code>):</p>

<code>
&gt;&gt;&gt; type(unicode_string)
&lt;type 'unicode'&gt;
&gt;&gt;&gt; unicode_string.encode("utf-8")
'Fu\xc3\x9fb\xc3\xa4lle'
&gt;&gt;&gt; utf8_string = unicode_string.encode("utf-8")
&gt;&gt;&gt; type(utf8_string)
&lt;type 'str'&gt;
&gt;&gt;&gt; unicode_string == utf8_string.decode("utf-8")
True</code>

<p>Tyvärr låter Python 2.x dig mixa <code>unicode</code> och <code>str</code> om 8-bitarssträngen råkade innehålla endast 7-bitars byte (ASCII), men skulle få <sys>UnicodeDecodeError</sys> om den innehöll värden som inte var ASCII.</p>

</section>

<section id="python-3">
<title>Strängar i Python 3</title>

<p>Sedan Python 3.0 lagras alla strängar som Unicode i en instans av typen <code>str</code>. Kodade strängar representeras å andra sidan som binärdata i form av instanser av typen ”bytes”. Konceptuellt refererar <code>str</code> till text, medan ”bytes” refererar till data. Använd <code>encode()</code> för att gå från <code>str</code> till <code>bytes</code>, och <code>decode()</code> för att gå från <code>bytes</code> till <code>str</code>.</p>

<p>Vidare är det inte längre möjligt att mixa Unicode-strängar med kodade strängar, eftersom det kommer resultera i ett :exc:`TypeError`:</p>

<code>
&gt;&gt;&gt; text = "Fu\u00dfb\u00e4lle"
&gt;&gt;&gt; data = b" sind rund"
&gt;&gt;&gt; text + data
Traceback (most recent call last):
  File "&lt;stdin&gt;", line 1, in &lt;module&gt;
TypeError: Can't convert 'bytes' object to str implicitly
&gt;&gt;&gt; text + data.decode("utf-8")
'Fußbälle sind rund'
&gt;&gt;&gt; text.encode("utf-8") + data
b'Fu\xc3\x9fb\xc3\xa4lle sind rund'</code>

</section>

<section id="gtk">
<title>Unicode i GTK+</title>

<p>GTK+ använder UTF-8-kodade strängar för all text. Detta betyder att om du anropar en metod som returnerar en sträng kommer du alltid erhålla en instans av typen <code>str</code>. Detsamma gäller metoder som förväntar sig en eller flera strängar som parameter, de måste vara UTF-8-kodade. För bekvämlighet kommer dock PyGObject automatiskt konvertera alla unicode-instanser till str om de tillhandahålls som argument:</p>

<code>
&gt;&gt;&gt; from gi.repository import Gtk
&gt;&gt;&gt; label = Gtk.Label()
&gt;&gt;&gt; unicode_string = u"Fu\u00dfb\u00e4lle"
&gt;&gt;&gt; label.set_text(unicode_string)
&gt;&gt;&gt; txt = label.get_text()
&gt;&gt;&gt; type(txt)
&lt;type 'str'&gt;</code>

<p>Vidare skulle:</p>

<code>
&gt;&gt;&gt; txt == unicode_string</code>

<p>returnera <code>False</code>, med varningen <code>__main__:1: UnicodeWarning: Unicode equal comparison failed to convert both arguments to Unicode - interpreting them as being unequal</code> (<code>Gtk.Label.get_text()</code> kommer alltid returnera en <code>str</code>-instans; därför är <code>txt</code> och <code>unicode_string</code> inte lika).</p>

<p>Detta är särskilt viktigt om du vill internationalisera ditt program med <link href="http://docs.python.org/library/gettext.html"><code>gettext</code></link>. Du måste säkerställa att <code>gettext</code> kommer returnera UTF-8-kodade 8-bitars strängar för alla språk.</p>

<p>I allmänhet rekommenderas det att inte använda <code>unicode</code>-objekt i GTK+-program överhuvudtaget och endast använda UTF-8-kodade :class:`str`-objekt då GTK+ inte är helt integrerat med <code>unicode</code>-objekt.</p>

<p>Strängkodning är mer konsekvent i Python 3.x, för PyGObject kommer automatiskt koda/avkoda till/från UTF-8 om du skickar med en sträng till en metod eller om en metod returnerar en sträng. Strängar, eller text, kommer alltid endast att representeras som instanser av <code>str</code>.</p>

</section>

<section id="references">
<title>Referenser</title>

<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/unicode.html">Hur du hanterar strängar - GTK+ 3-handledningen för Python</link></p>

</section>

</page>
