<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar.py" xml:lang="sv">
  <info>
    <title type="text">Toolbar (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="tooltip.py"/>
    <revision version="0.1" date="2012-06-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>En rad med knappar och andra komponenter</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2019</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Anders Jonsson</mal:name>
      <mal:email>anders.jonsson@norsjovallen.se</mal:email>
      <mal:years>2021</mal:years>
    </mal:credit>
  </info>

  <title>Toolbar</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>Ett exempel på ett verktygsfält med knappar (från standardikoner).</p>

  <links type="section"/>

  <section id="code">
    <title>Kod som använts för att generera detta exempel</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Toolbar Example", application=app)
        self.set_default_size(400, 200)

        # a grid to attach the toolbar
        grid = Gtk.Grid()

        # a toolbar created in the method create_toolbar (see below)
        toolbar = self.create_toolbar()
        # with extra horizontal space
        toolbar.set_hexpand(True)
        # show the toolbar
        toolbar.show()

        # attach the toolbar to the grid
        grid.attach(toolbar, 0, 0, 1, 1)

        # add the grid to the window
        self.add(grid)

        # create the actions that control the window and connect their signal to a
        # callback method (see below):

        # undo
        undo_action = Gio.SimpleAction.new("undo", None)
        undo_action.connect("activate", self.undo_callback)
        self.add_action(undo_action)

        # fullscreen
        fullscreen_action = Gio.SimpleAction.new("fullscreen", None)
        fullscreen_action.connect("activate", self.fullscreen_callback)
        self.add_action(fullscreen_action)

    # a method to create the toolbar
    def create_toolbar(self):
        # a toolbar
        toolbar = Gtk.Toolbar()

        # which is the primary toolbar of the application
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        # create a button for the "new" action, with a stock image
        new_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW)
        # label is shown
        new_button.set_is_important(True)
        # insert the button at position in the toolbar
        toolbar.insert(new_button, 0)
        # show the button
        new_button.show()
        # set the name of the action associated with the button.
        # The action controls the application (app)
        new_button.set_action_name("app.new")

        # button for the "open" action
        open_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN)
        open_button.set_is_important(True)
        toolbar.insert(open_button, 1)
        open_button.show()
        open_button.set_action_name("app.open")

        # button for the "undo" action
        undo_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO)
        undo_button.set_is_important(True)
        toolbar.insert(undo_button, 2)
        undo_button.show()
        undo_button.set_action_name("win.undo")

        # button for the "fullscreen/leave fullscreen" action
        self.fullscreen_button = Gtk.ToolButton.new_from_stock(
            Gtk.STOCK_FULLSCREEN)
        self.fullscreen_button.set_is_important(True)
        toolbar.insert(self.fullscreen_button, 3)
        self.fullscreen_button.set_action_name("win.fullscreen")

        # return the complete toolbar
        return toolbar

    # callback method for undo
    def undo_callback(self, action, parameter):
        print("You clicked \"Undo\".")

    # callback method for fullscreen / leave fullscreen
    def fullscreen_callback(self, action, parameter):
        # check if the state is the same as Gdk.WindowState.FULLSCREEN, which
        # is a bit flag
        is_fullscreen = self.get_window().get_state(
        ) &amp; Gdk.WindowState.FULLSCREEN != 0
        if not is_fullscreen:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN)
            self.fullscreen()
        else:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_FULLSCREEN)
            self.unfullscreen()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # create the actions that control the window and connect their signal to a
        # callback method (see below):

        # new
        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        app.add_action(new_action)

        # open
        open_action = Gio.SimpleAction.new("open", None)
        open_action.connect("activate", self.open_callback)
        app.add_action(open_action)

    # callback method for new
    def new_callback(self, action, parameter):
        print("You clicked \"New\".")

    # callback method for open
    def open_callback(self, action, parameter):
        print("You clicked \"Open\".")

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Användbara metoder för en Toolbar-komponent</title>
    <p>På rad 32 ansluts signalen <code>"activate"</code> från åtgärden <code>undo_action</code> till återanropsfunktionen <code>undo_callback()</code> med <code><var>åtgärd</var>.connect(<var>signal</var>, <var>återanropsfunktion</var>)</code>. Se <link xref="signals-callbacks.py"/> för en utförligare förklaring.</p>

    <list>
      <item><p>Use <code>insert(tool_item, position)</code> to insert the <code>tool_item</code> at <code>position</code>. If <code>position</code> is negative, the item is appended at the end of the toolbar.</p></item>
      <item><p><code>get_item_index(tool_item)</code> retrieves the position of <code>tool_item</code> on the toolbar.</p></item>
      <item><p><code>get_n_items()</code> returnerar antalet objekt i verktygsfältet; <code>get_nth_item(position)</code> returnerar objektet på positionen <code>position</code>.</p></item>
      <item><p>Om verktygsfältet inte har plats för alla menyobjekt, och <code>set_show_arrow(True)</code>, så kommer objekten som inte har plats att visas via en överspillsmeny.</p></item>
      <item><p><code>set_icon_size(icon_size)</code> sets the size of icons in the toolbar; <code>icon_size</code> can be one of <code>Gtk.IconSize.INVALID, Gtk.IconSize.MENU, Gtk.IconSize.SMALL_TOOLBAR, Gtk.IconSize.LARGE_TOOLBAR, Gtk.IconSize.BUTTON, Gtk.IconSize.DND, Gtk.IconSize.DIALOG</code>. This should be used only for special-purpose toolbars, normal application toolbars should respect user preferences for the size of icons. <code>unset_icon_size()</code> unsets the preferences set with <code>set_icon_size(icon_size)</code>, so that user preferences are used to determine the icon size.</p></item>
      <item><p><code>set_style(style)</code>, where <code>style</code> is one of <code>Gtk.ToolbarStyle.ICONS, Gtk.ToolbarStyle.TEXT, Gtk.ToolbarStyle.BOTH, Gtk.ToolbarStyle.BOTH_HORIZ</code>, sets if the toolbar shows only icons, only text, or both (vertically stacked or alongside each other). To let user preferences determine the toolbar style, and unset a toolbar style so set, use <code>unset_style()</code>.</p></item>
    </list>

  </section>

  <section id="reference">
    <title>API-referenser</title>
    <p>I detta exempel använde vi följande:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolButton.html">GtkToolButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolItem.html">GtkToolItem</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">Standardobjekt</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkActionable.html">GtkActionable</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gdk3/unstable/gdk3-Event-Structures.html#GdkEventWindowState">Event Structures</link></p></item>
    </list>
  </section>
</page>
