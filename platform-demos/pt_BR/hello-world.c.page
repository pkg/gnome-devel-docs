<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="hello-world" xml:lang="pt-BR">

  <info>
    <!-- The text title is used on the help.gnome.org -->
    <title type="link">Hello World (C)</title>
    <link type="guide" xref="c#examples"/>
    <revision version="0.1" date="2013-06-17" status="review"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Bastian Ilsø</name>
      <email its:translate="no">bastianilso@gnome.org</email>
    </credit>

    <desc>Creating a small "Hello, World" application using GTK+.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Hello world</title>

  <note>
    <p>For a detailed walk-through of creating a GTK+ dialog in C, see
    <link href="https://developer.gnome.org/gtk3/stable/gtk-getting-started.html">Getting Started with GTK+</link></p>
  </note>

  <p>Writing a hello world GTK+ dialog in C can be done as seen in the code sample below:</p>
  <code mime="text/x-csrc" style="numbered">
    #include &lt;gtk/gtk.h&gt;

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *label;

  window = gtk_application_window_new (app);
  label = gtk_label_new ("Hello GNOME!");
  gtk_container_add (GTK_CONTAINER (window), label);
  gtk_window_set_title (GTK_WINDOW (window), "Welcome to GNOME");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 100);
  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new (NULL, G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}

  </code>

  <p>GtkApplication initializes GTK+. It also connects the <gui>x</gui> button
  that's automatically generated along with the window to the "destroy" signal.
  We can start building our first window.
  We do this by creating a variable called <var>window</var> and assigning it
  a gtk_application_window_new. The window title can be any string you want it
  to be. To be on the safe side, it's best to stick to UTF-8 encoding.
  The code above will create a dialog window similar to what can be seen below:</p>

  <media type="image" mime="image/png" src="media/hello-world.png"/>
</page>
