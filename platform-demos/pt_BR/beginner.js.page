<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" style="task" id="beginner.js" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="js#code-samples"/>
    <revision version="0.2" date="2012-06-10" status="draft"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasettii@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Um guia para iniciantes na escrita de aplicativos do GNOME em JavaScript, incluindo amostras de códigos e exercícios práticos.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Rafael Fontenelle</mal:name>
      <mal:email>rafaelff@gnome.org</mal:email>
      <mal:years>2013, 2018</mal:years>
    </mal:credit>
  </info>

  <title>Tutoriais para iniciantes e amostras de códigos</title>
<synopsis>
  <p>JavaScript é uma das linguagens de programação mais populares da web. Não é apenas para a web, no entanto. Se você tiver uma compreensão básica do JavaScript, poderá criar aplicativos completos para o GNOME. <link href="https://wiki.gnome.org/Apps/Documents">Documentos do GNOME</link> é escrito em JavaScript, assim como <link href="https://live.gnome.org/GnomeShell/Tour">GNOME Shell</link>, a parte mais básica do GNOME.</p>
  <note style="tip"><p>O GNOME Shell é o que você vê quando clica em “Atividades” no canto superior esquerdo da tela. Também controla o relógio e o resto do painel superior. Além de mostrar como você escreve aplicativos GNOME, esses tutoriais também mostram como usar o JavaScript para escrever extensões do GNOME Shell, que fornecem novos recursos ou mudam a maneira como ele faz as coisas.</p></note>
</synopsis>

<section id="getting-started">
<title>Primeiros passos</title>
 <p>These tutorials are designed for people who already know how to write in JavaScript, and who have GNOME installed on their computers already, but who are new to developing GNOME applications. If you don't already know JavaScript, or if you need help getting GNOME set up, take a look at these resources first:</p>
<steps>
  <item><p><link href="http://eloquentjavascript.net/contents.html">Eloquent JavaScript</link> is a free, Creative Commons-licensed book, which explains the basics of JavaScript programming. Since you won't be writing JavaScript for the web, you only need to read up to chapter 10 or so.</p></item>
  <item><p><link href="http://www.gnome.org/getting-gnome/">Download GNOME</link> as part of a distribution, like Fedora, openSUSE, or Ubuntu. Each distribution has its own instructions for how to get GNOME.</p></item>
  <item><p><link xref="set-up-gedit.js">Set up gedit</link> for writing applications. GNOME's text editor, gedit, is sometimes just called "text editor".</p></item>
</steps>
</section>


<section id="tutorials">
<title>Tutoriais</title>
</section>

<section id="samples">
<title>Code samples</title>
  <p>These samples show how to use widgets in your GNOME applications. Each one demonstrates a complete application which showcases the featured widget. At the end of each sample, you will find links to more detailed reference material.</p>
  <p>To run the code samples:</p>
  <steps>
    <item><p>Copy and paste the code into <var>filename</var>.js</p></item>
    <item><p>In the terminal, type:</p>
          <screen>gjs <var>filename</var>.js</screen></item>
  </steps>

  <section id="windows" style="2column"><title>Windows</title>
  </section>
  <section id="display-widgets" style="2column"><title>Display widgets</title>
  </section>
  <section id="buttons" style="2column"><title>Buttons and toggles</title>
  </section>
  <section id="entry" style="2column"><title>Numeric and text data entry</title>
  </section>
  <section id="multiline" style="2column"><title>Multiline text editor</title>
  </section>
  <section id="menu-combo-toolbar" style="2column"><title>Menu, combo box and toolbar widgets</title>
  </section>
  <section id="treeview" style="2column"><title>TreeView widget</title>
  </section>
  <section id="selectors"><title>Selectors</title>
    <section id="file-selectors"><title>File selectors</title>
    </section>
    <section id="font-selectors"><title>Font selectors</title>
    </section>
    <section id="color-selectors"><title>Color Selectors</title>
    </section>
  </section>
  <section id="layout" style="2column"><title>Layout containers</title>
  </section>
  <section id="ornaments" style="2column"><title>Ornaments</title>
  </section>
  <section id="scrolling" style="2column"><title>Scrolling</title>
  </section>
  <section id="misc" style="2column"><title>Miscellaneous</title>
  </section>
</section>

<section id="exercises">
<title>Exercises</title>
</section>
</page>
