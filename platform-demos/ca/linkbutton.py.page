<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="linkbutton.py" xml:lang="ca">
  <info>
    <title type="text">LinkButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="checkbutton.py"/>
    <revision version="0.1" date="2012-05-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A button bound to an URL</desc>
  </info>

  <title>LinkButton</title>

  <media type="image" mime="image/png" src="media/linkbutton.png"/>
  <p>A button that links to a web page.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME LinkButton", application=app)
        self.set_default_size(250, 50)

        # a linkbutton pointing to the given URI
        button = Gtk.LinkButton(uri="http://live.gnome.org")
        # with given text
        button.set_label("Link to GNOME live!")

        # add the button to the window
        self.add(button)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  <section id="methods">
    <title>Useful methods for a LinkButton widget</title>
    <list>
      <item><p><code>get_visited()</code> returns the 'visited' state (<code>True</code> or <code>False</code>) of the URI where the LinkButton points. The button becomes visited when it is clicked.</p></item>
      <item><p><code>set_visited(True)</code> sets the 'visited' state of the URI where the LinkButton points as <code>True</code> (analogously for <code>False</code>).</p></item>
      <item><p>Each time the button is clicked, the signal <code>"activate-link"</code> is emitted. For an explanation of signals and callback functions, see <link xref="signals-callbacks.py"/>.</p></item>
    </list>
  </section>
  <section id="references">
    <title>API References</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLinkButton.html">GtkLinkButton</link></p></item>
    </list>
  </section>
</page>
