<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="togglebutton.py" xml:lang="ca">
  <info>
    <title type="text">ToggleButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="switch.py"/>
    <revision version="0.1" date="2012-05-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A button which retains state</desc>
  </info>

  <title>ToggleButton</title>
  <media type="image" mime="image/png" src="media/togglebutton.png"/>
  <p>When this ToggleButton is in an active state, the spinner spins.</p>

  <links type="section"/>

  <section id="code">
    <title>Code used to generate this example</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # a window

    def __init__(self, app):
        Gtk.Window.__init__(
            self, title="ToggleButton Example", application=app)
        self.set_default_size(300, 300)
        self.set_border_width(30)

        # a spinner animation
        self.spinner = Gtk.Spinner()
        # with extra horizontal space
        self.spinner.set_hexpand(True)
        # with extra vertical space
        self.spinner.set_vexpand(True)

        # a togglebutton
        button = Gtk.ToggleButton.new_with_label("Start/Stop")
        # connect the signal "toggled" emitted by the togglebutton
        # when its state is changed to the callback function toggled_cb
        button.connect("toggled", self.toggled_cb)

        # a grid to allocate the widgets
        grid = Gtk.Grid()
        grid.set_row_homogeneous(False)
        grid.set_row_spacing(15)
        grid.attach(self.spinner, 0, 0, 1, 1)
        grid.attach(button, 0, 1, 1, 1)

        # add the grid to the window
        self.add(grid)

    # callback function for the signal "toggled"
    def toggled_cb(self, button):
        # if the togglebutton is active, start the spinner
        if button.get_active():
            self.spinner.start()
        # else, stop it
        else:
            self.spinner.stop()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
    <title>Useful methods for a ToggleButton widget</title>
    <p>In line 22 the signal <code>"toggled"</code> is connected to the callback function <code>toggled_cb()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
  </section>

  <section id="references">
    <title>API References</title>
    <p>In this sample we used the following:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToggleButton.html">GtkToggleButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSpinner.html">GtkSpinner</link></p></item>
    </list>
  </section>
</page>
