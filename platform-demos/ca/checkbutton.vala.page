<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="checkbutton.vala" xml:lang="ca">
  <info>
  <title type="text">CheckButton (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <revision version="0.1" date="2012-05-09" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Create widgets with a discrete toggle button</desc>
  </info>

  <title>CheckButton</title>
  <media type="image" mime="image/png" src="media/checkbutton.png"/>
  <p>This CheckButton toggles the title.</p>

<code mime="text/x-csharp" style="numbered">/* A window in the application */
class MyWindow : Gtk.ApplicationWindow {

	/* The constructor */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "CheckButton Example");

		this.set_default_size (300, 100);
		this.border_width = 10;

		var checkbutton = new Gtk.CheckButton.with_label ("Show Title");

		/* Connect the checkbutton to the
		 * callback function (aka. signal handler).
		 */
		checkbutton.toggled.connect (this.toggled_cb);

		/* Add the button to the this window */
		this.add (checkbutton);

		checkbutton.set_active (true);
		checkbutton.show ();
	}

	/* The signal handler for the 'toggled' signal of the checkbutton. */
	void toggled_cb (Gtk.ToggleButton checkbutton) {
		if (checkbutton.get_active())
			this.set_title ("CheckButton Example");
		else
			this.set_title ("");
	}
}

/* This is the application */
class MyApplication : Gtk.Application {

	/* The constructor */
	internal MyApplication () {
		Object (application_id: "org.example.checkbutton");
	}

	/* Override the activate signal of GLib.Application */
	protected override void activate () {
		new MyWindow (this).show ();
	}

}

/* main creates and runs the application */
int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>
  In this sample we used the following:
</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.CheckButton.html">Gtk.CheckButton</link></p></item>
</list>
</page>
