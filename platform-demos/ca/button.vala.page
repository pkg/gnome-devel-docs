<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="button.vala" xml:lang="ca">
  <info>
  <title type="text">Button (Vala)</title>
    <link type="guide" xref="beginner.vala#buttons"/>
    <link type="seealso" xref="GtkApplicationWindow.vala"/>
    <revision version="0.1" date="2012-02-21" status="stub"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A button widget which emits a signal when clicked</desc>
  </info>

  <title>Button widget</title>

  <media type="image" mime="image/png" src="media/button.png"/>
  <p>A button widget connected to a simple callback function.</p>

<code mime="text/x-csharp" style="numbered">
/* A window in the application */
public class MyWindow : Gtk.ApplicationWindow {

	/* The constructor of the window */
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "GNOME Button");

		var button = new Gtk.Button.with_label ("Click Me");
		button.clicked.connect (this.reverse_label);
		button.show ();

		this.window_position = Gtk.WindowPosition.CENTER;
		this.set_default_size (250,50);
		this.add (button);
	}

	/* The callback function connected to the
	 * 'clicked' signal of the button.
	 */
	void reverse_label (Gtk.Button button) {
		button.label = button.label.reverse ();
	}
}

/* This is the application. */
public class MyApplication : Gtk.Application {

	/* This is the constructor */
	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}

	/* Override the activate signal of GLib.Application */
	protected override void activate () {
		new MyWindow (this).show ();
	}
}

/* main creates and runs the application */
public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>
  In this sample we used the following:
  <link href="http://www.valadoc.org/gtk+-3.0/Gtk.Button.html">Gtk.Button</link>
</p>
</page>
