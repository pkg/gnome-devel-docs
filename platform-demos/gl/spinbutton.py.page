<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="spinbutton.py" xml:lang="gl">
  <info>
    <title type="text">SpinButton (Python)</title>
    <link type="guide" xref="beginner.py#entry"/>
    <link type="seealso" xref="signals-callbacks.py"/>
    <link type="next" xref="entry.py"/>    
    <revision version="0.2" date="2012-06-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Obtendo un enteiro ou número en punto flotante desde o usuario.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Fran Dieguez</mal:name>
      <mal:email>frandieguez@gnome.org</mal:email>
      <mal:years>2012-2013.</mal:years>
    </mal:credit>
  </info>

  <title>SpinButton</title>
  <media type="image" mime="image/png" src="media/spinbutton.png"/>
  <p>Seleccione un número, escribíndoo ou premendo nos botóns -/+!</p>

  <links type="section"/>

  <section id="code">
    <title>Código usado para xerar este exemplo</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="SpinButton Example", application=app)
        self.set_default_size(210, 70)
        self.set_border_width(5)

        # an adjustment (initial value, min value, max value,
        # step increment - press cursor keys or +/- buttons to see!,
        # page increment - not used here,
        # page size - not used here)
        ad = Gtk.Adjustment(0, 0, 100, 1, 0, 0)

        # a spin button for integers (digits=0)
        self.spin = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
        # as wide as possible
        self.spin.set_hexpand(True)

        # we connect the signal "value-changed" emitted by the spinbutton with the callback
        # function spin_selected
        self.spin.connect("value-changed", self.spin_selected)

        # a label
        self.label = Gtk.Label()
        self.label.set_text("Choose a number")

        # a grid to attach the widgets
        grid = Gtk.Grid()
        grid.attach(self.spin, 0, 0, 1, 1)
        grid.attach(self.label, 0, 1, 2, 1)

        self.add(grid)

    # callback function: the signal of the spinbutton is used to change the
    # text of the label
    def spin_selected(self, event):
        self.label.set_text(
            "The number you selected is " + str(self.spin.get_value_as_int()) + ".")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Métodos útiles para o widget SpinButton</title>
    <p>A Gtk.Adjustment is needed to construct the Gtk.SpinButton. This is the representation of a value with a lower and upper bound, together with step and page increments, and a page size, and it is constructed as <code>Gtk.Adjustment(value, lower, upper, step_increment, page_increment, page_size)</code> where the fields are of type <code>float</code>; <code>step_increment</code> is the increment/decrement that is obtained by using the cursor keys or the buttons of the spinbutton. Note that <code>page_increment</code> and <code>page_size</code> are not used in this case, and they should be set to <code>0</code>.</p>
    <p>In line 23 the signal <code>"value-changed"</code> is connected to the callback function <code>spin_selected()</code> using <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. See <link xref="signals-callbacks.py"/> for a more detailed explanation.</p>
    <list>
      <item><p>If you want the value of the spinbutton to wrap around when they exceed the maximum or the minimum, set <code>set_wrap(True)</code>. The <code>"wrapped"</code> signal is emitted when this happens.</p></item>
      <item><p><code>set_digits(digits)</code> sets the precision to be displayed by the spinbutton, up to 20 digits.</p></item>
      <item><p>To get the value of the spinbutton as an integer, use <code>get_value_as_int()</code>.</p></item>
    </list>
  </section>

  <section id="references">
    <title>API References</title>
    <p>Neste exemplo empregaremos o seguinte:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSpinButton.html">GtkSpinButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkAdjustment.html">GtkAdjustment</link></p></item>
    </list>
  </section>
</page>
