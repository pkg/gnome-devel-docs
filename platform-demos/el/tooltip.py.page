<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="tooltip.py" xml:lang="el">
  <info>
  <title type="text">Συμβουλή οθόνης (Python)</title>
    <link type="guide" xref="beginner.py#misc"/>
    <link type="seealso" xref="toolbar.py"/>
    <link type="next" xref="toolbar_builder.py"/>
    <revision version="0.1" date="2012-08-20" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Προσθέστε συμβουλές στα γραφικά στοιχεία σας</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Συμβουλή οθόνης</title>
  <media type="image" mime="image/png" src="media/tooltip.png"/>
  <p>Μια εργαλειοθήκη με μια συμβουλή οθόνης (με μια εικόνα) για ένα κουμπί.</p>
  <note><p>Αυτό το παράδειγμα δομεί στο παράδειγμα <link xref="toolbar.py">εργαλειοθήκη</link>.</p></note>

  <links type="section"/>
    
  <section id="code">
  <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(
            self, title="Toolbar with Tooltips Example", application=app)
        self.set_default_size(400, 200)

        grid = Gtk.Grid()

        toolbar = self.create_toolbar()
        toolbar.set_hexpand(True)
        toolbar.show()

        grid.attach(toolbar, 0, 0, 1, 1)

        self.add(grid)

        undo_action = Gio.SimpleAction.new("undo", None)
        undo_action.connect("activate", self.undo_callback)
        self.add_action(undo_action)

        fullscreen_action = Gio.SimpleAction.new("fullscreen", None)
        fullscreen_action.connect("activate", self.fullscreen_callback)
        self.add_action(fullscreen_action)

    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        # button for the "new" action
        new_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW)
        # with a tooltip with a given text
        new_button.set_tooltip_text("Create a new file")
        new_button.set_is_important(True)
        toolbar.insert(new_button, 0)
        new_button.show()
        new_button.set_action_name("app.new")

        # button for the "open" action
        open_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN)
        # with a tooltip with a given text in the Pango markup language
        open_button.set_tooltip_markup("Open an &lt;i&gt;existing&lt;/i&gt; file")
        open_button.set_is_important(True)
        toolbar.insert(open_button, 1)
        open_button.show()
        open_button.set_action_name("app.open")

        # button for the "undo" action
        undo_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO)
        # with a tooltip with an image
        # set True the property "has-tooltip"
        undo_button.set_property("has-tooltip", True)
        # connect to the callback function that for the tooltip
        # with the signal "query-tooltip"
        undo_button.connect("query-tooltip", self.undo_tooltip_callback)
        undo_button.set_is_important(True)
        toolbar.insert(undo_button, 2)
        undo_button.show()
        undo_button.set_action_name("win.undo")

        # button for the "fullscreen/leave fullscreen" action
        self.fullscreen_button = Gtk.ToolButton.new_from_stock(
            Gtk.STOCK_FULLSCREEN)
        self.fullscreen_button.set_is_important(True)
        toolbar.insert(self.fullscreen_button, 3)
        self.fullscreen_button.set_action_name("win.fullscreen")

        return toolbar

    # the callback function for the tooltip of the "undo" button
    def undo_tooltip_callback(self, widget, x, y, keyboard_mode, tooltip):
        # set the text for the tooltip
        tooltip.set_text("Undo your last action")
        # set an icon for the tooltip
        tooltip.set_icon_from_stock("gtk-undo", Gtk.IconSize.MENU)
        # show the tooltip
        return True

    def undo_callback(self, action, parameter):
        print("You clicked \"Undo\".")

    def fullscreen_callback(self, action, parameter):
        is_fullscreen = self.get_window().get_state(
        ) &amp; Gdk.WindowState.FULLSCREEN != 0
        if not is_fullscreen:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN)
            self.fullscreen()
        else:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_FULLSCREEN)
            self.unfullscreen()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        app.add_action(new_action)

        open_action = Gio.SimpleAction.new("open", None)
        open_action.connect("activate", self.open_callback)
        app.add_action(open_action)

    def new_callback(self, action, parameter):
        print("You clicked \"New\".")

    def open_callback(self, action, parameter):
        print("You clicked \"Open\".")

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Χρήσιμες μέθοδοι για ένα γραφικό στοιχείο συμβουλής οθόνης</title>

    <p><code>set_tooltip_text(text)</code> και <code>set_tooltip_markup(text)</code> μπορούν να χρησιμοποιηθούν για να προσθέσουν μια συμβουλή οθόνης απλού κειμένου (ή κειμένου στην γλώσσα επισήμανσης Pango) σε ένα γραφικό στοιχείο.</p>
    <p>Για πιο σύνθετες συμβουλές οθόνης, για παράδειγμα για μια συμβουλή οθόνης με μια εικόνα:</p>
    <steps>
      <item><p>Ορίστε την ιδιότητα <code>"has-tooltip"</code> του γραφικού στοιχείου σε <code>True</code>· αυτό θα κάνει το GTK+ να παρακολουθήσει το γραφικό στοιχείο για κίνηση και σχετικά συμβάντα που απαιτούνται για προσδιορισμό πότε και πού θα εμφανιστεί μια συμβουλή οθόνης.</p></item>
      <item><p>Συνδεθείτε με το σήμα <code>"query-tooltip"</code>. Αυτό το σήμα εκπέμπεται όταν η συμβουλή οθόνης υποτίθεται ότι εμφανίζεται. Ένα από τα ορίσματα που πέρασαν στον χειριστή σήματος είναι ένα αντικείμενο GtkTooltip. Αυτό είναι το αντικείμενο που πρόκειται να εμφανίσουμε ως συμβουλή οθόνης και μπορεί να επεξεργαστεί στην επανάκλησή σας χρησιμοποιώντας συναρτήσεις όπως <code>set_icon()</code>. Υπάρχουν συναρτήσεις για ρύθμιση της επισήμανσης της συμβουλής οθόνης (<code>set_markup(text)</code>), ρύθμιση μιας εικόνας από ένα εικονίδιο παρακαταθήκης (<code>set_icon_from_stock(stock_id, size)</code>), ή ακόμα τοποθέτηση σε ένα προσαρμοσμένο γραφικό στοιχείο (<code>set_custom(widget)</code>).</p></item>
      <item><p>Επιστροφή <code>True</code> από τον χειριστή σας ερωτήματος συμβουλής οθόνης. Αυτό προκαλεί την εμφάνιση της συμβουλής οθόνης. Εάν επιστρέψετε <code>False</code>, δεν θα εμφανιστεί.</p></item>
    </steps>

    <p>Στην προφανώς σπάνια περίπτωση όπου θέλετε να έχετε ακόμα περισσότερο έλεγχο στην συμβουλή οθόνης που πρόκειται να εμφανιστεί, μπορείτε να ορίσετε το δικό σας GtkWindow που θα χρησιμοποιηθεί ως παράθυρο συμβουλής οθόνης. Αυτό δουλεύει ως εξής:</p>
    <steps>
      <item><p>Ορίστε <code>"has-tooltip"</code> και συνδεθείτε στο <code>"query-tooltip"</code> όπως πριν.</p></item>
      <item><p>Χρησιμοποιήστε <code>set_tooltip_window()</code> στο γραφικό στοιχείο για να ορίσετε ένα GtkWindow που δημιουργήθηκε από εσάς ως παράθυρο συμβουλής οθόνης.</p></item>
      <item><p>Στην επανάκληση <code>"query-tooltip"</code> μπορείτε να προσπελάσετε το παράθυρο σας χρησιμοποιώντας <code>get_tooltip_window()</code> και να το χειριστείτε όπως επιθυμείτε. Η σημασιολογία της τιμής επιστροφής είναι ακριβώς όπως πριν, επιστροφή <code>True</code> για να εμφανίσετε το παράθυρο, <code>False</code> για να μην το εμφανίσετε.</p></item>
    </steps>

  </section>
  
  <section id="references">
  <title>Αναφορές API</title>
    <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkTooltip.html">GtkTooltip</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkToolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Stock-Items.html">Stock Items</link></p></item>
    </list>
  </section>
</page>
