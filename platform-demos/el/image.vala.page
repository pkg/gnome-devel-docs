<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="image.vala" xml:lang="el">
  <info>
  <title type="text">Εικόνα (Vala)</title>
    <link type="guide" xref="beginner.vala#display-widgets"/>
    <revision version="0.1" date="2012-05-03" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένα γραφικό στοιχείο που εμφανίζει μια εικόνα</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Εικόνα</title>
  <media type="image" mime="image/png" src="media/image.png"/>
  <p>Αυτή η GtkApplication εμφανίζει ένα αρχείο εικόνας από τον τρέχοντα κατάλογο.</p>
  <note><p>Εάν το αρχείο εικόνας δεν φορτωθεί επιτυχώς, η εικόνα θα περιέχει ένα εικονίδιο "σπασμένης εικόνας". Το <file>filename.png</file> χρειάζεται να είναι στον τρέχοντα κατάλογο για να δουλέψει αυτός ο κώδικας. Χρησιμοποιήστε την αγαπημένη σας εικόνα!</p></note>
<code mime="text/x-csharp" style="numbered">public class MyWindow : Gtk.ApplicationWindow {
	internal MyWindow (MyApplication app) {
		Object (application: app, title: "Welcome to GNOME");

		var image = new Gtk.Image ();
		image.set_from_file ("gnome-image.png");
		this.add (image);
		this.set_default_size (300, 300);
	}
}

public class MyApplication : Gtk.Application {
	protected override void activate () {
		new MyWindow (this).show_all ();
	}

	internal MyApplication () {
		Object (application_id: "org.example.MyApplication");
	}
}

public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
<p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">GtkApplication</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ApplicationWindow.html">GtkApplicationWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Image.html">GtkImage</link></p></item>
</list>
</page>
