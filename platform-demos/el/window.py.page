<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="window.py" xml:lang="el">
  <info>
    <title type="text">Παράθυρο (Python)</title>
    <link type="guide" xref="beginner.py#windows"/>
    <link type="seealso" xref="properties.py"/>
    <link type="next" xref="GtkApplicationWindow.py"/>
    <revision version="0.2" date="2012-06-09" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="author">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένα παράθυρο ανώτατου επιπέδου που μπορεί να περιέχει άλλα γραφικά στοιχεία</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Παράθυρο</title>

  <table>
    <tr>
      <td>
        <media type="image" mime="image/png" src="media/window.png"/>
        <p>Μια ελάχιστη εφαρμογή GTK+: ένα παράθυρο με τίτλο.</p>
      </td>
      <td>
        <p>Χρησιμοποιήστε <link xref="GtkApplicationWindow.py"/>, εάν χρειάζεστε υποστήριξη <link xref="gmenu.py"/>.</p>
      </td>
    </tr>
  </table>

  <links type="section"/>

  <section id="code">
  <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyApplication(Gtk.Application):

    def do_activate(self):
        # δημιουργία ενός παραθύρου Gtk το οποίο ανήκει στην εφαρμογή
        window = Gtk.Window(application=self)
        # ορισμός τίτλου
        window.set_title("Welcome to GNOME")
        # εμφάνιση του παραθύρου
        window.show_all()

# δημιουργία και εκτέλεση της εφαρμογής, έξοδος με την επιστρεφόμενη τιμή
# από το εκτελούμενο πρόγραμμα
app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
  <title>Χρήσιμες μέθοδοι για ένα γραφικό στοιχείο παραθύρου</title>
  <list>
    <item><p><code>set_default_size(200, 100)</code> ορίζει το προεπιλεγμένο μέγεθος του παραθύρου σε πλάτος <code>200</code> και ύψος <code>100</code>· εάν αντί για ένα θετικό αριθμό περνάμε το <code>-1</code> έχουμε το προεπιλεγμένο μέγεθος.</p></item>
    <item><p><code>set_position(Gtk.WindowPosition.CENTER)</code> κεντράρει το παράθυρο. Άλλες επιλογές είναι <code>Gtk.WindowPosition.NONE, Gtk.WindowPosition.MOUSE, Gtk.WindowPosition.CENTER_ALWAYS, Gtk.WindowPosition.CENTER_ON_PARENT</code>.</p></item>
  </list>
  </section>

  <section id="references">
  <title>Αναφορές API</title>

  <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkApplication.html">GtkApplication</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
  </list>
  </section>
</page>
