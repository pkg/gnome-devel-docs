<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="colorbutton.py" xml:lang="el">
  <info>
    <title type="text">ColorButton (Python)</title>
    <link type="guide" xref="beginner.py#color-selectors"/>
    <link type="next" xref="fontchooserwidget.py"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Ένα κουμπί για εκκίνηση του διαλόγου επιλογής χρώματος</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ελληνική μεταφραστική ομάδα GNOME</mal:name>
      <mal:email>team@gnome.gr</mal:email>
      <mal:years>2012-2015</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Δημήτρης Σπίγγος</mal:name>
      <mal:email>dmtrs32@gmail.com</mal:email>
      <mal:years>2012, 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Μαρία Θουκιδίδου</mal:name>
      <mal:email>marablack3@gmail.com</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Θάνος Τρυφωνίδης</mal:name>
      <mal:email>tomtryf@gmail.com</mal:email>
      <mal:years>2014, 2015</mal:years>
    </mal:credit>
  </info>

  <title>Κουμπί χρώματος (ColorButton)</title>
  <media type="image" mime="image/png" src="media/colorbutton.png"/>
  <p>Αυτό το ColorButton ξεκινά τον διάλογο επιλογής χρωμάτων και τυπώνει στον τερματικό τις τιμές RGB του επιλεγμένου χρώματος.</p>

  <links type="sections"/>
  
  <section id="code">
  <title>Ο χρησιμοποιούμενος κώδικας για παραγωγή αυτού παραδείγματος</title>
  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="ColorButton", application=app)
        self.set_default_size(150, 50)
        self.set_border_width(10)

        # a colorbutton (which opens a dialogue window in
        # which we choose a color)
        self.button = Gtk.ColorButton()
        # with a default color (blue, in this instance)
        color = Gdk.RGBA()
        color.red = 0.0
        color.green = 0.0
        color.blue = 1.0
        color.alpha = 0.5
        self.button.set_rgba(color)

        # choosing a color in the dialogue window emits a signal
        self.button.connect("color-set", self.on_color_chosen)

        # a label
        label = Gtk.Label()
        label.set_text("Click to choose a color")

        # a grid to attach button and label
        grid = Gtk.Grid()
        grid.attach(self.button, 0, 0, 2, 1)
        grid.attach(label, 0, 1, 2, 1)
        self.add(grid)

    # if a new color is chosen, we print it as rgb(r,g,b) in the terminal
    def on_color_chosen(self, user_data):
        print("You chose the color: " + self.button.get_rgba().to_string())


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Χρήσιμες μέθοδοι για ένα γραφικό στοιχείο ColorButton</title>
  <p><code>set_color(color)</code>, όπου το <code>color</code> ορίζεται όπως στο παράδειγμα, ορίζει το χρώμα του ColorButton, που από προεπιλογή είναι μαύρο. <code>get_color()</code> επιστρέφει το χρώμα.</p>
    <p>Στη γραμμή 23 το σήμα <code>"color-set"</code> συνδέεται με τη συνάρτηση επανάκλησης <code>on_color_chosen()</code> χρησιμοποιώντας <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code>. Δείτε <link xref="signals-callbacks.py"/> για μια πιο λεπτομερή εξήγηση.</p>
  </section>
  
  <section id="references">
  <title>Αναφορές API</title>
  <p>Σε αυτό το παράδειγμα χρησιμοποιήσαμε τα παρακάτω:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorButton.html">GtkColorButton</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorChooser.html">GtkColorChooser</link></p></item>
    <item><p><link href="http://developer.gnome.org/gdk3/stable/gdk3-RGBA-Colors.html">RGBA Colors</link></p></item>
  </list>
  </section>
</page>
