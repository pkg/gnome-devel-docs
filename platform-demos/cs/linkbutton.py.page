<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="linkbutton.py" xml:lang="cs">
  <info>
    <title type="text">LinkButton (Python)</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="checkbutton.py"/>
    <revision version="0.1" date="2012-05-23" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Tlačítko svázané s URL</desc>
  </info>

  <title>LinkButton</title>

  <media type="image" mime="image/png" src="media/linkbutton.png"/>
  <p>Tlačítko, které odkazuje na webovou stránku.</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # Okno

    def __init__(self, app):
        Gtk.Window.__init__(self, title="GNOME LinkButton", application=app)
        self.set_default_size(250, 50)

        # LinkButton odkazující na zadanou adresu URI
        button = Gtk.LinkButton(uri="http://live.gnome.org")
        # se zadaným textem
        button.set_label("Link to GNOME live!")

        # Přidá tlačítko do okna
        self.add(button)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  <section id="methods">
    <title>Užitečné metody pro widget LinkButton</title>
    <list>
      <item><p><code>get_visited()</code> vrací stav vlastnosti <code>visited</code> (navštíveno) jako <code>True</code> nebo <code>False</code> pro adresu URI, na kterou <code>LinkButton</code> ukazuje. Tlačítko se stane „navštíveným“, když se na něj klikne.</p></item>
      <item><p><code>set_visited(True)</code> nastaví stav vlastnosti <code>visited</code> (navštíveno) pro adresu URI, na kterou <code>LinkButton</code> ukazuje na <code>True</code> (obdobně pro <code>False</code>).</p></item>
      <item><p>Pokaždé, když je kliknuto na tlačítko, je vyslán signál <code>"activate-link"</code>. Vysvětlení signálů a funkcí zpětného volání viz <link xref="signals-callbacks.py"/>.</p></item>
    </list>
  </section>
  <section id="references">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWindow.html">GtkWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLinkButton.html">GtkLinkButton</link></p></item>
    </list>
  </section>
</page>
