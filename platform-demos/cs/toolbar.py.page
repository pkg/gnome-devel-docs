<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="toolbar.py" xml:lang="cs">
  <info>
    <title type="text">Toolbar (Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="seealso" xref="grid.py"/>
    <link type="next" xref="tooltip.py"/>
    <revision version="0.1" date="2012-06-05" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Lišta s tlačítky a dalšími widgety</desc>
  </info>

  <title>Toolbar</title>

  <media type="image" mime="image/png" src="media/toolbar.png"/>
  <p>Příklad lišty s tlačítky (se standardními ikonami).</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>

    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Toolbar Example", application=app)
        self.set_default_size(400, 200)

        # Mřížka pro připojení nástrojové lišty
        grid = Gtk.Grid()

        # Nástrojová lišta vytvořená v metodě create_toolbar (viz dále)
        toolbar = self.create_toolbar()
        # S dodatečným vodorovným místem
        toolbar.set_hexpand(True)
        # Zobrazí nástrojovou lištu
        toolbar.show()

        # Připojí nástrojovou lištu do mřížky
        grid.attach(toolbar, 0, 0, 1, 1)

        # Přidá mřížku do okna
        self.add(grid)

        # Vytvoří akce, které ovládají okno a napojí jejich signály na
        # metody zpětného volání (viz dále):

        # Akce "undo"
        undo_action = Gio.SimpleAction.new("undo", None)
        undo_action.connect("activate", self.undo_callback)
        self.add_action(undo_action)

        # Akce "fullscreen"
        fullscreen_action = Gio.SimpleAction.new("fullscreen", None)
        fullscreen_action.connect("activate", self.fullscreen_callback)
        self.add_action(fullscreen_action)

    # Metoda, která vytvoří nástrojovou lištu
    def create_toolbar(self):
        # Nástrojová lišta
        toolbar = Gtk.Toolbar()

        # Jedná se o hlavní nástrojovou lištu aplikace
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        # Vytvoří tlačítko pro akci "new", se standardní ikonou
        new_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW)
        # Popisek se zobrazuje
        new_button.set_is_important(True)
        # Vloží tlačítko na pozici na nástrojové liště
        toolbar.insert(new_button, 0)
        # Zobrazí tlačítko
        new_button.show()
        # Nastaví název akce přidružené k tlačítku
        # Tato akce řídí aplikaci (app)
        new_button.set_action_name("app.new")

        # Tlačítko pro akci "open"
        open_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN)
        open_button.set_is_important(True)
        toolbar.insert(open_button, 1)
        open_button.show()
        open_button.set_action_name("app.open")

        # Tlačítko pro akci "undo"
        undo_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO)
        undo_button.set_is_important(True)
        toolbar.insert(undo_button, 2)
        undo_button.show()
        undo_button.set_action_name("win.undo")

        # Tlačítko pro akci "fullscreen/leave fullscreen"
        self.fullscreen_button = Gtk.ToolButton.new_from_stock(
            Gtk.STOCK_FULLSCREEN)
        self.fullscreen_button.set_is_important(True)
        toolbar.insert(self.fullscreen_button, 3)
        self.fullscreen_button.set_action_name("win.fullscreen")

        # Vrátí kompletní nástrojovou lištu
        return toolbar

    # Metoda zpětného volání pro "undo"
    def undo_callback(self, action, parameter):
        print("You clicked \"Undo\".")

    # Metoda zpětného volání pro "fullscreen/leave fullscreen"
    def fullscreen_callback(self, action, parameter):
        # Zkontroluje, jestli je stav stejný jako Gdk.WindowState.FULLSCREEN,
        # což je bitový příznak
        is_fullscreen = self.get_window().get_state(
        ) &amp; Gdk.WindowState.FULLSCREEN != 0
        if not is_fullscreen:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN)
            self.fullscreen()
        else:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_FULLSCREEN)
            self.unfullscreen()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        # Vytvoří akce pro ovládání aplikace a napojí jejich signály na
        # metody zpětného volání (viz dále):

        # Akce "new"
        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        app.add_action(new_action)

        # Akce "open"
        open_action = Gio.SimpleAction.new("open", None)
        open_action.connect("activate", self.open_callback)
        app.add_action(open_action)

    # Metoda zpětného volání pro "new"
    def new_callback(self, action, parameter):
        print("You clicked \"New\".")

    # Metoda zpětného volání pro "open"
    def open_callback(self, action, parameter):
        print("You clicked \"Open\".")

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Užitečné metody pro widget Toolbar</title>
    <p>Na řádku 32 je signál <code>"activate"</code> od činnosti <code>undo_action</code> napojen na funkci zpětného volání <code>undo_callback()</code> pomocí <code><var>action</var>.connect(<var>signál</var>, <var>funkce zpětného volání</var>)</code>. Podrobnější vysvětlení viz <link xref="signals-callbacks.py"/>.</p>

    <list>
      <item><p>Použijte <code>insert(nástrojová_položka, pozice)</code> k vložení <code>nástrojové_položky</code> na <code>pozici</code>. Pokud je <code>pozice</code> záporná, bude položka vložena na konec nástrojové lišty.</p></item>
      <item><p><code>get_item_index(tool_item)</code> vrací pozici <code>tool_item</code> na nástrojové liště.</p></item>
      <item><p><code>get_n_items()</code> vrací počet položek na nástrojové liště. <code>get_nth_item(pozice)</code> vrací položku na pozici <code>pozice</code>.</p></item>
      <item><p>Pokud nástrojová lišta nemá dostatek místa pro všechny položky a je použito <code>set_show_arrow(True)</code>, položky které jdou mimo se zobrazí v nabídce přetékajících položek.</p></item>
      <item><p><code>set_icon_size(velikost_ikony)</code> nastavuje velikost ikony na nástrojové liště. Parametr <code>velikost_ikony</code> může být něco z <code>Gtk.IconSize.INVALID</code>, <code>Gtk.IconSize.MENU</code>, <code>Gtk.IconSize.SMALL_TOOLBAR</code>, <code>Gtk.IconSize.LARGE_TOOLBAR</code>, <code>Gtk.IconSize.BUTTON</code>, <code>Gtk.IconSize.DND</code> nebo <code>Gtk.IconSize.DIALOG</code>. Používat by se to mělo jen pro speciální účely, normálně by nástrojová lišta aplikace měla respektovat uživatelské předvolby pro velikost ikon. <code>unset_icon_size()</code> zruší volbu nastavenou pomocí <code>set_icon_size(velikost_ikony)</code>, takže se k určení velikosti ikon použije uživatelská předvolba.</p></item>
      <item><p><code>set_style(styl)</code>, kde <code>styl</code> je jedno z <code>Gtk.ToolbarStyle.ICONS</code>, <code>Gtk.ToolbarStyle.TEXT</code>, <code>Gtk.ToolbarStyle.BOTH</code> nebo <code>Gtk.ToolbarStyle.BOTH_HORIZ</code>, nastavuje, jestli nástrojová lišta zobrazuje jen ikony, jen text nebo obojí (svisle na sobě nebo vedle sebe). Abyste umožňili uživateli předvolby určující styl nástrojové lišty a zrušit již nastavený styl, použijte <code>unset_style()</code>.</p></item>
    </list>

  </section>

  <section id="reference">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolButton.html">GtkToolButton</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkToolItem.html">GtkToolItem</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">Standardní položky</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkActionable.html">GtkActionable</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gdk3/unstable/gdk3-Event-Structures.html#GdkEventWindowState">Struktury událostí</link></p></item>
    </list>
  </section>
</page>
