<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="widget_drawing.py" xml:lang="cs">
  <info>
    <title type="text">Widget (Python)</title>
    <link type="guide" xref="beginner.py#misc"/>
    <revision version="0.1" date="2013-02-24" status="stub"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Widget, který používá k vykreslování knihovnu Cairo.</desc>
  </info>

  <title>Widget</title>
  <media type="image" mime="image/png" src="media/widget_drawing.png"/>
  <p>Zadejte úhel a on se zhmotní.</p>

  <links type="section"/>

  <section id="code">
  <title>Kód použitý k vygenerování tohoto příkladu</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import cairo
import sys
import math


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Choose an angle", application=app)
        self.set_default_size(400, 400)
        self.set_border_width(10)

        # Výchozí úhel
        self.angle = 360

        grid = Gtk.Grid()

        # Číselník, který získává hodnotu úhlu
        ad = Gtk.Adjustment(360, 0, 360, 1, 0, 0)
        self.spin = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
        self.spin.connect("value-changed", self.get_angle)

        # Kreslicí oblast pro kreslení čehokoliv chcete
        self.darea = Gtk.DrawingArea()
        # co jsme popsali v metodě draw() napojené na signál "draw"
        self.darea.connect("draw", self.draw)
        # Musí požádat o minimální velikost kreslicí oblasti, jinak ta
        # zmizí
        self.darea.set_size_request(300, 300)

        grid.attach(self.spin, 0, 0, 1, 1)
        grid.attach(self.darea, 0, 1, 1, 1)

        self.add(grid)

    # whenever we get a new angle in the spinbutton
    def get_angle(self, event):
        self.angle = self.spin.get_value_as_int()
        # Překreslí to, co je v kreslicí oblasti
        self.darea.queue_draw()

    def draw(self, darea, cr):
        # Čára tlustá 10 pixelů
        cr.set_line_width(10)
        # Červená
        cr.set_source_rgba(0.5, 0.0, 0.0, 1.0)

        # Získá šířku a výšku kreslicí oblasti
        w = self.darea.get_allocated_width()
        h = self.darea.get_allocated_height()

        # Posune počátek se do středu kreslicí oblasti
        # (z levého horního rohu na šířka/2, výška/2)
        cr.translate(w / 2, h / 2)
        # Nakreslí čáru do (55, 0)
        cr.line_to(55, 0)
        # a zpátky do (0, 0)
        cr.line_to(0, 0)
        # Nakreslí oblouk se středem v počátku, 50 pixelů široký, od úhlu 0
        # (v radiánech) do úhlu daného hodnotou v číselníku (ve stupních)
        cr.arc(0, 0, 50, 0, self.angle * (math.pi / 180))
        # Nakreslí čáru zpět do počátku
        cr.line_to(0, 0)
        # Vykreslí cestu a ponechá ji pro pozdější použití
        cr.stroke_preserve()

        # Nastaví barvu
        cr.set_source_rgba(0.0, 0.5, 0.5, 1.0)
        # a použije ji k vyplnění cesty (kterou jsme si ponechali)
        cr.fill()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>
  
  <section id="references">
  <title>Odkazy k API</title>
  <p>V této ukázce se používá následující:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkWidget.html">GtkWidget</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkDrawingArea.html">GtkDrawingArea</link></p></item>
    <item><p><link href="http://www.tortall.net/mu/wiki/CairoTutorial">Výuka Cairo pro programátory v jazyce Python</link></p></item>
  </list>
  </section>
</page>
