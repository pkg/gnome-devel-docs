<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="GtkApplicationWindow.js" xml:lang="cs">
  <info>
  <title type="text">ApplicationWindow (JavaScript)</title>
    <link type="guide" xref="beginner.js#windows"/>
    <revision version="0.1" date="2012-04-07" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Podtřída GtkWindow s podporou GtkApplication</desc>
  </info>

  <title>ApplicationWindow</title>
  <media type="image" mime="image/png" src="media/window.png"/>
  <p>Nejjednodušší GtkApplicationWindow, které umí podporovat nabídky.</p>

<code mime="application/javascript" style="numbered">
#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class Application {

    // Vytvoří aplikaci
    constructor() {
        this.application = new Gtk.Application ({
            application_id: 'org.example.myapp',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

       // Napojí signály "activate" a "startup" k funkcím zpětného volání
       this.application.connect('activate', this._onActivate.bind(this));
       this.application.connect('startup', this._onStartup.bind(this));
    }

    // Vytvoří uživatelské rozhraní (v tomto případě jen ApplicationWindow)
    _buildUI() {
        this._window = new Gtk.ApplicationWindow({ application: this.application,
                                                   window_position: Gtk.WindowPosition.CENTER,
                                                   title: "Welcome to GNOME" });

        // Zrušte zakomentování následujícího řádku, jestli chcete nastavit velikost okna
        //this._window.set_default_size(600, 400);

        // Zobrazí okno a všechny jeho synovské widgety (v tomto případě žádné nejsou)
        this._window.show_all();
    }

    // Funkce zpětného volání pro signál "activate"
    _onActivate() {
        this._window.present();
    }

    // Funkce zpětného volání pro signál "startup"
    _onStartup() {
        this._buildUI();
    }
};

// Spustí aplikaci
let app = new Application ();
app.application.run (ARGV);
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
</list>
</page>
