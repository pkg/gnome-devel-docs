<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="spinner.py" xml:lang="cs">
  <info>
    <title type="text">Spinner (Python)</title>
    <link type="guide" xref="beginner.py#display-widgets"/>
    <link type="next" xref="progressbar.py"/>    
    <revision version="0.2" date="2012-06-12" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Animovaná káča</desc>
  </info>

  <title>Spinner</title>
  <media type="image" mime="image/png" src="media/spinner.png"/>
  <p>Tento widget Spinner se zastavuje a spouští zmáčknutím mezerníku.</p>

  <links type="section"/>

  <section id="code">
  <title>Kód použitý k vygenerování tohoto příkladu</title>

  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):
    # Okno

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Spinner Example", application=app)
        self.set_default_size(200, 200)
        self.set_border_width(30)

        # Káča
        self.spinner = Gtk.Spinner()
        # Ve výchozím stavu se točí
        self.spinner.start()
        # Přidá káču do okna
        self.add(self.spinner)

    # Obsluha události
    # Signál od klávesnice (mezerník) ovládá spuštění/zastavení káči
    def do_key_press_event(self, event):
        # keyname je symbolický název k hodnotě klávesy poskytnuté událostí
        keyname = Gdk.keyval_name(event.keyval)
        # Jestliže je "space" (mezerník)
        if keyname == "space":
            # A káča je aktivní
            if self.spinner.get_property("active"):
                # Zastaví káču
                self.spinner.stop()
            # Jestliže káča není aktivní
            else:
                # Znovu spustí káču
                self.spinner.start()
        # Zastaví vysílání signálu
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  <note><p><code>Gdk.keyval_name(event.keyval)</code> převádí hodnotu klávesy <code>event.keyval</code> na symbolický název. Názvy a odpovídající hodnoty kláves můžete najít <link href="https://gitlab.gnome.org/GNOME/gtk/blob/master/gdk/gdkkeysyms.h">zde</link>, ale například <code>GDK_KEY_BackSpace</code> se změní na řetězec <code>"BackSpace"</code>.</p></note>
  </section>

  <section id="references">
  <title>Odkazy k API</title>
  <p>V této ukázce se používá následující:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkSpinner.html">GtkSpinner</link></p></item>
    <item><p><link href="http://developer.gnome.org/gdk/stable/gdk-Keyboard-Handling.html">Hodnoty klíčů</link></p></item>
  </list>
  </section>
</page>
