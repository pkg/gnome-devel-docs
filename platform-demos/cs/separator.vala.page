<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="separator.vala" xml:lang="cs">
  <info>
    <title type="text">Separator (Vala)</title>
    <link type="guide" xref="beginner.vala#ornaments"/>
    <link type="seealso" xref="grid.vala"/>
    <link type="seealso" xref="label.vala"/>
    <link type="seealso" xref="window.vala"/>
    <revision version="0.1" date="2013-06-18" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>Oddělovací widget</desc>
  </info>

  <title>Separator</title>

  <media type="image" mime="image/png" src="media/separator.png"/>
  <p>Vodorovný a svislý oddělovač rozdělující pár popisků.</p>

  <links type="section"/>

  <section id="code">
    <title>Kód použitý k vygenerování tohoto příkladu</title>
    <code mime="text/x-csharp" style="numbered">/* Toto je aplikace. */
public class Application : Gtk.Application {

	public Application () {
		Object (application_id: "org.example.window");
	}

	/* Přepíše signál "activate" z GLib.Application, 
	 * která je zděděná z Gtk.Application. */
	public override void activate () {

		var window = new Gtk.Window ();
		window.title = "Separator Example";

		var label1 = new Gtk.Label ("Below, a horizontal separator.");
		var label2 = new Gtk.Label ("On the right, a vertical separator.");
		var label3 = new Gtk.Label ("On the left, a vertical separator.");

		var hseparator = new Gtk.Separator (Gtk.Orientation.HORIZONTAL);
		var vseparator = new Gtk.Separator (Gtk.Orientation.VERTICAL);

		var grid = new Gtk.Grid();

		grid.attach (label1, 0, 0, 3, 1);
		grid.attach (hseparator, 0, 1, 3, 1);
		grid.attach (label2, 0, 2, 1, 1);
		grid.attach (vseparator, 1, 2, 1, 1);
		grid.attach (label3, 2, 2, 1, 1);

		grid.set_column_homogeneous(true);

		window.add (grid);
		this.add_window (window);

		window.show_all ();
	}
}

/* Funkce main vytvoří aplikaci a spustí ji.*/
int main (string[] args) {
	var app = new Application ();
	return app.run (args);
}
</code>
  </section>

  <section id="reference">
    <title>Odkazy k API</title>
    <p>V této ukázce se používá následující:</p>
    <list>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">GtkApplication</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.html">GtkWindow</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Separator.html">GtkSeparator</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Grid.html">GtkGrid</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Label.html">GtkLabel</link></p></item>
    </list>
  </section>

</page>
