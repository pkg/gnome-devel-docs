<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:e="http://projectmallard.org/experimental/" type="guide" style="task" id="properties.py" xml:lang="cs">

<info>
  <title type="text">Vlastnosti (Python)</title>
  <link type="guide" xref="beginner.py#theory"/>
  <link type="next" xref="grid.py"/>
  <revision version="0.1" date="2012-06-24" status="draft"/>

  <desc>Vysvětlení vlastností a jejich získávání a nastavování.</desc>
  <credit type="author copyright">
    <name>Sebastian Pölsterl</name>
    <email its:translate="no">sebp@k-d-w.org</email>
    <years>2011</years>
  </credit>
  <credit type="editor">
    <name>Marta Maria Casetti</name>
    <email its:translate="no">mmcasetti@gmail.com</email>
    <years>2012</years>
  </credit>
</info>

<title>Vlastnosti</title>

<links type="section"/>

<section id="overview">
<title>Přehled</title>

<p><em>Vlastnosti</em> popisují nastavení a stav widgetů. Každý widget má svoji vlastní konkrétní sadu vlastností. Například takový widget tlačítko má vlastnost <code>label</code>, která obsahuje text widgetu. Název a hodnotu libovolného počtu vlastností můžete zadat jako argumenty s klíčovými slovy při vytváření instance widgetu. Například k vytvoření popisku s textem „Hello World“, úhlem 25 stupňů a zarovnáním doprava, můžete použít:</p>
<code>
label = Gtk.Label(label="Hello World", angle=25, halign=Gtk.Align.END)</code>

<p>Případně můžete tyto vlastnosti definovat postupně pomocí metod, které k nim patří.</p>
<code>
label = Gtk.Label()
label.set_label("Hello World")
label.set_angle(25)
label.set_halign(Gtk.Align.END)</code>

<p>Jakmile máte takovýto popisek vytvořený, můžete získat jeho text pomocí <code>label.get_label()</code> a obdobně i ostatní jeho vlastnosti.</p>

<p>Místo jednotlivých metod pro získání a nastavení vlastností, můžete vlastnosti získat a nastavit pomocí <code>get_property(<var>"název-vlastnoti"</var>)</code> respektive <code>set_property(<var>"název-vlastnosti"</var>, <var>hodnota</var>)</code>.</p>

</section>
<section id="references">
<title>Odkazy</title>

<p><link href="http://python-gtk-3-tutorial.readthedocs.org/en/latest/basics.html">Základy – Vlastnosti</link> ve výuce GTK+ 3 v jazyce Python</p>
</section>

</page>
