<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="entry.js" xml:lang="cs">
  <info>
  <title type="text">Entry (JavaScript)</title>
    <link type="guide" xref="beginner.js#entry"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>Jednořádkové textové vstupní pole</desc>
  </info>

  <title>Entry</title>
  <media type="image" mime="image/png" src="media/entry.png"/>
  <p>Tato aplikace vás přivítá jménem ve vyskakovacím okně.</p>

<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

class EntryExample {

    // Vytvoří vlastní aplikaci
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsentry',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Napojí signály "activate" a "startup" k funkcím zpětného volání
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Funkce zpětného volání pro signál "activate" zobrazujicí okno při aktivaci
    _onActivate() {
        this._window.present();
    }

    // Funkce zpětného volání pro signál "startup" sestavující uživatelské rozhraní
    _onStartup() {
        this._buildUI();
    }

    // Sestaví uživatelské rozhraní aplikace
    _buildUI() {

        // Vytvoří okno aplikace
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            default_height: 100,
            default_width: 300,
            border_width: 10,
            title: "What is your name?"});

        // Vytvoří textové vstupní pole
        this.entry = new Gtk.Entry ();
        this._window.add(this.entry);

        // Napojí textové vstupní pole na funkci, která reaguje na to, co napíšete
        this.entry.connect("activate", this._hello.bind(this));

        // Zobrazí okno a všechny jeho synovské widgety
        this._window.show_all();
    }

    _hello() {

        // Vytvoří vyskakovací dialogové okno, které přivítá osobu, co napsala své jméno
        this._greeter = new Gtk.MessageDialog ({
            transient_for: this._window,
            modal: true,
            text: "Hello, " + this.entry.get_text() + "!",
            message_type: Gtk.MessageType.OTHER,
            buttons: Gtk.ButtonsType.OK,
        });

        // Zobrazí dialogové okno
        this._greeter.show();

        // Napojí tlačítko OK na funkci, která zavírá dialogové okno
        this._greeter.connect ("response", this._okClicked.bind(this));
    }

    _okClicked() {
        this._greeter.destroy();
    }

};

// Spustí aplikaci
let app = new EntryExample ();
app.application.run (ARGV);
</code>
<p>V této ukázce se používá následující:</p>
<list>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Entry.html">Gtk.Entry</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.MessageDialog.html">Gtk.MessageDialog</link></p></item>
</list>
</page>
