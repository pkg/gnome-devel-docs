<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="paned.vala" xml:lang="ko">
  <info>
    <title type="text">Paned (Vala)</title>
    <link type="guide" xref="beginner.vala#layout"/>
    <revision version="0.1" date="2013-06-19" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2013</years>
    </credit>

    <desc>크기 조절 가능한 두 창을 가진 위젯입니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Paned</title>
  <media type="image" mime="image/png" src="media/paned.png"/>
  <p>크기 조절 가능한 두 창에 그림 둘을 두고 수평 방향으로 정렬합니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-csharp" style="numbered">/* This is the application. */
public class MyApplication : Gtk.Application {
	/* Override the 'activate' signal of GLib.Application. */
	protected override void activate () {

		var window = new Gtk.ApplicationWindow (this);
		window.title = "Paned Example";
		window.set_default_size (450,350);

		// a new widget with two adjustable panes,
		// one on the left and one on the right
		var paned = new Gtk.Paned (Gtk.Orientation.HORIZONTAL);

		/* two images */
		var image1 = new Gtk.Image ();
		image1.set_from_file ("gnome-image.png");
		var image2 = new Gtk.Image ();
		image2.set_from_file ("tux.png");

		/* add the first image to the left pane */
		paned.add1 (image1);

		/* add the second image to the right pane */
		paned.add2 (image2);

		/* add the panes to the window */
		window.add (paned);
		window.show_all ();
	}
}

/* main creates and runs the application. */
public int main (string[] args) {
	return new MyApplication ().run (args);
}
</code>
  </section>

  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Paned.html">GtkPaned</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Standard-Enumerations.html#GtkOrientation">표준 에뮬레이션</link></p></item>
      <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Image.html">GtkImage</link></p></item>
    </list>
  </section>
</page>
