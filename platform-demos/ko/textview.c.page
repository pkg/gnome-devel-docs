<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="textview.c" xml:lang="ko">
  <info>
    <title type="text">TextView (C)</title>
    <link type="guide" xref="c#multiline"/>
    <revision version="0.1" date="2012-07-10" status="draft"/>

    <credit type="author copyright">
      <name>Monica Kochofar</name>
      <email its:translate="no">monicakochofar@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>GtkTextBuffer를 나타내는 위젯</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>TextView 위젯</title>
<note style="sidebar"><p>"enter" 키를 누르면 줄이 바뀝니다.</p>
     <p>"enter" 키를 더 누르면 기본 크기 창 안에 줄이 더 생기며, 창 크기를 넘으면서 수직 스크롤 표시줄이 나타납니다.</p>
     <p>긴 문장을 적어넣으면 문장에 있는 단어 사이에서 줄바꿈이 일어납니다.</p>
     <p>기이이이이이이이이이이이이이이이이이이인 단어가 있다면(길었다면), 수평 스크롤 표시줄이 나타납니다.</p></note>

  <media type="image" mime="image/png" src="media/textview.png"/>
    <p>Gtk.TextView 예제입니다</p>

      <code mime="text/x-csrc" style="numbered">
#include &lt;gtk/gtk.h&gt;



static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  /* Declare variables */
  GtkWidget *window;
  GtkWidget *text_view;
  GtkWidget *scrolled_window;

  GtkTextBuffer *buffer;


  /* Create a window with a title, and a default size */
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "TextView Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 220, 200);


  /* The text buffer represents the text being edited */
  buffer = gtk_text_buffer_new (NULL);
  

  /* Text view is a widget in which can display the text buffer. 
   * The line wrapping is set to break lines in between words.
   */
  text_view = gtk_text_view_new_with_buffer (buffer);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (text_view), GTK_WRAP_WORD); 


  /* Create the scrolled window. Usually NULL is passed for both parameters so 
   * that it creates the horizontal/vertical adjustments automatically. Setting 
   * the scrollbar policy to automatic allows the scrollbars to only show up 
   * when needed. 
   */
  scrolled_window = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window), 
                                  GTK_POLICY_AUTOMATIC, 
                                  GTK_POLICY_AUTOMATIC); 
  /* The function directly below is used to add children to the scrolled window 
   * with scrolling capabilities (e.g text_view), otherwise, 
   * gtk_scrolled_window_add_with_viewport() would have been used
   */
  gtk_container_add (GTK_CONTAINER (scrolled_window), 
                                         text_view);
  gtk_container_set_border_width (GTK_CONTAINER (scrolled_window), 5);
 
  
  gtk_container_add (GTK_CONTAINER (window), scrolled_window);

  gtk_widget_show_all (window);
}



int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
</code>
<p>이 예제는 다음 참고자료가 필요합니다:</p>
<list>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkApplication.html">GtkApplication</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/3.4/GtkWindow.html">GtkWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkTextBuffer.html">GtkTextBuffer</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkTextView.html">GtkTextView</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkScrolledWindow.html">GtkScrolledWindow</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkContainer.html">GtkContainer</link></p></item>
</list>
</page>
