<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="hello-world" xml:lang="ko">

  <info>
    <!-- The text title is used on the help.gnome.org -->
    <title type="link">Hello World (C)</title>
    <link type="guide" xref="c#examples"/>
    <revision version="0.1" date="2013-06-17" status="review"/>

    <credit type="author copyright">
      <name>Susanna Huhtanen</name>
      <email its:translate="no">ihmis.suski@gmail.com</email>
      <years>2012</years>
    </credit>
    <credit type="editor">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>
    <credit type="editor">
      <name>Bastian Ilsø</name>
      <email its:translate="no">bastianilso@gnome.org</email>
    </credit>

    <desc>GTK+로 작은 "Hello, World" 프로그램을 만듭니다.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Hello world</title>

  <note>
    <p>C 언어로 GTK+ 대화상자를 만드는 자세한 설명은 <link href="https://developer.gnome.org/gtk3/stable/gtk-getting-started.html">GTK+ 시작하기</link>를 참고하십시오</p>
  </note>

  <p>C 언어로의 hello world GTK+ 대화 상자 프로그램 작성은, 아래 보여드리는 예제 코드처럼 할 수 있습니다:</p>
  <code mime="text/x-csrc" style="numbered">
    #include &lt;gtk/gtk.h&gt;

static void
activate (GtkApplication* app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *label;

  window = gtk_application_window_new (app);
  label = gtk_label_new ("Hello GNOME!");
  gtk_container_add (GTK_CONTAINER (window), label);
  gtk_window_set_title (GTK_WINDOW (window), "Welcome to GNOME");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 100);
  gtk_widget_show_all (window);
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new (NULL, G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}

  </code>

  <p>GtkApplication은 GTK+를 초기화합니다. 또한 창을 만들 때 자동으로 붙인 <gui>x</gui> 단추를 "destroy" 시그널에 연결합니다. 첫 창 만들기를 시작할 수 있습니다. <var>window</var> 변수를 만들고 gtk_application_window_new를 할당하겠습니다. 제목은 원하는대로 지을 수 있습니다. 안전한 방편으로, UTF-8 인코딩으로 작성하시는게 좋습니다. 위 코드는 아래와 같이 보이는 대화상자 창을 만듭니다:</p>

  <media type="image" mime="image/png" src="media/hello-world.png"/>
</page>
