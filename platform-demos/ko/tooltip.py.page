<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="tooltip.py" xml:lang="ko">
  <info>
  <title type="text">Tooltip (Python)</title>
    <link type="guide" xref="beginner.py#misc"/>
    <link type="seealso" xref="toolbar.py"/>
    <link type="next" xref="toolbar_builder.py"/>
    <revision version="0.1" date="2012-08-20" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>위젯에 도움말을 추가합니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>Tooltip</title>
  <media type="image" mime="image/png" src="media/tooltip.png"/>
  <p>단추에 풍선 도움말(및 그림)을 넣은 도구 모음입니다.</p>
  <note><p>이 예제는 <link xref="toolbar.py">도구 모음</link> 예제로 만듭니다.</p></note>

  <links type="section"/>
    
  <section id="code">
  <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(
            self, title="Toolbar with Tooltips Example", application=app)
        self.set_default_size(400, 200)

        grid = Gtk.Grid()

        toolbar = self.create_toolbar()
        toolbar.set_hexpand(True)
        toolbar.show()

        grid.attach(toolbar, 0, 0, 1, 1)

        self.add(grid)

        undo_action = Gio.SimpleAction.new("undo", None)
        undo_action.connect("activate", self.undo_callback)
        self.add_action(undo_action)

        fullscreen_action = Gio.SimpleAction.new("fullscreen", None)
        fullscreen_action.connect("activate", self.fullscreen_callback)
        self.add_action(fullscreen_action)

    def create_toolbar(self):
        toolbar = Gtk.Toolbar()
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR)

        # button for the "new" action
        new_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_NEW)
        # with a tooltip with a given text
        new_button.set_tooltip_text("Create a new file")
        new_button.set_is_important(True)
        toolbar.insert(new_button, 0)
        new_button.show()
        new_button.set_action_name("app.new")

        # button for the "open" action
        open_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_OPEN)
        # with a tooltip with a given text in the Pango markup language
        open_button.set_tooltip_markup("Open an &lt;i&gt;existing&lt;/i&gt; file")
        open_button.set_is_important(True)
        toolbar.insert(open_button, 1)
        open_button.show()
        open_button.set_action_name("app.open")

        # button for the "undo" action
        undo_button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_UNDO)
        # with a tooltip with an image
        # set True the property "has-tooltip"
        undo_button.set_property("has-tooltip", True)
        # connect to the callback function that for the tooltip
        # with the signal "query-tooltip"
        undo_button.connect("query-tooltip", self.undo_tooltip_callback)
        undo_button.set_is_important(True)
        toolbar.insert(undo_button, 2)
        undo_button.show()
        undo_button.set_action_name("win.undo")

        # button for the "fullscreen/leave fullscreen" action
        self.fullscreen_button = Gtk.ToolButton.new_from_stock(
            Gtk.STOCK_FULLSCREEN)
        self.fullscreen_button.set_is_important(True)
        toolbar.insert(self.fullscreen_button, 3)
        self.fullscreen_button.set_action_name("win.fullscreen")

        return toolbar

    # the callback function for the tooltip of the "undo" button
    def undo_tooltip_callback(self, widget, x, y, keyboard_mode, tooltip):
        # set the text for the tooltip
        tooltip.set_text("Undo your last action")
        # set an icon for the tooltip
        tooltip.set_icon_from_stock("gtk-undo", Gtk.IconSize.MENU)
        # show the tooltip
        return True

    def undo_callback(self, action, parameter):
        print("You clicked \"Undo\".")

    def fullscreen_callback(self, action, parameter):
        is_fullscreen = self.get_window().get_state(
        ) &amp; Gdk.WindowState.FULLSCREEN != 0
        if not is_fullscreen:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_LEAVE_FULLSCREEN)
            self.fullscreen()
        else:
            self.fullscreen_button.set_stock_id(Gtk.STOCK_FULLSCREEN)
            self.unfullscreen()


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        app.add_action(new_action)

        open_action = Gio.SimpleAction.new("open", None)
        open_action.connect("activate", self.open_callback)
        app.add_action(open_action)

    def new_callback(self, action, parameter):
        print("You clicked \"New\".")

    def open_callback(self, action, parameter):
        print("You clicked \"Open\".")

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Tooltip 위젯에 쓸만한 메서드</title>

    <p><code>set_tooltip_text(text)</code> 함수와 <code>set_tooltip_markup(text)</code> 함수는 일반 텍스트(또는 팡고 마크업 언어 텍스트) 풍선 도움말을 위젯에 추가할 때 활용할 수 있습니다.</p>
    <p>더 복잡한 풍선 도움말은, 그림 위의 풍선 도움말을 예로 들 수 있습니다:</p>
    <steps>
      <item><p>위젯의 <code>"has-tooltip"</code> 속성을 <code>True</code> 값으로 설정하십시오. GTK+에서 풍선 도움말을 보여줄 때와 위치를 결정할 때 필요한 위젯의 움직임과 관련 이벤트를 지겨보게합니다.</p></item>
      <item><p><code>"query-tooltip"</code> 시그널에 연결하십시오. 이 시그널은 풍선 도움말을 나타낼 때 내보냅니다. 시그널 핸들러에 보내는 인자 중 하나는 GtkTooptip 객체입니다. 우리가 풍선 도움말로 나타내려는 객체고, <code>set_icon()</code> 같은 함수로 다룰 수 있습니다. 풍선 도움말의 마크업을 설정(<code>set_markup(text)</code>), 스톡 아이콘으로 그림을 설정(<code>set_icon_from_stock(stock_id, size)</code>), 또는 개별 위젯을 두는(<code>set_custom(widget)</code>) 몇가지 함수가 있습니다.</p></item>
      <item><p>query-tooltip 핸들러에서 <code>True</code> 값을 반환하십시오. 풍선 도움말을 나타냅니다. <code>False</code> 값을 반환하면 풍선 도움말이 나타나지 않습니다.</p></item>
    </steps>

    <p>풍선 도움말이 나타날 때 즈음에 풍선 도움말에 대해 뭔가를 더 다루는 경우는 흔하지 않는데, 이 경우 풍선 도움말 창으로 활용할 GtkWindow를 설정할 수 있습니다. 다음과 같이 하시면 됩니다:</p>
    <steps>
      <item><p><code>"has-tooltip"</code>을 설정하고 이전과 같이 <code>"query-tooltip"</code> 에 연결하십시오.</p></item>
      <item><p>풍선 도움말 창으로 만든 GTKWindow를 설정하려면 <code>set_tooltip_window()</code> 함수를 위젯에서 사용하십시오.</p></item>
      <item><p><code>"query-tooltip"</code> 콜백에서는 <code>get_tooltip_window()</code> 함수로 창에 접근할 수 있고 원하는대로 조절할 수 있습니다. 반환값 의미는 이전에 언급한 바와 같습니다. <code>True</code> 반환 값은 창을 나타내고 <code>False</code>값은 창을 나타내지 않습니다.</p></item>
    </steps>

  </section>
  
  <section id="references">
  <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkTooltip.html">GtkTooltip</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkToolbar.html">GtkToolbar</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkWidget.html">GtkWidget</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Stock-Items.html">취급 항목</link></p></item>
    </list>
  </section>
</page>
