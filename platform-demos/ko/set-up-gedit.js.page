<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="topic" style="task" id="set-up-gedit.js" xml:lang="ko">
  <info>
    <link type="guide" xref="beginner.js#tutorials"/>
    <revision version="0.1" date="2012-07-17" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>JavaScript 개발용 지에디트 설정</title>
  <p>This tutorial will show you how to set up <link href="https://wiki.gnome.org/Apps/Gedit">gedit</link>, GNOME's basic text editor, so that it has a handful of extra features which are useful for writing JavaScript code.</p>

  <section id="view">
    <title>코드를 명확하게 하기</title>
    <p>지에디트 메뉴 표시줄에서 <gui>편집</gui>을 누르고 <gui>기본 설정</gui>을 누르십시오. 다음과 같은 화면을 볼 수 있습니다:</p>
    <media type="image" mime="image/png" src="media/geditview.png"/>
    <p>활성화 여부를 확인할 옵션은 다음과 같습니다.</p>
    <steps>
      <item><p><gui>줄 번호 표시</gui> 옵션은 원래 여러분이 입력한 코드를 비교할 때 도움을 주며, 버그가 있을 경우 오류가 난 줄을 쉽게 찾아볼 수 있습니다.</p></item>
      <item><p><gui>현재 줄 강조</gui> 옵션은 줄 위아래로 왔다갔다 하고 있을 때 몇번째 줄을 편집하고 있는지 쉽게 살펴볼 수 있게 합니다.</p></item>
      <item><p><gui>일치하는 대괄호 강조</gui> 옵션은 실수로 괄호를 빼먹었는지 확인할 수 있게 합니다.</p></item>
    </steps>
  </section>

  <section id="edit">
    <title>코드 편집을 쉽게 하기</title>
    <p>지에디트의 <gui>기본 설정</gui> 대화상자에서  <gui>편집기</gui> 탭을 누르십시오. 다음과 같은 화면을 볼 수 있습니다:</p>
    <media type="image" mime="image/png" src="media/gediteditor.png"/>
    <p><gui>자동 들여 쓰기 사용</gui>을 원하실지도 모르겠습니다. <key>Enter</key>키를 치면 마지막 줄을 들여쓴 만큼 커서를 둡니다. 들여쓸 부분에 들여쓰기를 사용하여 코드를 더 알아보기 쉽게 하므로, JavaScript 코드를 작성할 때 상당히 쓸만합니다.</p>
    <note style="tip"><p>그놈 JavaScript 프로그램을 작성하는 다른 사람과 코드를 공유하려 한다면, <gui>탭 간격</gui> 을 4로 설정하고 <gui>탭 대신 공백 입력</gui>을 사용하십시오.</p></note>
  </section>






</page>
