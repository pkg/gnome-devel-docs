<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="menubutton.py" xml:lang="ko">
  <info>
  <title type="text">MenuButton</title>
    <link type="guide" xref="beginner.py#buttons"/>
    <link type="next" xref="toolbar.py"/>
    <revision version="0.1" date="2012-08-19" status="draft"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>누르면 메뉴를 보여주는 위젯입니다</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>MenuButton</title>
  <media type="image" mime="image/png" src="media/menubutton.png"/>
  <p>GtkMenuButton 위젯은 눌렀을 때 메뉴를 띄울 목적으로 활용합니다. 이 메뉴는 GtkMenu로 또는 GMenuModel로도 제공합니다. GtkMenuButton 위젯은 제대로 된 하위 위젯을 지니고 있을 수 있습니다. 즉, GtkMenuButton으로 대부분의 다른 표준 GtkWidget을 붙들어둘 수 있습니다. 가장 널리 활용하는 하위 요소는 GtkArrow입니다.</p>

  <note><p>MenuButton을 동작하게 하려면 그놈 3.6이 필요합니다.</p></note>

  <links type="section"/>
    
  <section id="code">
  <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gio
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Menubutton Example", application=app)
        self.set_default_size(600, 400)

        grid = Gtk.Grid()

        # a menubutton
        menubutton = Gtk.MenuButton()
        menubutton.set_size_request(80, 35)

        grid.attach(menubutton, 0, 0, 1, 1)

        # a menu with two actions
        menumodel = Gio.Menu()
        menumodel.append("New", "app.new")
        menumodel.append("About", "win.about")

        # a submenu with one action for the menu
        submenu = Gio.Menu()
        submenu.append("Quit", "app.quit")
        menumodel.append_submenu("Other", submenu)

        # the menu is set as the menu of the menubutton
        menubutton.set_menu_model(menumodel)

        # the action related to the window (about)
        about_action = Gio.SimpleAction.new("about", None)
        about_action.connect("activate", self.about_callback)
        self.add_action(about_action)

        self.add(grid)

    # callback for "about"
    def about_callback(self, action, parameter):
        print("You clicked \"About\"")


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

        #  the actions related to the application
        new_action = Gio.SimpleAction.new("new", None)
        new_action.connect("activate", self.new_callback)
        self.add_action(new_action)

        quit_action = Gio.SimpleAction.new("quit", None)
        quit_action.connect("activate", self.quit_callback)
        self.add_action(quit_action)

    # callback functions for the actions related to the application
    def new_callback(self, action, parameter):
        print("You clicked \"New\"")

    def quit_callback(self, action, parameter):
        print("You clicked \"Quit\"")
        self.quit()

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>MenuButton 위젯에 쓸만한 메서드</title>
      <p>33번째 줄에서 <code>"activate"</code> 시그널은 <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code> 함수로  <code>about_callback()</code> 콜백 함수에 연결했습니다. 더 자세한 설명은 <link xref="signals-callbacks.py"/>를 참조하십시오.</p>

    <p>메뉴 배치는 메뉴 단추의 "direction" 속성과 메뉴의 "halign" 속성 또는  "valign" 속성으로 결정합니다. 예를 들어 direction 값이 <code>Gtk.ArrowType.DOWN</code>(다른 옵션: <code>UP</code>)이고, 수평 정렬을 <code>Gtk.Align.START</code>로(다른 옵션: <code>CENTER</code>, <code>END</code>) 했다면 메뉴는, 시작 모서리(텍스트 방향에 따라)를 따라 정렬한 메뉴의 시작 모서리로 단추 아래에 들어갑니다. 단추 아래에 공간이 충분치 않다면 단추 위로 대신 메뉴를 띄웁니다. 정렬 상태가 메뉴 바깥 부분을 움직인다면 '눌러넣은' 상태입니다.</p>
    
    <p>수직으로 정렬하는 경우 적용할 수 있는 ArrowType 방향은  <code>LEFT</code>와 <code>RIGHT</code> 이며, <code>START</code>, <code>CENTER</code>, <code>END</code> 중 한 가지 방식으로 수직 정렬합니다.나입니다.</p>
    
    <p>이 속성을 설정할 때 <code>set_align_widget(alignment)</code> 메서드와 <code>set_direction(direction)</code> 메서드를 사용할 수 있습니다.</p>
  </section>
  
  <section id="references">
  <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkMenuButton.html">MenuButton</link></p></item>
    </list>
  </section>
</page>
