<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="combobox_multicolumn.py" xml:lang="ko">
  <info>
    <title type="text">ComboBox(Python)</title>
    <link type="guide" xref="beginner.py#menu-combo-toolbar"/>
    <link type="next" xref="treeview_advanced_liststore.py"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>항목 목록을 선택할 때 사용하는 위젯</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>조성호</mal:name>
      <mal:email>shcho@gnome.org</mal:email>
      <mal:years>2017-2019.</mal:years>
    </mal:credit>
  </info>

  <title>ComboBox(두 칸 짜리)</title>
  <media type="image" mime="image/png" src="media/combobox_multicolumn.png"/>
  <p>이 ComboBox는 여러분이 바꿔서 선택한 내용을 터미널에 나타냅니다.</p>

  <links type="section"/>

  <section id="code">
    <title>예제 결과를 만드는 코드</title>
    <code mime="text/x-python" style="numbered">from gi.repository import Gtk
import sys

actions = [["Select", None],
           ["New", Gtk.STOCK_NEW],
           ["Open", Gtk.STOCK_OPEN],
           ["Save", Gtk.STOCK_SAVE]]


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Welcome to GNOME", application=app)
        self.set_default_size(200, -1)
        self.set_border_width(10)

        # the data in the model, of type string on two columns
        listmodel = Gtk.ListStore(str, str)
        # append the data
        for i in range(len(actions)):
            listmodel.append(actions[i])

        # a combobox to see the data stored in the model
        combobox = Gtk.ComboBox(model=listmodel)

        # cellrenderers to render the data
        renderer_pixbuf = Gtk.CellRendererPixbuf()
        renderer_text = Gtk.CellRendererText()

        # we pack the cell into the beginning of the combobox, allocating
        # no more space than needed;
        # first the image, then the text;
        # note that it does not matter in which order they are in the model,
        # the visualization is decided by the order of the cellrenderers
        combobox.pack_start(renderer_pixbuf, False)
        combobox.pack_start(renderer_text, False)

        # associate a property of the cellrenderer to a column in the model
        # used by the combobox
        combobox.add_attribute(renderer_text, "text", 0)
        combobox.add_attribute(renderer_pixbuf, "stock_id", 1)

        # the first row is the active one at the beginning
        combobox.set_active(0)

        # connect the signal emitted when a row is selected to the callback
        # function
        combobox.connect("changed", self.on_changed)

        # add the combobox to the window
        self.add(combobox)

    def on_changed(self, combo):
        # if the row selected is not the first one, write on the terminal
        # the value of the first column in the model
        if combo.get_active() != 0:
            print("You chose " + str(actions[combo.get_active()][0]) + "\n")
        return True


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>

  </section>

  <section id="methods">
    <title>ComboBox 위젯에 쓸만한 함수</title>
    <p>ComboBox 위젯은 <em>모델/뷰/컨트롤러</em> 설계 방식따라 설계했습니다. <em>모델</em>에는 데이터가 들어가고요, <em>View</em>에는 바뀜 알림을 받아서 모델의 내용을 표시합니다. 마지막으로, <em>컨트롤러</em>에서는, 모델의 상태를 바꾸고 뷰가 바뀌었다는걸 알리죠. ComboBox에서 쓸만한 메서드 목록 내용은 <link xref="model-view-controller.py"/>를 확인해보시죠.</p>
    <p>45번째 줄에서, <code><var>widget</var>.connect(<var>signal</var>, <var>callback function</var>)</code> 코드로  <code>"changed"</code> 시그널을 <code>on_changed()</code> 콜백 함수에 연결했습니다. 더 자세한 설명은 <link xref="signals-callbacks.py"/>를 참조하십시오.</p>
  </section>

  <section id="references">
    <title>API 참고서</title>
    <p>이 예제는 다음 참고자료가 필요합니다:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkComboBox.html">GtkComboBox</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkListStore.html">GtkListStore</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererText.html">GtkCellRendererText</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkCellRendererPixbuf.html">GtkCellRendererPixbuf</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/gtk3-Stock-Items.html">스톡 항목</link></p></item>
      <item><p><link href="https://gitlab.gnome.org/GNOME/pygobject/blob/master/gi/overrides/Gtk.py">pygobject - Python bindings for GObject Introspection</link></p></item>
    </list>
  </section>
</page>
