<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="switch.js" xml:lang="fr">
  <info>
  <title type="text">Switch (JavaScript)</title>
    <link type="guide" xref="beginner.js#buttons"/>
    <revision version="0.1" date="2012-06-18" status="draft"/>

    <credit type="author copyright">
      <name>Taryn Fox</name>
      <email its:translate="no">jewelfox@fursona.net</email>
      <years>2012</years>
    </credit>

    <desc>Un interrupteur à glissière qui peut être allumé ou éteint</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Switch (interrupteur)</title>
  <media type="image" mime="image/png" src="media/switchanimals.png"/>
  <p>A Switch has two positions, on and off. This example shows how you can use multiple switches together to control which <link xref="image.js">Image</link> is shown in the window. The pictures used in this example <link href="https://live.gnome.org/TarynFox?action=AttachFile&amp;do=get&amp;target=Animal+Photos.zip">can be downloaded here</link>.</p>
  <note><p>Si les fichiers <file>redfox.png</file>, <file>muteswan.png</file>, <file>fruitbat.png</file> et <file>gentoopenguin.png</file> ne se trouvent pas dans le même dossier, la fenêtre affichera une icône « image brisée ». Vous pouvez modifier le code et les images comme bon vous semble, mais sachez que les photos sous licence Creative Commons utilisées dans cet exemple proviennent des sources suivantes et ont été rognées à 640x435 :</p>
    <list>
        <item><p><link href="http://en.wikipedia.org/wiki/File:Fuzzy_Freddy.jpg">Red fox photo</link> (renard rouge) par Rob Lee, licence <link href="http://creativecommons.org/licenses/by/2.0/deed.en">CC-By</link></p></item>
        <item><p><link href="http://en.wikipedia.org/wiki/File:Pygoscelis_papua_-Nagasaki_Penguin_Aquarium_-swimming_underwater-8a.jpg">Gentoo penguinphoto</link> (pingouin) par Ken Funakoshi, licence <link href="http://creativecommons.org/licenses/by-sa/2.0/deed.en">CC-By-SA</link></p></item>
        <item><p><link href="http://www.flickr.com/photos/shekgraham/127431519/in/photostream/">Fruit bat photo</link> (chauve-souris) par Shek Graham, licence <link href="http://creativecommons.org/licenses/by/2.0/deed.en">CC-By</link></p></item>
        <item><p><link href="http://commons.wikimedia.org/wiki/File:Mute_Swan-Mindaugas_Urbonas.jpg">Mute swan photo</link> (cygne) par Mindaugas Urbonas, licence <link href="http://creativecommons.org/licenses/by-sa/2.5/deed.en">CC-By-SA</link></p></item>
    </list>
    <p>Photo credits and licensing information are shown in the application's <link xref="aboutdialog.js">AboutDialog</link>. Always remember to credit the original artist when using <link href="http://creativecommons.org">Creative Commons-licensed works!</link></p></note>
    <links type="section"/>

  <section id="imports">
    <title>Bibliothèques à importer</title>
    <code mime="application/javascript"><![CDATA[
#!/usr/bin/gjs

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
]]></code>
    <p>Ce sont les bibliothèques que nous devons importer pour faire fonctionner cette application. N'oubliez pas que la ligne qui informe GNOME que nous allons utiliser Gjs doit toujours se trouver au début.</p>
    </section>

  <section id="applicationwindow">
    <title>Création de la fenêtre de l'application</title>
    <code mime="application/javascript"><![CDATA[
const SwitchExample = new Lang.Class({
    Name: 'Switch Example',

    // Create the application itself
    _init: function() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsswitch',
            flags: Gio.ApplicationFlags.FLAGS_NONE
        });

    // Connect 'activate' and 'startup' signals to the callback functions
    this.application.connect('activate', Lang.bind(this, this._onActivate));
    this.application.connect('startup', Lang.bind(this, this._onStartup));
    },

    // Callback function for 'activate' signal presents window when active
    _onActivate: function() {
        this._window.present();
    },

    // Callback function for 'startup' signal creates the menu and builds the UI
    _onStartup: function() {
        this._initMenus();
        this._buildUI ();
    },
]]></code>
    <p>Tout le code de cet exemple est contenu dans la classe SwitchExample. Le code ci-dessus crée une <link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link> pour nos éléments graphiques et la fenêtre qui les contient.</p>
    <note><p>Avant d'appeler _buildUI pour créer la fenêtre et les éléments graphiques qu'elle contient, il nous faut appeler _initMenus, qui indique à GNOME de générer le menu. Nous pouvons mettre le code actuel pour _initMenus après le code pour _buildUI, car l'ordre n'a pas d'importance du moment que _initMenus est appelé en premier dans _onStartup.</p></note>
    <code mime="application/javascript"><![CDATA[
    // Build the application's UI
    _buildUI: function() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            border_width: 20,
            title: "Animal Creator"});
]]></code>
    <p>La fonction _buildUI est l'endroit où nous mettons tout le code nécessaire à la création de l'interface utilisateur de l'application. La première étape consiste à créer une <link xref="GtkApplicationWindow.js">Gtk.ApplicationWindow</link> pour y mettre tous nos éléments graphiques.</p>
  </section>

  <section id="button">
    <title>Création des interrupteurs</title>
    <code mime="application/javascript"><![CDATA[
        // Create the image widget and set its default picture
        this._image = new Gtk.Image ({file: "redfox.png"});
]]></code>

    <p>Créons en premier l'<link xref="image.js">Image</link> que les interrupteurs vont contrôler. Souvenez-vous qu'un fichier nommé <file>redfox.png</file> doit être présent dans le même dossier que cette application.</p>

    <code mime="application/javascript"><![CDATA[
        // Create a label for the first switch
        this._flyLabel = new Gtk.Label ({
            label: "Make it fly",
            margin_right: 30});

        // Create the first switch and set its default position
        this._flySwitch = new Gtk.Switch ({active: false});
        this._flySwitch.connect ('notify::active', Lang.bind (this, this._switchFlip));

        // Create a label for the second switch
        this._birdLabel = new Gtk.Label ({
            label: "Make it a bird",
            margin_right: 30});

        // Create the second switch
        this._birdSwitch = new Gtk.Switch ({active: false});
        this._birdSwitch.connect ('notify::active', Lang.bind (this, this._switchFlip));
]]></code>

    <p>Utilisons un <link xref="label.js">Label</link> pour étiqueter chaque interrupteur et laissons-leur un peu de marge à droite pour qu'ils ne débordent pas les uns sur les autres. Créons ensuite les interrupteurs et paramétrons-les pour qu'ils soient en position « éteint » par défaut.</p>
    <p>Le signal qu'un interrupteur envoie quand il est basculé dans un sens ou dans l'autre est appelé notify::active. Après avoir créé chaque interrupteur, connectons son signal notify::active à une fonction appelée _switchFlip. Si vous avez plein d'interrupteurs qui font chacun quelque chose de différent, vous pouvez les connecter à différentes fonctions, mais ici, ils sont tous les deux utilisés pour la même chose : contrôler quelle image est affichée par _image.</p>

    <code mime="application/javascript"><![CDATA[
        // Create a grid for the labels and switches beneath the picture
        this._UIGrid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER,
            margin_top: 20});

        // Attach the labels and switches to that grid
        this._UIGrid.attach (this._flyLabel, 0, 0, 1, 1);
        this._UIGrid.attach (this._flySwitch, 1, 0, 1, 1);
        this._UIGrid.attach (this._birdLabel, 0, 1, 1, 1);
        this._UIGrid.attach (this._birdSwitch, 1, 1, 1, 1);

        // Create a master grid to put both the UI and the picture into
        this._mainGrid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER });

        // Attach the picture and the UI grid to the master grid
        this._mainGrid.attach (this._image, 0, 0, 1, 1);
        this._mainGrid.attach (this._UIGrid, 0, 1, 1, 1);
]]></code>
    <p>Créons une <link xref="grid.js">grille</link> pour les étiquettes et les interrupteurs de façon à les organiser dans un agencement 2x2 avec une marge entre eux et l'image. Ensuite, mettons cette grille dans une plus grande au format 2x1 qui contient l'image en haut et la grille avec les étiquettes et les interrupteurs en bas.</p>
    <code mime="application/javascript"><![CDATA[
        // Add the master grid to the window
        this._window.add (this._mainGrid);

        // Show the window and all child widgets
        this._window.show_all();
    },
]]></code>
    <p>Enfin, ajoutons la grille la plus grande à la fenêtre et indiquons à la fenêtre de s'afficher avec tous les éléments graphiques à l'intérieur.</p>
    </section>

    <section id="switch-handler">
    <title>Fonction prenant en charge la commutation des interrupteurs</title>

    <code mime="application/javascript"><![CDATA[
    _switchFlip: function() {

        // Change the picture depending on which switches are flipped
        if (this._flySwitch.get_active()) {

            if (this._birdSwitch.get_active()) this._image.set_from_file ("muteswan.png");

            else this._image.set_from_file ("fruitbat.png");
        }

        else {

            if (this._birdSwitch.get_active()) this._image.set_from_file ("gentoopenguin.png");

            else this._image.set_from_file ("redfox.png");

        }

    },
]]></code>
    <p>À chaque fois qu'un interrupteur est activé, cette fonction vérifie lequel des deux interrupteurs est allumé à l'aide de la fonction intégrée get_active(). Elle affiche ensuite l'image correspondante. Vous pouvez modifier les noms des fichiers comme bon vous semble, tant que les images correspondantes sont dans le même dossier.</p>
</section>

<section id="about">
    <title>Création de la boîte de dialogue « À propos »</title>
    <code mime="application/javascript"><![CDATA[
    _initMenus: function() {

        // Build the application's menu so we can have an "About" button
        let menu = new Gio.Menu();
        menu.append("About", 'app.about');
        menu.append("Quit",'app.quit');
        this.application.set_app_menu(menu);

        // Bind the "About" button to the _showAbout() function
        let aboutAction = new Gio.SimpleAction ({ name: 'about' });
        aboutAction.connect('activate', Lang.bind(this,
            function() {
                this._showAbout();
            }));
        this.application.add_action(aboutAction);

        // Bind the "Quit" button to the function that closes the window
        let quitAction = new Gio.SimpleAction ({ name: 'quit' });
        quitAction.connect('activate', Lang.bind(this,
            function() {
                this._window.destroy();
            }));
        this.application.add_action(quitAction);
    },
]]></code>
    <p>La première étape est de créer le <link xref="gmenu.js">GMenu</link> dans lequel s'insère le bouton « À propos ». C'est ce menu qui s'affiche quand vous cliquez dans le coin gauche supérieur de l'écran, à côté du menu Activités. Notre menu ne contient que deux options : À propos et Quitter.</p>

    <code mime="application/javascript"><![CDATA[
    _showAbout: function () {

        // String arrays of the names of the people involved in the project
        var artists = ['Rob Lee http://en.wikipedia.org/wiki/File:Fuzzy_Freddy.jpg', 'Ken Funakoshi http://en.wikipedia.org/wiki/File:Pygoscelis_papua_-Nagasaki_Penguin_Aquarium_-swimming_underwater-8a.jpg', 'Shek Graham http://www.flickr.com/photos/shekgraham/127431519/in/photostream/', 'Mindaugas Urbonas http://commons.wikimedia.org/wiki/File:Mute_Swan-Mindaugas_Urbonas.jpg'];
        var authors = ["GNOME Documentation Team"];
        var documenters = ["GNOME Documentation Team"];

        // Create the About dialog
        let aboutDialog = new Gtk.AboutDialog({
            title: "AboutDialog Example",
            program_name: "Animal Creator",
            copyright: "Copyright \xa9 2012 GNOME Documentation Team\n\nRed fox photo licensed CC-By by Rob Lee\nGentoo penguin photo licensed CC-By-SA by Ken Funakoshi\nFruit bat photo licensed CC-By by Shek Graham\nMute swan photo licensed CC-By-SA by Mindaugas Urbonas\nLinks to the originals are available under Credits.\n\nHave you hugged a penguin today?",
            artists: artists,
            authors: authors,
            documenters: documenters,
            website: "http://developer.gnome.org",
            website_label: "GNOME Developer Website" });

        // Attach the About dialog to the window
        aboutDialog.modal = true;
        aboutDialog.transient_for = this._window;

        // Show the About dialog
        aboutDialog.show();

        // Connect the Close button to the destroy signal for the dialog
        aboutDialog.connect('response', function() {
            aboutDialog.destroy();
        });
    }

});
]]></code>
    <p>Vous pouvez définir beaucoup de choses très différentes dans une boîte de dialogue <link xref="aboutdialog.js">À propos</link>, comme créditer tous les contributeurs ayant participé au développement de l'application et laisser une note à qui veut la lire. Dans notre cas, la section licence contient nos notes et les crédits aux photographes auteurs, alors que la section artistes affiche une liste des photographes avec les liens vers les images originales quand vous cliquez sur le bouton Crédits. Les liens URL qui suivent leurs noms les transforment en liens cliquables vous renvoyant à la section Crédits.</p>

    <code mime="application/javascript"><![CDATA[
// Run the application
let app = new SwitchExample ();
app.application.run (ARGV);
]]></code>
    <p>Enfin, créons une nouvelle instance de la classe SwitchExample et démarrons l'application.</p>
  </section>

  <section id="complete">
    <title>Exemple complet de code</title>
<code mime="application/javascript" style="numbered">#!/usr/bin/gjs

imports.gi.versions.Gtk = '3.0';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

class SwitchExample {

    // Create the application itself
    constructor() {
        this.application = new Gtk.Application({
            application_id: 'org.example.jsswitch'
        });

        // Connect 'activate' and 'startup' signals to the callback functions
        this.application.connect('activate', this._onActivate.bind(this));
        this.application.connect('startup', this._onStartup.bind(this));
    }

    // Callback function for 'activate' signal presents window when active
    _onActivate() {
        this._window.present();
    }

    // Callback function for 'startup' signal creates the menu and builds the UI
    _onStartup() {
        this._initMenus();
        this._buildUI();
    }

    // Build the application's UI
    _buildUI() {

        // Create the application window
        this._window = new Gtk.ApplicationWindow({
            application: this.application,
            window_position: Gtk.WindowPosition.CENTER,
            border_width: 20,
            title: "Animal Creator"});

        // Create the image widget and set its default picture
        this._image = new Gtk.Image ({file: "redfox.png"});

        // Create a label for the first switch
        this._flyLabel = new Gtk.Label ({
            label: "Make it fly",
            margin_right: 30});

        // Create the first switch and set its default position
        this._flySwitch = new Gtk.Switch ({active: false});
        this._flySwitch.connect ('notify::active', this._switchFlip.bind(this));

        // Create a label for the second switch
        this._birdLabel = new Gtk.Label ({
            label: "Make it a bird",
            margin_right: 30});

        // Create the second switch
        this._birdSwitch = new Gtk.Switch ({active: false});
        this._birdSwitch.connect ('notify::active', this._switchFlip.bind(this));

        // Create a grid for the labels and switches beneath the picture
        this._UIGrid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER,
            margin_top: 20});

        // Attach the labels and switches to that grid
        this._UIGrid.attach (this._flyLabel, 0, 0, 1, 1);
        this._UIGrid.attach (this._flySwitch, 1, 0, 1, 1);
        this._UIGrid.attach (this._birdLabel, 0, 1, 1, 1);
        this._UIGrid.attach (this._birdSwitch, 1, 1, 1, 1);

        // Create a master grid to put both the UI and the picture into
        this._mainGrid = new Gtk.Grid ({
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER });

        // Attach the picture and the UI grid to the master grid
        this._mainGrid.attach (this._image, 0, 0, 1, 1);
        this._mainGrid.attach (this._UIGrid, 0, 1, 1, 1);

        // Add the master grid to the window
        this._window.add (this._mainGrid);

        // Show the window and all child widgets
        this._window.show_all();
    }

    _switchFlip() {

        // Change the picture depending on which switches are flipped
        if (this._flySwitch.get_active()) {

            if (this._birdSwitch.get_active())
                this._image.set_from_file ("muteswan.png");
            else
                this._image.set_from_file ("fruitbat.png");
        } else {

            if (this._birdSwitch.get_active())
                this._image.set_from_file ("gentoopenguin.png");
            else
                this._image.set_from_file ("redfox.png");
        }
    }

    _initMenus() {

        // Build the application's menu so we can have an "About" button
        let menu = new Gio.Menu();
        menu.append("About", 'app.about');
        menu.append("Quit",'app.quit');
        this.application.set_app_menu(menu);

        // Bind the "About" button to the _showAbout() function
        let aboutAction = new Gio.SimpleAction ({ name: 'about' });
        aboutAction.connect('activate', () =&gt; { this._showAbout(); });
        this.application.add_action(aboutAction);

        // Bind the "Quit" button to the function that closes the window
        let quitAction = new Gio.SimpleAction ({ name: 'quit' });
        quitAction.connect('activate', () =&gt; { this._window.destroy(); });
        this.application.add_action(quitAction);
    }

    _showAbout() {

        // String arrays of the names of the people involved in the project
        var artists = ['Rob Lee http://en.wikipedia.org/wiki/File:Fuzzy_Freddy.png', 'Ken Funakoshi http://en.wikipedia.org/wiki/File:Pygoscelis_papua_-Nagasaki_Penguin_Aquarium_-swimming_underwater-8a.png', 'Shek Graham http://www.flickr.com/photos/shekgraham/127431519/in/photostream/', 'Mindaugas Urbonas http://commons.wikimedia.org/wiki/File:Mute_Swan-Mindaugas_Urbonas.png'];
        var authors = ["GNOME Documentation Team"];
        var documenters = ["GNOME Documentation Team"];

        // Create the About dialog
        let aboutDialog = new Gtk.AboutDialog({
            title: "AboutDialog Example",
            program_name: "Animal Creator",
            copyright: "Copyright \xa9 2012 GNOME Documentation Team\n\nRed fox photo licensed CC-By by Rob Lee\nGentoo penguin photo licensed CC-By-SA by Ken Funakoshi\nFruit bat photo licensed CC-By by Shek Graham\nMute swan photo licensed CC-By-SA by Mindaugas Urbonas\nLinks to the originals are available under Credits.\n\nHave you hugged a penguin today?",
            artists: artists,
            authors: authors,
            documenters: documenters,
            website: "http://developer.gnome.org",
            website_label: "GNOME Developer Website" });

        // Attach the About dialog to the window
        aboutDialog.modal = true;
        aboutDialog.transient_for = this._window;

        // Show the About dialog
        aboutDialog.show();

        // Connect the Close button to the destroy signal for the dialog
        aboutDialog.connect('response', function() {
            aboutDialog.destroy();
        });
    }
};

// Run the application
let app = new SwitchExample ();
app.application.run (ARGV);
</code>
  </section>

  <section id="in-depth">
    <title>Documentation approfondie</title>
<list>
  <item><p><link href="http://developer.gnome.org/gio/unstable/GMenu.html">GMenu</link></p></item>
  <item><p><link href="http://developer.gnome.org/gio/stable/GSimpleAction.html">GSimpleAction</link></p></item>
  <item><p><link href="http://www.roojs.com/seed/gir-1.2-gtk-3.0/gjs/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Grid.html">Gtk.Grid</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Image.html">Gtk.Image</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Label.html">Gtk.Label</link></p></item>
  <item><p><link href="http://www.roojs.org/seed/gir-1.2-gtk-3.0/gjs/Gtk.Switch.html">Gtk.Switch</link></p></item>
</list>
  </section>
</page>
