<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="paned.c" xml:lang="fr">
  <info>
    <title type="text">Paned (C)</title>
    <link type="guide" xref="c#layout"/>
    <revision version="0.1" date="2013-07-04" status="review"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>A widget with two adjustable panes</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Paned</title>
  <media type="image" mime="image/png" src="media/paned.png"/>
  <p>Two images in two adjustable panes, horizontally aligned.</p>

  <links type="section"/>

  <section id="code">
    <title>Code utilisé pour générer cet exemple</title>
    <code mime="text/x-csrc" style="numbered">#include &lt;gtk/gtk.h&gt;

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *paned;
  GtkWidget *window;
  GtkWidget *image1;
  GtkWidget *image2;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Paned Example");
  gtk_window_set_default_size (GTK_WINDOW (window), 450, 350);

  paned = gtk_paned_new (GTK_ORIENTATION_HORIZONTAL);
  image1 = gtk_image_new_from_file ("gnome-image.png");
  image2 = gtk_image_new_from_file ("tux.png");

  gtk_paned_add1 (GTK_PANED (paned), image1);
  gtk_paned_add2 (GTK_PANED (paned), image2);

  gtk_container_add (GTK_CONTAINER (window), paned);

  gtk_widget_show_all (window);
}

int
main (int argc, char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);
  return status;
}
</code>
  </section>

  <section id="references">
    <title>Références API</title>
    <p>Dans cet exemple, les éléments suivants sont utilisés :</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/GtkPaned.html">GtkPaned</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/stable/gtk3-Standard-Enumerations.html#GtkOrientation">Standard Enumerations</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkImage.html">GtkImage</link></p></item>
    </list>
  </section>
</page>
