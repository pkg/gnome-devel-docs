<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="messagedialog.vala" xml:lang="fr">
  <info>
  <title type="text">MessageDialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Une fenêtre de message</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>Boîte de dialogue de message</title>
  <media type="image" mime="image/png" src="media/messagedialog.png"/>
  <p>Une boîte de dialogue de message modal qui peut faire exploser le monde.</p>

<code mime="text/x-csharp" style="numbered">
//A window in the application
public class Window : Gtk.ApplicationWindow {
	public Window (Application app) {
		Object (application: app, title: "Gtk.MessageDialog Example");

		var label = new Gtk.Label ("This application goes boom!");
		this.add (label);

		var message_action = new SimpleAction ("message", null);
		message_action.activate.connect (message);
		this.add_action (message_action);

		this.set_default_size (400, 200);
		this.show_all ();
	}

	void dialog_response (Gtk.Dialog dialog, int response_id) {
		switch (response_id) {
			case Gtk.ResponseType.OK:
				print ("*boom*\n");
				break;
			case Gtk.ResponseType.CANCEL:
				print ("good choice\n");
				break;
			case Gtk.ResponseType.DELETE_EVENT:
				print ("dialog closed or cancelled\n");
				break;
		}
			dialog.destroy();
	}

	void message (SimpleAction simple, Variant? parameter) {
		var messagedialog = new Gtk.MessageDialog (this,
                            Gtk.DialogFlags.MODAL,
                            Gtk.MessageType.WARNING,
                            Gtk.ButtonsType.OK_CANCEL,
                            "This action will cause the universe to stop existing.");

		messagedialog.response.connect (dialog_response);
		messagedialog.show ();
	}
}

//This is the Application
public class Application : Gtk.Application {
	protected override void activate () {
		new Window (this);
	}

	protected override void startup () {
		base.startup ();

		var menu = new Menu ();
		menu.append ("Message", "win.message");
		menu.append ("Quit", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		//quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	public Application () {
		Object (application_id: "org.example.application");
	}
}

//main function creates Application and runs it
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>Dans cet exemple, les éléments suivants sont utilisés :</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ResponseType.html">Gtk.ResponseType</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.MessageDialog.html">Gtk.MessageDialog</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.DialogFlags.html">Gtk.DialogFlags</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.MessageType.html">Gtk.MessageType</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ButtonsType.html">Gtk.ButtonsType</link></p></item>
</list>
</page>
