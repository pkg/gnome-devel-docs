<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="aboutdialog.vala" xml:lang="fr">
  <info>
  <title type="text">AboutDialog (Vala)</title>
    <link type="guide" xref="beginner.vala#windows"/>
    <link type="seealso" xref="button.vala"/>
    <link type="seealso" xref="linkbutton.vala"/>
    <revision version="0.1" date="2012-04-07" status="stub"/>

    <credit type="author copyright">
      <name>Ryan Lortie</name>
      <email its:translate="no">desrt@desrt.ca</email>
      <years>2012</years>
    </credit>

    <credit type="author">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
    </credit>


    <desc>Afficher des informations à propos d'une application</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Rebert,</mal:name>
      <mal:email>traduc@rebert.name</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alain Lojewski,</mal:name>
      <mal:email>allomervan@gmail.com</mal:email>
      <mal:years>2011-2012</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luc Pionchon</mal:name>
      <mal:email>pionchon.luc@gmail.com</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Bruno Brouard</mal:name>
      <mal:email>annoa.b@gmail.com</mal:email>
      <mal:years>2011-12</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Luis Menina</mal:name>
      <mal:email>liberforce@freeside.fr</mal:email>
      <mal:years>2014</mal:years>
    </mal:credit>
  </info>

  <title>AboutDialog</title>
  <media type="image" mime="image/png" src="media/aboutdialog_GMenu.png"/>
  <p>Un exemple AboutDialog utilisant Gtk.ApplicationWindow et Menu</p>
  <note><p> <em style="bold">GTK 3.4 ou supérieur est requis pour que cela fonctionne</em></p></note>

<code mime="text/x-csharp" style="numbered">/* A window in the application */
public class Window : Gtk.ApplicationWindow {

	/* The constructor */
	public Window (Application app) {
		Object (application: app, title: "AboutDialog Example");

		var about_action = new SimpleAction ("about", null);

		about_action.activate.connect (this.about_cb);
		this.add_action (about_action);
		this.show_all ();
	}

	/* This is the callback function connected to the 'activate' signal
	 * of the SimpleAction about_action.
	 */
	void about_cb (SimpleAction simple, Variant? parameter) {
		string[] authors = { "GNOME Documentation Team", null };
		string[] documenters = { "GNOME Documentation Team", null };

		Gtk.show_about_dialog (this,
                               "program-name", ("GtkApplication Example"),
                               "copyright", ("Copyright \xc2\xa9 2012 GNOME Documentation Team"),
                               "authors", authors,
                               "documenters", documenters,
                               "website", "http://developer.gnome.org",
                               "website-label", ("GNOME Developer Website"),
                               null);
	}
}

/* This is the Application */
public class Application : Gtk.Application {

	/* Here we override the activate signal of GLib.Application */
	protected override void activate () {
		new Window (this);
	}

	/* Here we override the startup signal of GLib.Application */
	protected override void startup () {

		base.startup ();

		var menu = new Menu ();
		menu.append ("About", "win.about");
		menu.append ("Quit", "app.quit");
		this.app_menu = menu;

		var quit_action = new SimpleAction ("quit", null);
		//quit_action.activate.connect (this.quit);
		this.add_action (quit_action);
	}

	/* The constructor */
	public Application () {
		Object (application_id: "org.example.application");
	}
}

/* main function creates Application and runs it */
int main (string[] args) {
	return new Application ().run (args);
}
</code>
<p>Dans cet exemple, les éléments suivants sont utilisés :</p>
<list>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Application.html">Gtk.Application</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.ApplicationWindow.html">Gtk.ApplicationWindow</link></p></item>
  <item><p><link href="http://www.valadoc.org/gtk+-3.0/Gtk.Window.set_default_size.html">set_default_size</link></p></item>
</list>
</page>
