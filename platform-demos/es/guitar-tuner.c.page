<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="topic" id="guitar-tuner.c" xml:lang="es">

  <info>
    <title type="text">Afinador de guitarra (C)</title>
    <link type="guide" xref="c#examples"/>

    <desc>Usar GTK+ y GStreamer para construir un sencillo afinador de guitarra para GNOME. Muestra cómo usar el diseñador de interfaces.</desc>

    <revision pkgversion="0.1" version="0.1" date="2010-12-02" status="review"/>
    <credit type="author">
      <name>Proyecto de documentación de GNOME</name>
      <email its:translate="no">gnome-doc-list@gnome.org</email>
    </credit>
    <credit type="author">
      <name>Johannes Schmid</name>
      <email its:translate="no">jhs@gnome.org</email>
    </credit>
    <credit type="editor">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2013</years>
    </credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

<title>Afinador de guitarra</title>

<synopsis>
  <p>En este tutorial se va a hacer un programa que reproduce tonos que puede usar para afinar su guitarra. Aprenderá a:</p>
  <list>
    <item><p>Configurar un proyecto básico en Anjuta</p></item>
    <item><p>Crear una IGU sencilla con el diseñador IU de Anjuta</p></item>
    <item><p>Usar GStreamer para reproducir sonidos</p></item>
  </list>
  <p>Necesitará lo siguiente para poder seguir este tutorial:</p>
  <list>
    <item><p>Una copia instalada del <link xref="getting-ready">EID Anjuta</link></p></item>
    <item><p>Conocimiento básico del lenguaje de programación C</p></item>
  </list>
</synopsis>

<media type="image" mime="image/png" src="media/guitar-tuner.png"/>

<section id="anjuta">
  <title>Crear un proyecto en Anjuta</title>
  <p>Antes de empezar a programar, deberá configurar un proyecto nuevo en Anjuta. Esto creará todos los archivos que necesite para construir y ejecutar el código más adelante. También es útil para mantener todo ordenado.</p>
  <steps>
    <item>
    <p>Inicie Anjuta y pulse <guiseq><gui>Archivo</gui><gui>Nuevo</gui><gui>Proyecto</gui></guiseq> para abrir el asistente de proyectos.</p>
    </item>
    <item>
    <p>Seleccione <gui>GTK+ (simple)</gui> en la pestaña <gui>C</gui>, pulse <gui>Continuar</gui>, y rellene sus detalles en las siguientes páginas. Use <file>afinador-guitarra</file> como nombre del proyecto y de la carpeta.</p>
   	</item>
    <item>
    <p>Asegúrese de que <gui>Configurar paquetes externos</gui> está <gui>activada</gui>. En la siguiente página, seleccione <em>gstreamer-0.10</em> de la lista para incluir la biblioteca GStreamer en su proyecto.</p>
    </item>
    <item>
    <p>Pulse <gui>Aplicar</gui> y se creará el proyecto. Abra <file>src/main.c</file> desde las pestañas <gui>Proyecto</gui> o <gui>Archivo</gui>. Debería ver algo de código que comience con las líneas:</p>
    <code mime="text/x-csrc">
#include &lt;config.h&gt;
#include &lt;gtk/gtk.h&gt;</code>
    </item>
  </steps>
</section>

<section id="build">
  <title>Construir el código por primera vez</title>
  <p>C es un lenguaje más detallado, por lo que no se sorprenda de que el archivo contiene un gran cantidad de código. La mayor parte es código de plantilla. Carga una ventana (vacía) desde el archivo de descripción de la interfaz de usuario y la muestra. A continuación se ofrecen más detalles; omita esta lista si entiende los conceptos básicos:</p>

  <list>
  <item>
    <p>Las tres líneas <code>#include</code> en la parte superior incluyen las bibliotecas <code>config</code> (útil para definiciones de construcción de autoconf), <code>gtk</code> (interfaz de usuario) y <code>gi18n</code> (internacionalización). Las funciones de estas bibliotecas se usan en el resto del código.</p>
   </item>
   <item>
    <p>La función <code>create_window</code> crea una ventana nueva abriendo un archivo de GtkBuilder (<file>src/guitar-tuner.ui</file>, definido unas pocas líneas más arriba), conectando sus señales y mostrándolo en una ventana. El archivo de GtkBuilder contiene una descripción de una interfaz de usuario y de todos sus elementos. Puede usar el editor Anjuta para diseñar interfaces de usuario con GtkBuilder.</p>
    <p>Conectar señales es como se define lo que pasa cuando pulsa un botón, o cuando ocurre algún otro evento. Aquí, se llama a la función <code>destroy</code> (y se sale de la aplicación) cuando cierra la ventana.</p>
   </item>
   <item>
    <p>La función <code>main</code> se ejecuta de manera predeterminada cuando inicia una aplicación en C. Llama a unas pocas funciones que configuran y ejecutan la aplicación. La función <code>gtk_main</code> inicia el bucle principal de GTK+, que ejecuta la interfaz de usuario y comienza a escuchar eventos (como pulsaciones del ratón y del teclado).</p>
   </item>
   <item>
    <p>La definición condicional <code>ENABLE_NLS</code> configura <code>gettext</code>, que es un entorno de trabajo para traducir aplicaciones. Estas funciones especifican cómo deben manejar su aplicación las herramientas de traducción cuando las ejecuta.</p>
   </item>
  </list>

  <p>Este código está listo para usarse, por lo que puede compilarlo pulsando <guiseq><gui>Construir</gui><gui>Construir proyecto</gui></guiseq> (o pulsando <keyseq><key>Mayús</key><key>F7</key></keyseq>).</p>
  <p>Pulse <gui>Ejecutar</gui> en la siguiente ventana que aparece para configurar una construcción de depuración. Esto sólo necesita hacer una vez para la primera construcción.</p>
</section>

<section id="ui">
  <title>Crear la interfaz de usuario</title>
  <p>El archivo de GtkBuilder contiene una descripción de la interfaz de usuario (IU). Para editar la interfaz de usuario, abra <file>src/guitar_tuner.ui</file>. Esto cambiará al diseñador de interfaces. La ventana de diseño está en el centro; los widgets y sus propiedades están a la izquierda, y la paleta de los widgets disponibles está a la derecha.</p>
  <p>La distribución de cualquier IU en GTK+ se organiza usando cajas y tablas. Aquí se usará una <gui>GtkButtonBox</gui> vertical para asignar seis <gui>GtkButtons</gui>, uno para cada una de las cuerdas de la guitarra.</p>

<media type="image" mime="image/png" src="media/guitar-tuner-glade.png"/>

  <steps>
   <item>
   <p>Seleccione una <gui>GtkButtonBox</gui> de la sección <gui>Contenedor</gui> de la <gui>Paleta</gui> de la derecha y póngalo en la ventana. En el panel de <gui>Propiedades</gui>, establezca el número de elementos a «6» (para las seis cuerdas) y la orientación a «vertical».</p>
   </item>
   <item>
    <p>Ahora, elija un <gui>GtkButton</gui> de la paleta y póngalo en la primera parte de la caja.</p>
   </item>
   <item>
    <p>Mientras el botón esté seleccionado, cambie la propiedad <gui>Etiqueta</gui> en la pestaña <gui>Widgets</gui> a <gui>E</gui>. Esta será la cuerda Mi grave.</p>
    </item>
    <item>
     <p>Cambie a la pestaña <gui>Señales</gui> (dentro de la pestaña <gui>Widgets</gui>) y busque la señal <code>clicked</code> del botón. Puede usar esto para conectar un gestor de señal al que se llamará cuando el usuario pulse el botón. Para hacer esto, pulse sobre la señal, escriba <code>on_button_clicked</code> en la columna <gui>Gestor</gui> y pulse <key>Intro</key>.</p>
    </item>
    <item>
    <p>Repita los pasos anteriores para el resto de botones, añadiendo las 5 cuerdas restantes con los nombres <em>A</em>, <em>D</em>, <em>G</em>, <em>B</em>, y <em>e</em>.</p>
    </item>
    <item>
    <p>Guarde el diseño de la IU (pulsando <guiseq><gui>Archivo</gui><gui>Guardar</gui></guiseq>) y déjelo abierto.</p>
    </item>
  </steps>
</section>

<section id="signal">
  <title>Crear el gestor de señales</title>
  <p>El el diseñador de interfaces, se ha hecho que todos los botones llamen a la misma función, <gui>on_button_clicked</gui>, cuando se pulsan. Se debe añadir esta función al archivo de código fuente.</p>
  <p>Para hacer esto, abra <file>main.c</file> mientras el archivo de la interfaz de usuario está abierto. Cambie a la pestaña <gui>Señales</gui>, que ya ha usado para establecer el nombre de la señal. Ahora vaya a la fila en la que estableció la señal <gui>clicked</gui> y arrástrela al archivo de código fuente, fuera de cualquier función. Se añadirá el siguiente código a su archivo de código fuente:</p>
<code mime="text/x-csrc">
void on_button_clicked (GtkWidget* button, gpointer user_data)
{

}</code>
  <p>El gestor de la señal tiene dos argumentos. un puntero al <code>GtkWidget</code> que llamó a la función (en este caso, siempre es un <code>GtkButton</code>), y un puntero a ciertos «datos de usuario» que puede definir, pero que aquí no se usan. (Puede establecer los datos de usuario llamando a <code>gtk_builder_connect_signals</code>; normalmente se usa para pasar un puntero a una estructura de datos a la que puede necesitar acceder dentro del gestor de la señal.)</p>
  <p>Por ahora, se dejará el gestor de la señal vacío mientras se escribe el código para producir sonidos.</p>
</section>

<section id="gstreamer">
  <title>Tuberías de Gstreamer</title>
  <p>GStreamer es el entorno multimedia de trabajo de GNOME: puede usarlo para reproducir, grabar y procesar vídeo, sonido, flujos de la cámara web y similares. En este caso, se usará para generar tonos de frecuencia única.</p>
  <p>Conceptualmente. GStreamer funciona de la siguiente manera: puede crear una <em>tubería</em> que contenga varios elementos de procesado que van desde la <em>fuente</em> hasta el <em>sumidero</em> (salida). La fuente puede ser, por ejemplo, un archivo de imagen, un vídeo o un archivo de música, y la salida puede ser un widget o la tarjeta de sonido.</p>
  <p>Entre la fuente y el sumidero, puede aplicar varios filtros y conversores para manejar efectos, conversiones de formato, etc. Cada elemento de la tubería tiene propiedades que se pueden usar para cambiar este comportamiento.</p>
  <media type="image" mime="image/png" src="media/guitar-tuner-pipeline.png">
    <p>Un ejemplo de tubería de GStreamer.</p>
  </media>
</section>

<section id="pipeline">
  <title>Configurar la tubería</title>
  <p>En este sencillo ejemplo se usará un generador de tonos llamado <code>audiotestsrc</code> y se enviará la salida al dispositivo de sonido predeterminado del sistema, <code>autoaudiosink</code>. Sólo es necesario configurar la frecuencia del generador de tonos; esto es accesible a través de la propiedad <code>freq</code> de <code>audiotestsrc</code>.</p>

  <p>Inserte la siguiente línea en <file>main.c</file>, justo a continuación de la línea <code>#include &lt;gtk/gtk.h&gt;</code>:</p>
  <code mime="text/x-csrc">#include &lt;gst/gst.h&gt;</code>
  <p>Esto incluye la bilbioteca GStreamer. También necesita añadir una línea para inicializar GStreamer; ponga la siguiente línea de código antes de la llamada <code>gtk_init</code> en la función <code>main</code>:</p>
  <code>gst_init (&amp;argc, &amp;argv);</code>
  <p>Después, copie la siguiente función en <file>main.c</file> encima de la función <code>on_button_clicked</code> vacía:</p>
  <code mime="text/x-csrc">static void
play_sound (gdouble frequency)
{
	GstElement *source, *sink;
	GstElement *pipeline;

	pipeline = gst_pipeline_new ("note");
	source   = gst_element_factory_make ("audiotestsrc",
	                                     "source");
	sink     = gst_element_factory_make ("autoaudiosink",
	                                     "output");

	/* set frequency */
	g_object_set (source, "freq", frequency, NULL);

	gst_bin_add_many (GST_BIN (pipeline), source, sink, NULL);
	gst_element_link (source, sink);

	gst_element_set_state (pipeline, GST_STATE_PLAYING);

	/* stop it after 500ms */
	g_timeout_add (LENGTH, (GSourceFunc) pipeline_stop, pipeline);
}</code>

  <steps>
    <item>
    <p>Las cinco primeras líneas crean los elementos «fuente» y «sumidero» de GStreamer (<code>GstElement</code>), y un elemento de tubería (que se usará como contenedor de los otros dos elementos). A la tubería se le asigna el nombre «note»; la fuente se llama «source» y se asocia a la fuente <code>audiotestsrc</code> y el sumidero se llama «output» y se asocia con el sumidero <code>autoaudiosink</code> (la salida de la tarjeta de sonido predeterminada).</p>
    </item>
    <item>
    <p>La llamada a <code>g_object_set</code> establece la propiedad <code>freq</code> del elemento fuente a <code>frequency</code>, que se pasa como argumento a la función <code>play_sound</code>. Esto sólo es la frecuencia de la nota en Hercios; más adelante se definirán algunas frecuencias útiles.</p>
    </item>
    <item>
    <p><code>gst_bin_add_many</code> añade la fuente y el sumidero a la tubería. La tubería es un <code>GstBin</code>, que es un elemento que puede contener otros muchos elementos de GStreamer. En general, puede añadir tantos elementos como quiera a una tubería añadiendo más argumentos a <code>gst_bin_add_many</code>.</p>
    </item>
    <item>
    <p>Después, se usa <code>gst_element_link</code> para conectar los elementos de forma conjunta, de tal forma que la salida de <code>source</code> (un tono) va a la entrada de <code>sink</code> (que es después la salida de la tarjeta de sonido). <code>gst_element_set_state</code> se usa al iniciar la reproducción, configurando el estado de la tubería a reproduciendo (<code>GST_STATE_PLAYING</code>).</p>
    </item>
  </steps>

</section>

<section id="stop">
  <title>Detener la reproducción</title>
  <p>No se quiere reproducir un tono molesto para siempre, por lo que lo último que <code>play_sound</code> hace es llamar a <code>g_timeout_add</code>. Esto establece un tiempo de expiración para detener el sonido; espera <code>LENGTH</code> milisegundos antes de llamar a la función <code>pipeline_stop</code>, y se queda llamando a <code>pipeline_stop</code> hasta que devuelve <code>FALSE</code>.</p>
  <p>Ahora, se escribirá el código de la función <code>pipeline_stop</code>, llamada por <code>g_timeout_add</code>. Inserte el código siguiente <em>encima</em> de la función <code>play_sound</code>:</p>
  <code mime="text/x-csrc">
#define LENGTH 500 /* Length of playing in ms */

static gboolean
pipeline_stop (GstElement* pipeline)
{
	gst_element_set_state (pipeline, GST_STATE_NULL);
	g_object_unref (pipeline);

	return FALSE;
}</code>
  <p>La llamada a <code>gst_element_set_state</code> detiene la reproducción de la tubería y <code>g_object_unref</code> desreferencia la tubería, la destruye y libera su memoria.</p>
</section>

<section id="tones">
  <title>Definir los tonos</title>
  <p>Se quiere reproducir el sonido correcto cuando un usuario pulsa un botón. En primer lugar, se necesita conocer las frecuencias de las seis cuerdas de la guitarra, que están definidas (al principio de <file>main.c</file>) de la siguiente manera:</p>
  <code mime="text/x-csrc">
/* Frequencies of the strings */
#define NOTE_E 329.63
#define NOTE_A 440
#define NOTE_D 587.33
#define NOTE_G 783.99
#define NOTE_B 987.77
#define NOTE_e 1318.5</code>
  <p>Ahora se se profundiza en el gestor de la señal definido anteriormente, <code>on_button_clicked</code>. Se podría haber conectado cada botón a un gestor de la señal diferente, pero esto había supuesto duplicar mucho código. En su lugar, se puede usar la etiqueta del botón para saber cuál de ellos se ha pulsado:</p>
  <code mime="text/x-csrc">
/* Callback for the buttons */
void on_button_clicked (GtkButton* button,
                        gpointer user_data)
{
	const gchar* text = gtk_button_get_label (button);

	if (g_str_equal (text, _("E")))
	    play_sound (NOTE_E);
	else if (g_str_equal (text, _("A")))
	    play_sound (NOTE_A);
	else if (g_str_equal (text, _("G")))
	    play_sound (NOTE_G);
	else if (g_str_equal (text, _("D")))
	    play_sound (NOTE_D);
	else if (g_str_equal (text, _("B")))
	    play_sound (NOTE_B);
	else if (g_str_equal (text, _("e")))
	    play_sound (NOTE_e);
}
</code>
  <p>A <code>on_button_clicked</code> se le pasa como argumento (<code>button</code>) un puntero al <code>GtkButton</code> que se ha pulsado. Se puede obtener el texto de este botón usando <code>gtk_label_get_label</code>.</p>
  <p>El texto se compara con las notas que se tiene usando <code>g_str_equal</code>, y se llama a <code>play_sound</code> con la frecuencia correspondiente a cada nota. Esto reproduce el tono; el afinador de guitarra ya está funcionando.</p>
</section>

<section id="run">
  <title>Construir y ejecutar la aplicación</title>
  <p>Todo el código debería estar listo para ejecutarse. Pulse <guiseq><gui>Construir</gui><gui>Construir proyecto</gui></guiseq> para construir todo otra vez y pulse <guiseq><gui>Ejecutar</gui><gui>Ejecutar</gui></guiseq> para iniciar la aplicación.</p>
  <p>Si todavía no lo ha hecho, elija la aplicación <file>Debug/src/afinador-guitarra</file> en el diálogo que aparece. Finalmente, pulse <gui>Ejecutar</gui> y disfrute.</p>
</section>

<section id="impl">
 <title>Implementación de referencia</title>
 <p>Si tiene problemas con este tutorial, compare su código con este <link href="guitar-tuner/guitar-tuner.c">código de referencia</link>.</p>
</section>

<section id="next">
  <title>Siguientes pasos</title>
  <p>Aquí hay algunas ideas sobre cómo puede extender esta sencilla demostración:</p>
  <list>
   <item>
   <p>Hacer que el programa recorra las notas automáticamente.</p>
   </item>
   <item>
   <p>Hacer que el programa reproduzca grabaciones de cuerdas de guitarras que se están afinando.</p>
   <p>PAra hacer esto, debe configurar una tubería de GStreamer más complicada, que le permite cargar y reproducir archivos de música. Deberá elegir un los elementos <link href="http://gstreamer.freedesktop.org/documentation/plugins.html">decodificador y demultiplexor</link> de GStreamer basándose en el formato del archivo de sus sonidos grabados; los MP3 usan elementos diferentes de los de los archivos Ogg Vorbis, por ejemplo.</p>
   <p>Puede querer conectar los elementos de maneras más complicadas. Esto puede implicar usar <link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/chapter-intro-basics.html">conceptos de GStreamer</link> que no se han comentado en este tutorial, tales como <link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/manual/html/section-intro-basics-pads.html">interfaces</link>. Es posible que encuentre útil el comando <cmd>gst-inspect</cmd>.</p>
   </item>
   <item>
   <p>Analizar automáticamente las notas que toca el músico.</p>
   <p>Puede conectar un micrófono y grabar sonidos con él usando una <link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-autoaudiosrc.html">fuente de entrada</link>. ¿Es posible que algún tipo de <link href="http://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-good-plugins/html/gst-plugins-good-plugins-plugin-spectrum.html">análisis de espectro</link> le permita saber qué notas se están reproduciendo?</p>
   </item>
  </list>
</section>

</page>
