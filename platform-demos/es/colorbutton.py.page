<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="colorbutton.py" xml:lang="es">
  <info>
    <title type="text">ColorButton (Python)</title>
    <link type="guide" xref="beginner.py#color-selectors"/>
    <link type="next" xref="fontchooserwidget.py"/>
    <revision version="0.1" date="2012-06-03" status="draft"/>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Un botón para mostrar el diálogo de selección de color</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>ColorButton</title>
  <media type="image" mime="image/png" src="media/colorbutton.png"/>
  <p>Este ColorButton muestra un diálogo de selección de color e imprime por la terminal los valores RGB del color seleccionado.</p>

  <links type="sections"/>
  
  <section id="code">
  <title>Código usado para generar este ejemplo</title>
  <code mime="text/x-python" style="numbered">from gi.repository import Gtk
from gi.repository import Gdk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="ColorButton", application=app)
        self.set_default_size(150, 50)
        self.set_border_width(10)

        # a colorbutton (which opens a dialogue window in
        # which we choose a color)
        self.button = Gtk.ColorButton()
        # with a default color (blue, in this instance)
        color = Gdk.RGBA()
        color.red = 0.0
        color.green = 0.0
        color.blue = 1.0
        color.alpha = 0.5
        self.button.set_rgba(color)

        # choosing a color in the dialogue window emits a signal
        self.button.connect("color-set", self.on_color_chosen)

        # a label
        label = Gtk.Label()
        label.set_text("Click to choose a color")

        # a grid to attach button and label
        grid = Gtk.Grid()
        grid.attach(self.button, 0, 0, 2, 1)
        grid.attach(label, 0, 1, 2, 1)
        self.add(grid)

    # if a new color is chosen, we print it as rgb(r,g,b) in the terminal
    def on_color_chosen(self, user_data):
        print("You chose the color: " + self.button.get_rgba().to_string())


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

    def do_startup(self):
        Gtk.Application.do_startup(self)

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
  <title>Métodos útiles para un widget «ColorButton»</title>
  <p><code>set_color(color)</code>, donde el <code>color</code> se define como en el ejemplo, establece el color del «ColorButton», que es negro de manera predeterminada. <code>get_color()</code> devuelve el color.</p>
    <p>En la línea 23 la señal <code>«color-set»</code> se conecta a una función de retorno de llamada <code>on_color_chosen()</code> usando <code><var>widget</var>.connect(<var>señal</var>, <var>función de retorno de llamada</var>)</code>. Consulte la sección <link xref="signals-callbacks.py"/> para obtener una explicación más detallada.</p>
  </section>
  
  <section id="references">
  <title>Referencias de la API</title>
  <p>En este ejemplo se usa lo siguiente:</p>
  <list>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorButton.html">GtkColorButton</link></p></item>
    <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkColorChooser.html">GtkColorChooser</link></p></item>
    <item><p><link href="http://developer.gnome.org/gdk3/stable/gdk3-RGBA-Colors.html">Colores RGBA</link></p></item>
  </list>
  </section>
</page>
