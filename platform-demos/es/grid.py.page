<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" xmlns:xi="http://www.w3.org/2001/XInclude" type="guide" style="task" id="grid.py" xml:lang="es">
  <info>
    <title type="text">Grid (Python)</title>
    <link type="guide" xref="beginner.py#layout"/>
    <link type="seealso" xref="label.py"/>
    <link type="next" xref="separator.py"/>
    <revision version="0.2" date="2012-08-01" status="stub"/>

    <credit type="author copyright">
      <name>Tiffany Antopolski</name>
      <email its:translate="no">tiffany.antopolski@gmail.com</email>
      <years>2012</years>
    </credit>

    <credit type="author copyright">
      <name>Marta Maria Casetti</name>
      <email its:translate="no">mmcasetti@gmail.com</email>
      <years>2012</years>
    </credit>

    <desc>Empaquetar widgets en filas y columnas</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Daniel Mustieles</mal:name>
      <mal:email>daniel.mustieles@gmail.com</mal:email>
      <mal:years>2011 - 2020</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Nicolás Satragno</mal:name>
      <mal:email>nsatragno@gmail.com</mal:email>
      <mal:years>2012 - 2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Jorge González</mal:name>
      <mal:email>jorgegonz@svn.gnome.org</mal:email>
      <mal:years>2011</mal:years>
    </mal:credit>
  </info>

  <title>Rejilla</title>

  <media type="image" mime="image/png" src="media/grid_simple.png"/>
  <p>Algunas etiquetas en una rejilla.</p>

  <links type="section"/>

  <section id="code">
    <title>Código usado para generar este ejemplo</title>
    <code mime="text/python" style="numbered">from gi.repository import Gtk
import sys


class MyWindow(Gtk.ApplicationWindow):

    def __init__(self, app):
        Gtk.Window.__init__(self, title="Grid Example", application=app)

        # three labels
        label_top_left = Gtk.Label(label="This is Top Left")
        label_top_right = Gtk.Label(label="This is Top Right")
        label_bottom = Gtk.Label(label="This is Bottom")

        # a grid
        grid = Gtk.Grid()

        # some space between the columns of the grid
        grid.set_column_spacing(20)

        # in the grid:
        # attach the first label in the top left corner
        grid.attach(label_top_left, 0, 0, 1, 1)
        # attach the second label
        grid.attach(label_top_right, 1, 0, 1, 1)
        # attach the third label below the first label
        grid.attach_next_to(
            label_bottom, label_top_left, Gtk.PositionType.BOTTOM, 2, 1)

        # add the grid to the window
        self.add(grid)


class MyApplication(Gtk.Application):

    def __init__(self):
        Gtk.Application.__init__(self)

    def do_activate(self):
        win = MyWindow(self)
        win.show_all()

app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)
</code>
  </section>

  <section id="methods">
    <title>Métodos útiles para un widget «Grid»</title>

    <list>
      <item><p>Para añadir un widget <code>hijo</code> en la posición <code>izquierda, arriba</code> en un espacio de una <code>anchura, altura</code> dada use <code>attach(hijo, arriba, izquierda, anchura, altura</code>. Si un widget <code>hermano</code> ya está en posición, también se puede usar <code>attach_next_to(hijo, hermano, lado, anchura, altura)</code>, donde <code>lado</code> puede ser <code>Gtk.PositionType.LEFT, Gtk.PositionType.RIGHT, Gtk.PositionType.TOP, Gtk.PositionType.BOTTOM</code>.</p></item>
      <item><p><code>insert_row(posición)</code> e <code>insert_column(posición)</code> hacen que los hijos añadidos en esa posición o por debajo se muevan una fila hacia abajo, y que los hijos que se extienden a lo largo de esta posición crezcan para ocupar la siguiente fila. <code>insert_next_to(hermano, lado)</code> inserta una fila o columna en la posición especificada. La fila o columna nueva se pone al lado del <code>hermano</code>, a su <code>lado</code>; si es <code>Gtk.PositionType.TOP</code> or <code>Gtk.PositionType.BOTTOM</code>, se inserta una fila; si es <code>Gtk.PositionType.LEFT</code> o <code>Gtk.PositionType.RIGHT</code>, se inserta una columna.</p></item>
      <item><p><code>set_row_homogeneous(True)</code> y <code>set_column_homogeneous(True)</code> se aseguran de que (respectivamente) cada fila o cada columna tengan la misma anchura o altura.</p></item>
      <item><p><code>set_row_spacing(espaciado)</code> y <code>set_column_spacing(espaciado)</code> fuerzan un espaciado entre (respectivamente) filas o columnas. El valor de <code>espaciado</code> puede estar entre <code>0</code>, que es el valor predeterminado, y <code>32767</code>.</p></item>
    </list>

  </section>

  <section id="references">
    <title>Referencias de la API</title>
    <p>En este ejemplo se usa lo siguiente:</p>
    <list>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkApplication.html">GtkApplication</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkApplicationWindow.html">GtkApplicationWindow</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkLabel.html">GtkLabel</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkImage.html">GtkImage</link></p></item>
      <item><p><link href="http://developer.gnome.org/gtk3/unstable/GtkGrid.html">GtkGrid</link></p></item>
    </list>
  </section>
</page>
